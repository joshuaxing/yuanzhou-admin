<!--
 * @Author: your name
 * @Date: 2019-12-03 09:01:15
 * @LastEditTime: 2020-06-24 15:31:55
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-admin\node.md
 -->

### Git Flow

#### master 分支 - 线上分支（只做提交，不做修改）

#### Develop 分支 - 主开发分支（项目主开发分支，只做提交，不做修改）

#### Feature(dev)分支- 功能分支 （基于 Develop 分支创建一个 Feature 分支，开发一个新的功能，合并回 Develop 分支）

#### Hotfix 分支 - Bug 分支 （合并回 Master 和 Develop 分支）

#### Release 分支 - 发布分支 （基于 Develop 分支创建一个 Release 分支，完成 Release 后，我们合并到 Master 和 Develop 分支）

/*
 * @Author: your name
 * @Date: 2019-11-26 15:33:24
 * @LastEditTime: 2020-11-05 15:53:07
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \mini-admin\vue.demo.js
 */

const path = require("path");
function resolve(dir) {
  return path.join(__dirname, dir);
}
console.log(process.env.VUE_APP_SERVER);
console.log(process.env.VUE_APP_SALE);
console.log(process.env.VUE_APP_SYSTEMNO);
module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "/" : "/",
  devServer: {
    proxy: {
      "/api": {
        target: process.env.VUE_APP_SERVER,
        changeOrigin: true
      }
    },
    overlay: {
      warnings: true,
      errors: true
    }
  },
  lintOnSave: false,
  transpileDependencies: ["vue-echarts", "resize-detector"]
  // chainWebpack: config => {
  //   config.resolve.alias
  //     .set("@$", resolve("src"))
  //     .set("@/store", resolve("src/store"))
  //     .set("@/components", resolve("src/components"))
  //     .set("@/router", resolve("src/router"))
  //     .set("@/assets", resolve("src/assets"))
  // }
};

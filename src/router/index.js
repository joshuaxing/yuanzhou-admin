import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store/index";

Vue.use(VueRouter);

const routes = [
  // {
  //   path: "/",
  //   name: "index",
  //   redirect: "/hotel"
  // },
  {
    path: "/login",
    name: "login",
    component: () => import("../views/login/index.vue")
  },
  {
    path: "/login2",
    name: "login2",
    component: () => import("../views/login2/index.vue")
  },
  {
    path: "/",
    component: () => import("../views/layout/index.vue"),
    children: [
      {
        path: "/hotel",
        name: "hotel",
        component: () => import("../views/hotel/index.vue"),
        meta: {
          title: "基础信息",
          requiresAuth: true
        }
      },
      {
        path: "/roomprice",
        name: "roomprice",
        component: () => import("../views/roomprice/index.vue"),
        meta: {
          title: "房价码管理",
          requiresAuth: true
        }
      },
      {
        path: "/roomstyle",
        name: "roomstyle",
        component: () => import("../views/roomstyle/index.vue"),
        meta: {
          title: "房型管理",
          requiresAuth: true
        }
      },
      {
        path: "/hotellabel",
        name: "hotellabel",
        component: () => import("../views/hotellabel/index.vue"),
        meta: {
          title: "标签管理",
          requiresAuth: true
        }
      },
      {
        path: "/facilities",
        name: "facilities",
        component: () => import("../views/facilities/index.vue"),
        meta: {
          title: "设施管理",
          requiresAuth: true
        }
      },
      {
        path: "/brand",
        name: "brand",
        component: () => import("../views/brand/index.vue"),
        meta: {
          title: "品牌管理",
          requiresAuth: true
        }
      },
      {
        path: "/rulecancel",
        name: "rulecancel",
        component: () => import("../views/rulecancel/index.vue"),
        meta: {
          title: "取消规则",
          requiresAuth: true
        }
      },
      {
        path: "/commentpend",
        name: "commentpend",
        component: () => import("../views/commentpend/index.vue"),
        meta: {
          title: "评论列表",
          requiresAuth: true
        }
      },
      {
        path: "/commentmould",
        name: "commentmould",
        component: () => import("../views/commentmould/index.vue"),
        meta: {
          title: "回复模板设置",
          requiresAuth: true
        }
      },
      {
        path: "/channelbase",
        name: "channelbase",
        component: () => import("../views/channelbase/index.vue"),
        meta: {
          title: "渠道基础信息",
          requiresAuth: true
        }
      },
      {
        path: "/crsbaseprice",
        name: "crsbaseprice",
        component: () => import("../views/crsbaseprice/index.vue"),
        meta: {
          title: "房价基价管理",
          requiresAuth: true
        }
      },
      {
        path: "/crsprice",
        name: "crsprice",
        component: () => import("../views/crsprice/index.vue"),
        meta: {
          title: "房价管理",
          requiresAuth: true
        }
      },
      {
        path: "/rulebuild",
        name: "rulebuild",
        component: () => import("../views/rulebuild/index.vue"),
        meta: {
          title: "预定规则设置",
          requiresAuth: true
        }
      },
      {
        path: "/crsroom",
        name: "crsroom",
        component: () => import("../views/crsroom/index.vue"),
        meta: {
          title: "房态管理",
          requiresAuth: true
        }
      },
      {
        path: "/order",
        name: "order",
        component: () => import("../views/order/index.vue"),
        meta: {
          title: "订单查询",
          requiresAuth: true
        }
      },
      {
        path: "/member",
        name: "member",
        component: () => import("../views/member/index.vue"),
        meta: {
          title: "会员查询",
          requiresAuth: true
        }
      },
      {
        path: "/memberlevel",
        name: "memberlevel",
        component: () => import("../views/memberlevel/index.vue"),
        meta: {
          title: "会员等级配置",
          requiresAuth: true
        }
      },
      {
        path: "/memberjifenexchange",
        name: "memberjifenexchange",
        component: () => import("../views/memberjifenexchange/index.vue"),
        meta: {
          title: "积分兑换配置",
          requiresAuth: true
        }
      },
      {
        path: "/memberlevelrule",
        name: "memberlevelrule",
        component: () => import("../views/memberlevelrule/index.vue"),
        meta: {
          title: "自动升级配置",
          requiresAuth: true
        }
      },
      {
        path: "/memberlevelset",
        name: "memberlevelset",
        component: () => import("../views/memberlevelset/index.vue"),
        meta: {
          title: "会员权益配置",
          requiresAuth: true
        }
      },
      {
        path: "/membercard",
        name: "membercard",
        component: () => import("../views/membercard/index.vue"),
        meta: {
          title: "会员模板配置",
          requiresAuth: true
        }
      },
      {
        path: "/marketroomprice",
        name: "marketroomprice",
        component: () => import("../views/marketroomprice/index.vue"),
        meta: {
          title: "房价立减活动",
          requiresAuth: true
        }
      },
      {
        path: "/marketnewpeople",
        name: "marketnewpeople",
        component: () => import("../views/marketnewpeople/index.vue"),
        meta: {
          title: "新人特惠活动",
          requiresAuth: true
        }
      },
      {
        path: "/marketcontinous",
        name: "marketcontinous",
        component: () => import("../views/marketcontinous/index.vue"),
        meta: {
          title: "连住早订活动",
          requiresAuth: true
        }
      },
      {
        path: "/marketjforder",
        name: "marketjforder",
        component: () => import("../views/marketjforder/index.vue"),
        meta: {
          title: "积分预定活动",
          requiresAuth: true
        }
      },
      {
        path: "/memeberprice",
        name: "memeberprice",
        component: () => import("../views/memeberprice/index.vue"),
        meta: {
          title: "会员价",
          requiresAuth: true
        }
      },
      {
        path: "/upgrade",
        name: "upgrade",
        component: () => import("../views/upgrade/index.vue"),
        meta: {
          title: "升级有礼",
          requiresAuth: true
        }
      },
      {
        path: "/role",
        name: "role",
        component: () => import("../views/role/index.vue"),
        meta: {
          title: "角色管理",
          requiresAuth: true
        }
      },
      {
        path: "/employee",
        name: "employee",
        component: () => import("../views/employee/index.vue"),
        meta: {
          title: "用户管理",
          requiresAuth: true
        }
      },
      {
        path: "/couponuse",
        name: "couponuse",
        component: () => import("../views/couponuse/index.vue"),
        meta: {
          title: "优惠券应用",
          requiresAuth: true
        }
      },
      {
        path: "/couponwriteoff",
        name: "couponwriteoff",
        component: () => import("../views/couponwriteoff/index.vue"),
        meta: {
          title: "优惠券核销",
          requiresAuth: true
        }
      },
      {
        path: "/couponsend",
        name: "couponsend",
        component: () => import("../views/couponsend/index.vue"),
        meta: {
          title: "优惠券发放",
          requiresAuth: true
        }
      },
      {
        path: "/couponsource",
        name: "couponsource",
        component: () => import("../views/couponsource/index.vue"),
        meta: {
          title: "优惠券来源",
          requiresAuth: true
        }
      },
      {
        path: "/salestatement",
        name: "salestatement",
        component: () => import("../views/salestatement/index.vue"),
        meta: {
          title: "分销报表",
          requiresAuth: true
        }
      },
      {
        path: "/ythgzh",
        name: "ythgzh",
        component: () => import("../views/salegzh/index.vue"),
        meta: {
          title: "公众号推广",
          requiresAuth: true
        }
      },
      {
        path: "/gainmember",
        name: "gainmember",
        component: () => import("../views/salemember/index.vue"),
        meta: {
          title: "会员拉新",
          requiresAuth: true
        }
      },
      {
        path: "/goldsale",
        name: "goldsale",
        component: () => import("../views/salegold/index.vue"),
        meta: {
          title: "金卡会籍",
          requiresAuth: true
        }
      },
      {
        path: "/bindmoneycard",
        name: "bindmoneycard",
        component: () => import("../views/bindmoneycard/index.vue"),
        meta: {
          title: "储值卡绑定",
          requiresAuth: true
        }
      },
      {
        path: "/logmoneysave",
        name: "logmoneysave",
        component: () => import("../views/logmoneysave/index.vue"),
        meta: {
          title: "会员储值记录",
          requiresAuth: true
        }
      },
      {
        path: "/logbuygold",
        name: "logbuygold",
        component: () => import("../views/logbuygold/index.vue"),
        meta: {
          title: "金卡购买记录",
          requiresAuth: true
        }
      },
      {
        path: "/saleemployee",
        name: "saleemployee",
        component: () => import("../views/saleemployee/index.vue"),
        meta: {
          title: "分销员工",
          requiresAuth: true
        }
      },
      {
        path: "/typebase",
        name: "typebase",
        component: () => import("../views/storetypebase/index.vue"),
        meta: {
          title: "类目基础信息",
          requiresAuth: true
        }
      },
      {
        path: "/storegroupbase",
        name: "storegroupbase",
        component: () => import("../views/storegroupbase/index.vue"),
        meta: {
          title: "分组基础信息",
          requiresAuth: true
        }
      },
      {
        path: "/storecommon",
        name: "storecommon",
        component: () => import("../views/storecommon/index.vue"),
        meta: {
          title: "通用模块",
          requiresAuth: true
        }
      },
      {
        path: "/marketroomprice",
        name: "marketroomprice",
        component: () => import("../views/marketroomprice/index.vue"),
        meta: {
          title: "房价立减活动",
          requiresAuth: true
        }
      },
      {
        path: "/marketnewpeople",
        name: "marketnewpeople",
        component: () => import("../views/marketroomprice/index.vue"),
        meta: {
          title: "新人特惠活动",
          requiresAuth: true
        }
      },
      {
        path: "/marketcontinous",
        name: "marketcontinous",
        component: () => import("../views/marketroomprice/index.vue"),
        meta: {
          title: "连住早订活动",
          requiresAuth: true
        }
      },
      {
        path: "/marketjforder",
        name: "marketjforder",
        component: () => import("../views/marketroomprice/index.vue"),
        meta: {
          title: "积分预定活动",
          requiresAuth: true
        }
      },
      {
        path: "/storeorderlist",
        name: "storeorderlist",
        component: () => import("../views/storeorderlist/index.vue"),
        meta: {
          title: "订单列表",
          requiresAuth: true
        }
      },
      {
        path: "/storesaleservice",
        name: "storesaleservice",
        component: () => import("../views/storesaleservice/index.vue"),
        meta: {
          title: "售后服务",
          requiresAuth: true
        }
      },
      {
        path: "/storeexchangeset",
        name: "storeexchangeset",
        component: () => import("../views/storeexchangeset/index.vue"),
        meta: {
          title: "退换货设置",
          requiresAuth: true
        }
      },
      {
        path: "/storepushgoods",
        name: "storepushgoods",
        component: () => import("../views/storepushgoods/index.vue"),
        meta: {
          title: "发布商品",
          requiresAuth: true
        }
      },
      {
        path: "/storegoodslist",
        name: "storegoodslist",
        component: () => import("../views/storegoodslist/index.vue"),
        meta: {
          title: "商品管理",
          requiresAuth: true
        }
      },
      {
        path: "/marketbirthday",
        name: "marketbirthday",
        component: () => import("../views/marketbirthday/index.vue"),
        meta: {
          title: "会员日",
          requiresAuth: true
        }
      },
      {
        path: "/reportorder",
        name: "reportorder",
        component: () => import("../views/reportorder/index.vue"),
        meta: {
          title: "订单报表",
          requiresAuth: true
        }
      },
      {
        path: "/storeticket",
        name: "storeticket",
        component: () => import("../views/storeticket/index.vue"),
        meta: {
          title: "票券核销",
          requiresAuth: true
        }
      },
      {
        path: "/ticketcardactivity",
        name: "ticketcardactivity",
        component: () => import("../views/ticketcardactivity/index.vue"),
        meta: {
          title: "活动列表",
          requiresAuth: true
        }
      },
      {
        path: "/ticketcardlist",
        name: "ticketcardlist",
        component: () => import("../views/ticketcardlist/index.vue"),
        meta: {
          title: "消费券列表",
          requiresAuth: true
        }
      },
      {
        path: "/ticketcardorder",
        name: "ticketcardorder",
        component: () => import("../views/ticketcardorder/index.vue"),
        meta: {
          title: "消费券订单",
          requiresAuth: true
        }
      },
      {
        path: "/ssoaccount",
        name: "ssoaccount",
        component: () => import("../views/ssoaccount/index.vue"),
        meta: {
          title: "系统账号管理",
          requiresAuth: true
        }
      },
      {
        path: "/ssoemployee",
        name: "ssoemployee",
        component: () => import("../views/ssoemployee/index.vue"),
        meta: {
          title: "员工管理",
          requiresAuth: true
        }
      },
      {
        path: "/ssosystem",
        name: "ssosystem",
        component: () => import("../views/ssosystem/index.vue"),
        meta: {
          title: "系统管理",
          requiresAuth: true
        }
      },
      {
        path: "/reportrepurchase",
        name: "reportrepurchase",
        component: () => import("../views/reportrepurchase/index.vue"),
        meta: {
          title: "复购率报表",
          requiresAuth: true
        }
      },
      {
        path: "/saleroom",
        name: "saleroom",
        component: () => import("../views/saleroom/index.vue"),
        meta: {
          title: "日历房分销",
          requiresAuth: true
        }
      },
      {
        path: "/reportordernomoney",
        name: "reportordernomoney",
        component: () => import("../views/reportordernomoney/index.vue"),
        meta: {
          title: "订单未收帐报表",
          requiresAuth: true
        }
      },
      {
        path: "/serviceorder",
        name: "serviceorder",
        component: () => import("../views/serviceorder/index.vue"),
        meta: {
          title: "服务订单管理",
          requiresAuth: true
        }
      },
      {
        path: "/serviceclass",
        name: "serviceclass",
        component: () => import("../views/serviceclass/index.vue"),
        meta: {
          title: "班次排班管理",
          requiresAuth: true
        }
      },
      {
        path: "/serviceinfo",
        name: "serviceinfo",
        component: () => import("../views/serviceinfo/index.vue"),
        meta: {
          title: "酒店信息设置",
          requiresAuth: true
        }
      },
      {
        path: "/serviceset",
        name: "serviceset",
        component: () => import("../views/serviceset/index.vue"),
        meta: {
          title: "酒店服务设置",
          requiresAuth: true
        }
      },
      {
        path: "/servicesuggest",
        name: "servicesuggest",
        component: () => import("../views/servicesuggest/index.vue"),
        meta: {
          title: "投诉与建议管理",
          requiresAuth: true
        }
      },
      {
        path: "/servicecomment",
        name: "servicecomment",
        component: () => import("../views/servicecomment/index.vue"),
        meta: {
          title: "服务评价管理",
          requiresAuth: true
        }
      },
      {
        path: "/servicetemplate",
        name: "servicetemplate",
        component: () => import("../views/servicetemplate/index.vue"),
        meta: {
          title: "评价回复模板",
          requiresAuth: true
        }
      },
    ]
  },
  {
    path: "*",
    name: "页面404",
    component: () => import("../views/nomatch/index.vue")
  }
];

let router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});
router.beforeEach((to, from, next) => {
  console.log('beforeEach')
  let userinfo = {
    userid: 0,
    username: "",
    token: "",
    name: "",
    logo: "",
    appList: [],
    roleHotelId: []
  };
  if (localStorage.getItem("yzzhongtai")) {
    const value = localStorage.getItem("yzzhongtai");
    userinfo = JSON.parse(value);
  }
  store.commit("user/gotuserInfo", userinfo);
  //请求参数
  let httpparam = {
    appcode: "",
    modcode: "",
    actcode: ""
  };
  
  if (localStorage.getItem("codezhongtai")) {
    const codevalue = localStorage.getItem("codezhongtai");
    httpparam = JSON.parse(codevalue);
  }

  store.commit("user/gotcodeValue", httpparam);

  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    let data = store.state.user.userinfo;
    if (data.userid) {
      next();
    } else {
      next({
        path: "/login"
      });
    }
  } else {
    next(); // 确保一定要调用 next()
  }
});

// router.beforeResolve((to, from, next) => {
//   if (to.matched.some(record => record.meta.requiresAuth)) {
//     next()
//   } else {
//     next()
//   }

// })

const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err);
};
export default router;

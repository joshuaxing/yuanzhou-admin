/*
 * @Author: your name
 * @Date: 2019-12-02 11:12:00
 * @LastEditTime: 2020-06-03 13:58:18
 * @LastEditors: Please set LastEditors
 * @Description: In member Settings Edit
 * @FilePath: \mini-admin\src\store\modules\market.js
 */

import * as api from "@/api/member";

const state = {};

const mutations = {};

const actions = {
  // 会员分页查询
  memberList({ commit }, data) {
    return api.memberList(data);
  },
  // 会员详情基本信息
  memberBasicinfo({ commit }, data) {
    return api.memberBasicinfo(data);
  },
  // 会员详情权益信息
  memberBenefits({ commit }, data) {
    return api.memberBenefits(data);
  },
  // 会员详情明细
  memberDetail({ commit }, data) {
    return api.memberDetail(data);
  },
  // 会员新增
  memberSave({ commit }, data) {
    return api.memberSave(data);
  },
  // 会员详情积分信息分页查询
  memberScore({ commit }, data) {
    return api.memberScore(data);
  },
  // 会员详情消费信息分页查询
  memberConsume({ commit }, data) {
    return api.memberConsume(data);
  },
  // 会员详情消费明细统计
  memberAmount({ commit }, data) {
    return api.memberAmount(data);
  },
  // 会员详情积分细统计
  memberUsedscore({ commit }, data) {
    return api.memberUsedscore(data);
  },
  // 会员等级下拉
  memberCardLevel({ commit }, data) {
    return api.memberCardLevel(data);
  },
  // 会员等级详情
  memberLevelDetail({ commit }, data) {
    return api.memberLevelDetail(data);
  },
  // 会员等级配置分页查询
  memberLevelList({ commit }, data) {
    return api.memberLevelList(data);
  },
  // 会员等级配置添加
  memberLevelSave({ commit }, data) {
    return api.memberLevelSave(data);
  },
  // 会员等级配置编辑
  memberLevelEdit({ commit }, data) {
    return api.memberLevelEdit(data);
  },
  // 会员卡配置下拉
  memberCardSelect({ commit }, data) {
    return api.memberCardSelect(data);
  },
  // 等级特权下拉
  memberLevelSetSelect({ commit }, data) {
    return api.memberLevelSetSelect(data);
  },
  // 权益分页查询
  benfitsList({ commit }, data) {
    return api.benfitsList(data);
  },
  //权益新加
  benfitsSave({ commit }, data) {
    return api.benfitsSave(data);
  },
  // 权益修改
  benfitsUpdate({ commit }, data) {
    return api.benfitsUpdate(data);
  },
  // 权益详情
  benfitsDetail({ commit }, data) {
    return api.benfitsDetail(data);
  },
  //积分兑换配置列表
  ruleList({ commit }, data) {
    return api.ruleList(data);
  },
  // 积分兑换配置详细
  ruleDetail({ commit }, data) {
    return api.ruleDetail(data);
  },
  // 积分兑换配置修改
  ruleUpdate({ commit }, data) {
    return api.ruleUpdate(data);
  },
  //会员卡配置分页查询
  styleList({ commit }, data) {
    return api.styleList(data);
  },
  // 会员卡配置新增
  styleSave({ commit }, data) {
    return api.styleSave(data);
  },
  // 会员卡配置编辑
  styleUpdate({ commit }, data) {
    return api.styleUpdate(data);
  },
  // 会员卡配置详情
  styleDetail({ commit }, data) {
    return api.styleDetail(data);
  },
  // 会会员冻结/解冻
  memberFrozen({ commit }, data) {
    return api.memberFrozen(data);
  },
  // 金卡充值记录分页查询
  goldcardList({ commit }, data) {
    return api.goldcardList(data);
  },
  // 储值卡绑定记录分页查询
  cardbindingList({ commit }, data) {
    return api.cardbindingList(data);
  },
  // 储值卡解绑
  cardbindingCancel({ commit }, data) {
    return api.cardbindingCancel(data);
  },
  // 金卡充值记录分页查询
  goldcardList({ commit }, data) {
    return api.goldcardList(data);
  },
  // 金卡充值记录导出
  goldcardListExcel({ commit }, data) {
    return api.goldcardListExcel(data);
  },
  // 会员储值记录分页查询
  depositRecordList({ commit }, data) {
    return api.depositRecordList(data);
  },
  // 冲销
  depositRecordReverse({ commit }, data) {
    return api.depositRecordReverse(data);
  },
  // 会员储值调整
  depositRecordAdd({ commit }, data) {
    return api.depositRecordAdd(data);
  },
  // 根据手机号查询客户姓名，余额信息
  depositRecordInfo({ commit }, data) {
    return api.depositRecordInfo(data);
  },
   // 绑定储值卡
  cardbindingBind({ commit }, data) {
    return api.cardbindingBind(data);
  },
  // 绑卡短信验证码验证
  cardbindingCheckCode({ commit }, data) {
    return api.cardbindingCheckCode(data);
  },
  // 绑卡发送短信验证码
  cardbindingSendCode({ commit }, data) {
    return api.cardbindingSendCode(data);
  },
   // 根据用户主键ID查询手机号
  userPhone({ commit }, data) {
    return api.userPhone(data);
  },
  // 用户手机号重新绑定
  updatePhone({ commit }, data) {
    return api.updatePhone(data);
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};

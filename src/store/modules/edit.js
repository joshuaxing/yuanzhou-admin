/*
 * @Author: your name
 * @Date: 2020-07-23 11:34:48
 * @LastEditTime: 2020-07-31 13:58:03
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-admin\src\store\modules\edit.js
 */
import router from "@/router/index";
const state = {
  detail: null,
  page: 0
};
const mutations = {
  sendData(state, payload) {
    state.detail = payload
    router.push('/storepushgoods')
  },
  savePage (state, payload) {
    console.log(payload)
    state.page = payload
  }
};

const actions = {
  gotId({ dispatch, commit, getters, rootGetters }, data) {
    commit('sendData', data)
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};

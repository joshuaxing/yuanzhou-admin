/*
 * @Author: your name
 * @Date: 2019-12-02 11:12:00
 * @LastEditTime: 2020-05-12 10:31:16
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \mini-admin\src\store\modules\user.js
 */
import * as api from "@/api/comment";

const state = {};

const mutations = {};

const actions = {
  /**
   * 订单评论API
   */
  // 评论分页
  commentList({ commit }, data) {
    return api.commentList(data);
  },
  // 评论详情页
  commentDetail({ commit }, data) {
    return api.commentDetail(data);
  },
  //评论模板查询
  commentTemplateList({ commit }, data) {
    return api.commentTemplateList(data);
  },
  //评论模板详情
  commentTemplateDetail({ commit }, data) {
    return api.commentTemplateDetail(data);
  },
   //评论模板新增
  commentTemplateAdd ({ commit }, data) {
    return api.commentTemplateAdd(data);
  },
  //评论模板编辑
  commentTemplateUpdate({ commit }, data) {
    return api.commentTemplateUpdate(data);
  },
  //根据酒店ID查询对应回复模板
  commentTemplateByHotel({ commit }, data) {
    return api.commentTemplateByHotel(data);
  },
  //酒店回复
  commentReply({ commit }, data) {
    return api.commentReply(data);
  },
  //评论展示
  commentShow({ commit }, data) {
    return api.commentShow(data);
  },
  //评论屏蔽
  commentHide({ commit }, data) {
    return api.commentHide(data);
  },
  //评论删除
  commentDelete({ commit }, data) {
    return api.commentDelete(data);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};


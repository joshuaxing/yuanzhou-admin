/*
 * @Author: your name
 * @Date: 2020-06-24 09:48:52
 * @LastEditTime: 2020-11-12 16:39:12
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-admin\src\store\modules\store.js
 */

import * as api from "@/api/store";

const state = {};

const mutations = {};

const actions = {
  // 自定义类目分页查询
  categoryList({ commit }, data) {
    return api.categoryList(data);
  },
  // 自定义类目保存
  categorySave({ commit }, data) {
    return api.categorySave(data);
  },
  // 自定义类目编辑
  categoryUpdate({ commit }, data) {
    return api.categoryUpdate(data);
  },
  // 上级分类下拉框
  categoryPull({ commit }, data) {
    return api.categoryPull(data);
  },
  // 类目删除
  categoryDelete({ commit }, data) {
    return api.categoryDelete(data);
  },
  // 根据父类目ID查询子类目
  categoryChild({ commit }, data) {
    return api.categoryChild(data);
  },
  // 首页轮播图显示
  bannerConfigList({ commit }, data) {
    return api.bannerConfigList(data);
  },
  // 首页轮播图添加
  bannerConfigSave({ commit }, data) {
    return api.bannerConfigSave(data);
  },
  // 首页轮播图删除
  bannerConfigDelete({ commit }, data) {
    return api.bannerConfigDelete(data);
  },
  // 导航列表显示
  navigationConfigList({ commit }, data) {
    return api.navigationConfigList(data);
  },
  // 导航列表添加
  navigationConfigSave({ commit }, data) {
    return api.navigationConfigSave(data);
  },
  // 导航列表修改
  navigationConfigUpdate({ commit }, data) {
    return api.navigationConfigUpdate(data);
  },
  // 导航列表显示
  navigationConfigDetail({ commit }, data) {
    return api.navigationConfigDetail(data);
  },
  // 导航列表删除
  navigationConfigDelete({ commit }, data) {
    return api.navigationConfigDelete(data);
  },

  // 物流模板分页查询
  logisticsList({ commit }, data) {
    return api.logisticsList(data);
  },
  // 物流模板发布
  logisticsSave({ commit }, data) {
    return api.logisticsSave(data);
  },
  // 物流模板编辑
  logisticsUpdate({ commit }, data) {
    return api.logisticsUpdate(data);
  },
  // 物流模板删除
  logisticsDelete({ commit }, data) {
    return api.logisticsDelete(data);
  },
  // 物流模板详情
  logisticsDetail({ commit }, data) {
    return api.logisticsDetail(data);
  },
  // 物流模板下拉
  logisticsPull({ commit }, data) {
    return api.logisticsPull(data);
  },
  // 分组分页查询
  groupsList({ commit }, data) {
    return api.groupsList(data);
  },
  // 分组保存
  groupsSave({ commit }, data) {
    return api.groupsSave(data);
  },
  // 分组修改
  groupsUpdate({ commit }, data) {
    return api.groupsUpdate(data);
  },
  // 分组删除
  groupsDelete({ commit }, data) {
    return api.groupsDelete(data);
  },
  // 分组详情
  groupsDetail({ commit }, data) {
    return api.groupsDetail(data);
  },
  // 分组下拉
  groupsPull({ commit }, data) {
    return api.groupsPull(data);
  },
  // 真实商品发布
  goodsSave({ commit }, data) {
    return api.goodsSave(data);
  },
  // 真实商品发布保存
  goodsSave2({ commit }, data) {
    return api.goodsSave2(data);
  },
  // 票券发布
  goodsTicketsSave({ commit }, data) {
    return api.goodsTicketsSave(data);
  },
  // 票券发布保存
  goodsTicketsSave2({ commit }, data) {
    return api.goodsTicketsSave2(data);
  },
  // 预订须知保存
  shoppingconfigBook({ commit }, data) {
    return api.shoppingconfigBook(data);
  },
  // 通知列表
  shoppingconfigNotice({ commit }, data) {
    return api.shoppingconfigNotice(data);
  },
  // 通知栏保存
  shoppingconfigSave({ commit }, data) {
    return api.shoppingconfigSave(data);
  },
  // 显示通知栏
  shoppingconfigShow({ commit }, data) {
    return api.shoppingconfigShow(data);
  },
  // 隐藏通知栏
  shoppingconfigUpdate({ commit }, data) {
    return api.shoppingconfigUpdate(data);
  },
  // 商品列表
  goodsList({ commit }, data) {
    return api.goodsList(data);
  },
  // 商品列表导出
  goodsListExcel({ commit }, data) {
    return api.goodsListExcel(data);
  },
  // 商品编辑
  goodsUpdate({ commit }, data) {
    return api.goodsUpdate(data);
  },
  // 商品编辑上架
  goodsUpdate2({ commit }, data) {
    return api.goodsUpdate2(data);
  },
  // 票券详情查询
  goodsDetail({ commit }, data) {
    return api.goodsDetail(data);
  },
  // 商品详情
  goodsTicketsDetail({ commit }, data) {
    return api.goodsTicketsDetail(data);
  },
  // 商品下架
  editgoodsDown({ commit }, data) {
    return api.editgoodsDown(data);
  },
  // 商品类别修改
  editgoodsCategory({ commit }, data) {
    return api.editgoodsCategory(data);
  },
  // 商品删除
  editgoodsDelete({ commit }, data) {
    return api.editgoodsDelete(data);
  },
  // 商品分组
  editgoodsGroup({ commit }, data) {
    return api.editgoodsGroup(data);
  },
  // 商品运费修改
  editgoodsLogistics({ commit }, data) {
    return api.editgoodsLogistics(data);
  },
  // 商品修改价格
  editgoodsPrice({ commit }, data) {
    return api.editgoodsPrice(data);
  },
  // 商品修改库存
  editgoodsStock({ commit }, data) {
    return api.editgoodsStock(data);
  },
  // 商品置顶
  editgoodsTop({ commit }, data) {
    return api.editgoodsTop(data);
  },
  // 商品上架
  editgoodsUp({ commit }, data) {
    return api.editgoodsUp(data);
  },
  // 添加商品获取主键ID
  goodsID({ commit }, data) {
    return api.goodsID(data);
  },
  // 商城订单列表分页查询
  shopOrderList({ commit }, data) {
    return api.shopOrderList(data);
  },
  // 商城订单详情查询
  shopOrderDetail({ commit }, data) {
    return api.shopOrderDetail(data);
  },
  // 商城订单发货接口
  shopOrderSend({ commit }, data) {
    return api.shopOrderSend(data);
  },
  // 商城订单退款接口
  shopOrderBack({ commit }, data) {
    return api.shopOrderBack(data);
  },
  // 商城订单添加备注接口
  shopOrderRemark({ commit }, data) {
    return api.shopOrderRemark(data);
  },
  // 商城订单操作日志
  shopOrderLog({ commit }, data) {
    return api.shopOrderLog(data);
  },
  // 商城订单修改
  shopOrderUpdate({ commit }, data) {
    return api.shopOrderUpdate(data);
  },
  // 取消订单
  shopOrderCancle({ commit }, data) {
    return api.shopOrderCancle(data);
  },
  // 券码核销分页查询
  shopOrderTicket({ commit }, data) {
    return api.shopOrderTicket(data);
  },
  // 商城票券消费核销接口
  shopOrderTicketAgree({ commit }, data) {
    return api.shopOrderTicketAgree(data);
  },
  // 商城票券退款核销接口
  shopOrderTicketAgree2({ commit }, data) {
    return api.shopOrderTicketAgree2(data);
  },
  // 商城票券过期票券核销接口
  shopOrderTicketAgree3({ commit }, data) {
    return api.shopOrderTicketAgree3(data);
  },
  // 商城票券券核列表导出
  shopOrderTicketExcel({ commit }, data) {
    return api.shopOrderTicketExcel(data);
  },
  // 订单列表label数量
  shopOrderCount({ commit }, data) {
    return api.shopOrderCount(data);
  },
  // 订单列表label数量
  goodsCount({ commit }, data) {
    return api.goodsCount(data);
  },
  // 获取省级行政单位
  shopProvince({ commit }, data) {
    return api.shopProvince(data);
  },
  // 获取城市
  shopCity({ commit }, data) {
    return api.shopCity(data);
  },
  // 获取区
  shopDistrict({ commit }, data) {
    return api.shopDistrict(data);
  },
  // 支持商户
  goodsMerchant({ commit }, data) {
    return api.goodsMerchant(data);
  },
  // 订单导出
  shopOrderExcel({ commit }, data) {
    return api.shopOrderExcel(data);
  },
  // 票券拆分
  ticketSplit({ commit }, data) {
    return api.ticketSplit(data);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};

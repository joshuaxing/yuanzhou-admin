/*
 * @Author: your name
 * @Date: 2019-12-04 10:21:39
 * @LastEditTime: 2020-05-12 10:35:48
 * @LastEditors: Please set LastEditors
 * @Description: 渠道管理
 * @FilePath: \yuanzhou-admin\src\store\modules\crspirce.js
 */
import * as api from "@/api/crspirce";

const state = {};

const mutations = {};

const actions = {
  // 渠道基础信息分页查询
  channelList({ commit }, data) {
    return api.channelList(data);
  },

  //获取渠道详情
  channelDetail({ commit }, data) {
    return api.channelDetail(data);
  },

  //渠道更新
  channelUpdate({ commit }, data) {
    return api.channelUpdate(data);
  },

  //渠道新增
  channelSave({ commit }, data) {
    return api.channelSave(data);
  },

  // 房价基价列表
  crsbasepirceList({ commit }, data) {
    return api.crsbasepirceList(data);
  },
  // 房价基价新增
  crsbasepirceAdd({ commit }, data) {
    return api.crsbasepirceAdd(data);
  },
  // 房价基价修改
  crsbasepirceEdit({ commit }, data) {
    return api.crsbasepirceEdit(data);
  },
  // 房价基价详情
  crsbasepirceDetail({ commit }, data) {
    return api.crsbasepirceDetail(data);
  },
  // 根据房型获取库存
  crsbasepirceByroomId({ commit }, data) {
    return api.crsbasepirceByroomId(data);
  },
  // 房价批量修改
  crsroompriceBatchupdate({ commit }, data) {
    return api.crsroompriceBatchupdate(data);
  },
  // 房价列表带分页查询
  roompriceList({ commit }, data) {
    return api.roompriceList(data);
  },
  // 酒店关联房价树节点
  roompriceTreenode({ commit }, data) {
    return api.roompriceTreenode(data);
  },
  //房态管理批量修改
  roomstatusBatchupdate({ commit }, data) {
    return api.roomstatusBatchupdate(data);
  },
  //房态管理列表查询
  roomstatusList({ commit }, data) {
    return api.roomstatusList(data);
  },
  //渠道下拉
  channelListSelect({ commit }, data) {
    return api.channelListSelect(data);
  },
  //预定规则设置分页查询
  bookingruleList({ commit }, data) {
    return api.bookingruleList(data);
  },
  //预定规则添加
  bookingruleSave({ commit }, data) {
    return api.bookingruleSave(data);
  },
  //预定规则详情
  bookingruleDetail({ commit }, data) {
    return api.bookingruleDetail(data);
  },
  //预定规则编辑
  bookingruleEdit({ commit }, data) {
    return api.bookingruleEdit(data);
  },
   //预定规则下拉
  bookingruleSelect ({ commit }, data) {
    return api.bookingruleSelect(data);
  },
   //取消规则下拉
  cancelruleSelect ({ commit }, data) {
    return api.cancelruleSelect(data);
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};

/*
 * @Author: your name
 * @Date: 2020-12-01 11:17:20
 * @LastEditTime: 2020-12-25 13:19:42
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-admin\src\store\modules\service.js
 */
import * as api from "@/api/service";

const state = {};

const mutations = {};

const actions = {
  // 客房服务评论模板分页查询
  replyList({ commit }, data) {
    return api.replyList(data);
  },
  // 客房服务评论模板分页查询
  replyDetail({ commit }, data) {
    return api.replyDetail(data);
  },
  // 客房服务评论模板添加
  replySave({ commit }, data) {
    return api.replySave(data);
  },
  // 客房服务评论模板修改
  replyUpdate({ commit }, data) {
    return api.replyUpdate(data);
  },
  //客房服务项目列表
  serviceList({ commit }, data) {
    return api.serviceList(data);
  },
  // 酒店服务模板详情
  hotelServiceDetail({ commit }, data) {
    return api.hotelServiceDetail(data);
  },
  // 酒店服务模板修改
  hotelServiceUpdate({ commit }, data) {
    return api.hotelServiceUpdate(data);
  },
  // 清扫服务模板详情
  cleanServiceDetail({ commit }, data) {
    return api.cleanServiceDetail(data);
  },
  // 酒店服务模板修改
  cleanServiceUpdate({ commit }, data) {
    return api.cleanServiceUpdate(data);
  },
  // 接送服务模板详情
  carServiceDetail({ commit }, data) {
    return api.carServiceDetail(data);
  },
  // 接送服务模板修改
  carServiceUpdate({ commit }, data) {
    return api.carServiceUpdate(data);
  },
  // 物品租赁模板详情
  leaseServiceDetail({ commit }, data) {
    return api.leaseServiceDetail(data);
  },
  // 物品租赁模板修改
  leaseServiceUpdate({ commit }, data) {
    return api.leaseServiceUpdate(data);
  },
  // 商城模板详情
  shopServiceDetail({ commit }, data) {
    return api.shopServiceDetail(data);
  },
  // 商城模板修改
  shopServiceUpdate({ commit }, data) {
    return api.shopServiceUpdate(data);
  },
  // 一键WIFI模板详情
  wifiServiceDetail({ commit }, data) {
    return api.wifiServiceDetail(data);
  },
  // 一键WIFI模板修改
  wifiServiceUpdate({ commit }, data) {
    return api.wifiServiceUpdate(data);
  },
  // 回复模板下拉
  replypull({ commit }, data) {
    return api.replypull(data);
  },
  // 根据酒店ID查询楼号
  hotelbuildno({ commit }, data) {
    return api.hotelbuildno(data);
  },
  // 根据酒店ID 楼号 查询楼层
  hotelfloor({ commit }, data) {
    return api.hotelfloor(data);
  },
  // 根据酒店ID  楼号 楼层 查询房间号
  hotelroomno({ commit }, data) {
    return api.hotelroomno(data);
  },
  // 客房服务类别分页查询
  templateList({ commit }, data) {
    return api.templateList(data);
  },
  // 客房服务订单派单
  orderSend({ commit }, data) {
    return api.orderSend(data);
  },
  // 客房服务订单取消
  orderCancle({ commit }, data) {
    return api.orderCancle(data);
  },
  // 订单列表
  orderList({ commit }, data) {
    return api.orderList(data);
  },
  // 订单详情
  orderDetail({ commit }, data) {
    return api.orderDetail(data);
  },
  // 评论回复
  orderReply({ commit }, data) {
    return api.orderReply(data);
  },
  // 评论详情
  OrderReplyDetail({ commit }, data) {
    return api.OrderReplyDetail(data);
  },
  // 班次列表新增
  classAdd({ commit }, data) {
    return api.classAdd(data);
  },
  // 班次列表删除
  classDel({ commit }, data) {
    return api.classDel(data);
  },
  // 班次列表分页查询
  classList({ commit }, data) {
    return api.classList(data);
  },
  // 班次列表修改
  classEdit({ commit }, data) {
    return api.classEdit(data);
  },
  // 班次下拉
  classPull({ commit }, data) {
    return api.classPull(data);
  },
  // 员工排班查询
  employeeList({ commit }, data) {
    return api.employeeList(data);
  },
  // 员工排班
  employeeArrange({ commit }, data) {
    return api.employeeArrange(data);
  },
  // 根据二级组织ID查询部门
  employeeDept({ commit }, data) {
    return api.employeeDept(data);
  },
  // 根据酒店ID查询二级组织
  employeeSecond({ commit }, data) {
    return api.employeeSecond(data);
  },
  // 生成酒店二维码
  hotelqrcode({ commit }, data) {
    return api.hotelqrcode(data);
  },
 
  // 服务流程配置列表
  processList({ commit }, data) {
    return api.processList(data);
  },
  // 服务流程配置详情
  processDetail({ commit }, data) {
    return api.processDetail(data);
  },
  // 流程节点编辑
  processEdit({ commit }, data) {
    return api.processEdit(data);
  },
   // 流程节点添加
   processAdd({ commit }, data) {
    return api.processAdd(data);
  },
  // 流程节点删除
  processDel({ commit }, data) {
    return api.processDel(data);
  },
  // 根据二级组织ID查询岗位
  employeePost({ commit }, data) {
    return api.employeePost(data);
  },
  // 服务流程配置列表下拉
  processPull({ commit }, data) {
    return api.processPull(data);
  },
  // 投诉与建议分页查询
  suggestList({ commit }, data) {
    return api.suggestList(data);
  },
  // 投诉与建议详情
  suggestDetail({ commit }, data) {
    return api.suggestDetail(data);
  },
  // 建议反馈回复
  suggestReply({ commit }, data) {
    return api.suggestReply(data);
  },
  // 排班人员楼层查询
  floorList({ commit }, data) {
    return api.floorList(data);
  },
  // 排班人员楼层修改
  floorUpdate({ commit }, data) {
    return api.floorUpdate(data);
  },
  // 排班人员楼层添加
  floorAddUpdate({ commit }, data) {
    return api.floorAddUpdate(data);
  },
  // 客房服务项目是否显示
  serviceShow({ commit }, data) {
    return api.serviceShow(data);
  },
  // 员工取消排班
  employeeArrangeCancle({ commit }, data) {
    return api.employeeArrangeCancle(data);
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};

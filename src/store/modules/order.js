/*
 * @Author: your name
 * @Date: 2019-12-04 10:21:39
 * @LastEditTime: 2020-12-21 11:20:32
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-admin\src\store\modules\order.js
 */

import * as api from "@/api/order";

const state = {};

const mutations = {};

const actions = {
  // 订单详情
  list({ commit }, data) {
    return api.list(data);
  },
  // 订单详情
  detail({ commit }, data) {
    return api.detail(data);
  },
  // 退款审核通过
  check({ commit }, data) {
    return api.check(data);
  },
  // 退款审核拒绝
  reject({ commit }, data) {
    return api.reject(data);
  },
  // 订单取消
  cancle({ commit }, data) {
    return api.cancle(data);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};

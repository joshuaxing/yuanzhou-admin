/*
 * @Author: your name
 * @Date: 2020-03-25 09:52:53
 * @LastEditTime: 2020-11-03 15:22:38
 * @LastEditors: Please set LastEditors
 * @Description: 分销管理API
 * @FilePath: \yuanzhou-admin\src\store\modules\sale.js
 */

import * as api from "@/api/sale";

const state = {};

const mutations = {};

const actions = {
  // 人员审核分页查询
  saleCheckList({ commit }, data) {
    return api.saleCheckList(data);
  },
  // 人员审核批量通过
  saleCheckPass({ commit }, data) {
    return api.saleCheckPass(data);
  },
  // 人员审核批量拒绝
  saleCheckReject({ commit }, data) {
    return api.saleCheckReject(data);
  },
  //人员列表分页查询
  salePeopleList({ commit }, data) {
    return api.salePeopleList(data);
  },
  // 人员列表批量冻结
  salePeopleFrozen({ commit }, data) {
    return api.salePeopleFrozen(data);
  },
  // 推广结算设置详情修改
  saleDetailEdit({ commit }, data) {
    return api.saleDetailEdit(data);
  },
  // 推广结算设置详情修改
  saleDetailData({ commit }, data) {
    return api.saleDetailData(data);
  },
  // 公众号推广是否考核显示
  gzhCheck({ commit }, data) {
    return api.gzhCheck(data);
  },
  // 公众号考核审核修改
  gzhCheckEdit({ commit }, data) {
    return api.gzhCheckEdit(data);
  },
  // 小程序会员拉新是否考核显示
  memberCheck({ commit }, data) {
    return api.memberCheck(data);
  },
  // 小程序会员拉新考核审核修改
  memberCheckEdit({ commit }, data) {
    return api.memberCheckEdit(data);
  },
  // 金卡会籍是否考核显示
  goldCheck({ commit }, data) {
    return api.goldCheck(data);
  },
  // 金卡会籍考核审核修改
  goldCheckEdit({ commit }, data) {
    return api.goldCheckEdit(data);
  },
  // 关注公众号统计详细数据列表
  gzhDataList({ commit }, data) {
    return api.gzhDataList(data);
  },
  // 关注公众号统计详细数据导出
  gzhDataListExcel({ commit }, data) {
    return api.gzhDataListExcel(data);
  },
  // 关注公众号统计汇总数据列表
  gzhDataTotal({ commit }, data) {
    return api.gzhDataTotal(data);
  },
  // 关注公众号统计汇总数据导出
  gzhDataTotalExcel({ commit }, data) {
    return api.gzhDataTotalExcel(data);
  },
  // 新会员统计详细数据列表查询
  memberDataList({ commit }, data) {
    return api.memberDataList(data);
  },
  // 新会员统计详细数据列表导出
  memberDataListExcel({ commit }, data) {
    return api.memberDataListExcel(data);
  },
  // 新会员统计汇总数据列表查询
  memberDataTotal({ commit }, data) {
    return api.memberDataTotal(data);
  },
  // 新会员统计汇总数据列表导出
  memberDataTotalExcel({ commit }, data) {
    return api.memberDataTotalExcel(data);
  },
  // 金卡会籍分销统计详细数据
  goldDataList({ commit }, data) {
    return api.goldDataList(data);
  },
  // 金卡会籍分销统计详细导出
  goldDataListExcel({ commit }, data) {
    return api.goldDataListExcel(data);
  },
  // 金卡会籍分销统计汇总数据列表
  goldDataTotal({ commit }, data) {
    return api.goldDataTotal(data);
  },
  // 金卡会籍分销统计汇总数据列表
  goldDataTotalExcel({ commit }, data) {
    return api.goldDataTotalExcel(data);
  },
  // 推广人员全员显示(HR系统未接入过渡期显示)
  saleActivityList({ commit }, data) {
    return api.saleActivityList(data);
  },
  // 推广人员全员显示(HR系统未接入过渡期显示)
  saleActivityList({ commit }, data) {
    return api.saleActivityList(data);
  },
  // 推广人员全员显示(HR系统未接入过渡期显示)
  saleActivityList({ commit }, data) {
    return api.saleActivityList(data);
  },
  // 推广人员全员显示(HR系统未接入过渡期显示)
  saleActivityList({ commit }, data) {
    return api.saleActivityList(data);
  },
  // 日历房是否考核显示
  roomCheck({ commit }, data) {
    return api.roomCheck(data);
  },
  // 日历房考核修改
  roomCheckEdit({ commit }, data) {
    return api.roomCheckEdit(data);
  },
  // 日历房分销活动分页查询
  bookHotelList({ commit }, data) {
    return api.bookHotelList(data);
  },
  // 日历房分销活动添加
  bookHotelSave({ commit }, data) {
    return api.bookHotelSave(data);
  },
  // 日历房分销活动修改
  bookHotelUpdate({ commit }, data) {
    return api.bookHotelUpdate(data);
  },
  // 日历房分销活动修改
  bookHotelDelete({ commit }, data) {
    return api.bookHotelDelete(data);
  },
  // 日历房统计详细数据列表查询
  roomDataList({ commit }, data) {
    return api.roomDataList(data);
  },
  // 日历房统计详细数据列表导出
  roomDataListExcel({ commit }, data) {
    return api.roomDataListExcel(data);
  },
  // 日历房统计汇总数据列表查询
  roomDataTotal({ commit }, data) {
    return api.roomDataTotal(data);
  },
  // 日历房统计汇总数据列表导出
  roomDataTotalExcel({ commit }, data) {
    return api.roomDataTotalExcel(data);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};

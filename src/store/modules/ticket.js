/*
 * @Author: your name
 * @Date: 2020-09-08 09:28:09
 * @LastEditTime: 2020-09-08 13:03:09
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-admin\src\store\modules\ticket.js
 */
import * as api from "@/api/ticket";

const state = {};

const mutations = {};

const actions = {
  // 券列表分页查询
  TicketList({ commit }, data) {
    return api.TicketList(data);
  },
  // 券列表删除
  TicketListDelete({ commit }, data) {
    return api.TicketListDelete(data);
  },
  // 消费券订单列表
  TicketOrderList({ commit }, data) {
    return api.TicketOrderList(data);
  },
  // 消费券订单发货
  TicketOrderSend({ commit }, data) {
    return api.TicketOrderSend(data);
  },
  // 消费券活动分页查询
  TicketActivityList({ commit }, data) {
    return api.TicketActivityList(data);
  },
  // 消费券活动接口添加
  TicketActivityAdd({ commit }, data) {
    return api.TicketActivityAdd(data);
  },
  // 消费券活动修改
  TicketActivityUpdate({ commit }, data) {
    return api.TicketActivityUpdate(data);
  },
  // 消费券活动导入消费券
  TicketActivityLead({ commit }, data) {
    return api.TicketActivityLead(data);
  },
  // 消费券活动详情
  TicketActivityDetail({ commit }, data) {
    return api.TicketActivityDetail(data);
  },
   // 消费券活动详情
  ticketActivitySelect({ commit }, data) {
    return api.ticketActivitySelect(data);
  },
  // 消费券订单导出
  TicketOrderExport({ commit }, data) {
    return api.TicketOrderExport(data);
  },
  // 券列表批量删除
  TicketListDeleteAll({ commit }, data) {
    return api.TicketListDeleteAll(data);
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};

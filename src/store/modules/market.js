/*
 * @Author: your name
 * @Date: 2019-12-02 11:12:00
 * @LastEditTime: 2020-07-02 10:51:28
 * @LastEditors: Please set LastEditors
 * @Description: In member Settings Edit
 * @FilePath: \mini-admin\src\store\modules\market.js
 */

import * as api from "@/api/market";

const state = {};

const mutations = {};

const actions = {
  //会员价分页查询
  activityMemberList({ commit }, data) {
    return api.activityMemberList(data);
  },
  //会员价明细
  activityMemberDetail({ commit }, data) {
    return api.activityMemberDetail(data);
  },
  //所有会员卡
  cardAll({ commit }, data) {
    return api.cardAll(data);
  },
  //保存会员价编辑
  activityMemberUpdate({ commit }, data) {
    return api.activityMemberUpdate(data);
  },
  //保存会员价新增
  activityMemberSave({ commit }, data) {
    return api.activityMemberSave(data);
  },
  //删除会员价
  activityMemberDelete({ commit }, data) {
    return api.activityMemberDelete(data);
  },
  //房价立减活动分页查询
  aRoompriceList({ commit }, data) {
    return api.aRoompriceList(data);
  },
  //房价立减活动详情
  aRoompriceDetail({ commit }, data) {
    return api.aRoompriceDetail(data);
  },
  //房价立减活动新加
  aRoompriceSave({ commit }, data) {
    return api.aRoompriceSave(data);
  },
  //房价立减活动编辑
  aRoompriceEdit({ commit }, data) {
    return api.aRoompriceEdit(data);
  },
  //房价立减活动删除
  aRoompriceDelete({ commit }, data) {
    return api.aRoompriceDelete(data);
  },
  //活动房型管理新增
  activitydecreaseRelation({ commit }, data) {
    return api.activitydecreaseRelation(data);
  },
  //活动房型管理移除
  activitydecreaseRemove({ commit }, data) {
    return api.activitydecreaseRemove(data);
  },
  //连住早定活动分页查询
  aContinousList({ commit }, data) {
    return api.aContinousList(data);
  },
  //连住早定活动详情
  aContinousDetail({ commit }, data) {
    return api.aContinousDetail(data);
  },
  //连住早定活动新加
  aContinousSave({ commit }, data) {
    return api.aContinousSave(data);
  },
  //连住早定活动编辑
  aContinousEdit({ commit }, data) {
    return api.aContinousEdit(data);
  },
  //连住早定活动删除
  aContinousDelete({ commit }, data) {
    return api.aContinousDelete(data);
  },
  //新人特惠活动分页查询
  aNewPeopleList({ commit }, data) {
    return api.aNewPeopleList(data);
  },
  //新人特惠活动详情
  aNewPeopleDetail({ commit }, data) {
    return api.aNewPeopleDetail(data);
  },
  //新人特惠活动新加
  aNewPeopleSave({ commit }, data) {
    return api.aNewPeopleSave(data);
  },
  //新人特惠活动编辑
  aNewPeopleEdit({ commit }, data) {
    return api.aNewPeopleEdit(data);
  },
  //新人特惠活动删除
  aNewPeopleDelete({ commit }, data) {
    return api.aNewPeopleDelete(data);
  },
  //活动关联已选房价数据查询
  activitydecreaseChose({ commit }, data) {
    return api.activitydecreaseChose(data);
  },
  //活动关联未选房价数据查询
  activitydecreaseUnChose({ commit }, data) {
    return api.activitydecreaseUnChose(data);
  },
  //会员日活动详情
  activitymemberdayDetail({ commit }, data) {
    return api.activitymemberdayDetail(data);
  },
  //会员日活动分页查询
  activitymemberdayList({ commit }, data) {
    return api.activitymemberdayList(data);
  },
  //会员日活动添加
  activitymemberdaySave({ commit }, data) {
    return api.activitymemberdaySave(data);
  },
  //会员日活动编辑
  activitymemberdayUpdate({ commit }, data) {
    return api.activitymemberdayUpdate(data);
  },
  //会员日活动删除
  activitymemberdayDelete({ commit }, data) {
    return api.activitymemberdayDelete(data);
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};

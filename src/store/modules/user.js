/*
 * @Author: your name
 * @Date: 2019-12-02 11:12:00
 * @LastEditTime: 2020-11-03 14:35:39
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \mini-admin\src\store\modules\user.js
 */
import * as api from "@/api/user";
const state = {
  userinfo: {
    userid: 0,
    username: '',
    token: '',
    name: '',
    logo: '',
    appList: [],
    roleHotelId: []
  },
  menuList: [],
  modelid: '',
  code: {
    appcode: '',
    modcode: '',
    actcode: ''
  }
};

const mutations = {
  gotuserInfo(state, payload) {
    const roleHotelId = payload.roleHotelId ? payload.roleHotelId : []
    state.userinfo = {
      ...payload,
      roleHotelId: roleHotelId
    };
  },
  gotmenuList(state, payload) {
    const data = payload.value
    state.menuList = data;
  },
  gotmodelId(state, payload) {
    state.modelid = payload.value;
  },
  gotcodeValue(state, payload) {
    state.code = payload;
  }
};

const actions = {
  // user login
  login({ commit }, data) {
    return api.login(data);
  },
  login2({ commit }, data) {
    return api.login2(data);
  },
  logincheck({ commit }, data) {
    return api.logincheck(data);
  },
  // user loginout
  loginout({ commit }, data) {
    return api.loginout(data);
  },
  //用户列表
  userList({ commit }, data) {
    return api.userList(data);
  },
  //用户新增
  userAdd({ commit }, data) {
    return api.userAdd(data);
  },
  //用户详情
  userDetail({ commit }, data) {
    return api.userDetail(data);
  },
  //用户编辑
  userEdit({ commit }, data) {
    return api.userEdit(data);
  },
  //角色列表
  roleList({ commit }, data) {
    return api.roleList(data);
  },
  //角色新增
  roleAdd({ commit }, data) {
    return api.roleAdd(data);
  },
  //角色编辑
  roleEdit({ commit }, data) {
    return api.roleEdit(data);
  },
  //角色详情
  roleDetail({ commit }, data) {
    return api.roleDetail(data);
  },
  //应用树列表
  roleTree({ commit }, data) {
    return api.roleTree(data);
  },
  // model列表
  modelList({ commit }, data) {
    return api.modelList(data);
  },
  // action列表
  modelAction({ commit }, data) {
    return api.modelAction(data);
  },
  //角色下拉
  roleListSelect({ commit }, data) {
    return api.roleListSelect(data);
  },
   //用户修改密码
  userEditPassword({ commit }, data) {
    return api.userEditPassword(data);
  },
   //用户重置密码
  userResetPassword({ commit }, data) {
    return api.userResetPassword(data);
  }
};
export default {
  namespaced: true,
  state,
  mutations,
  actions
};

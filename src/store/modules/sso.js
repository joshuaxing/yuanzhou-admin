/*
 * @Author: your name
 * @Date: 2020-09-16 17:23:47
 * @LastEditTime: 2020-09-18 16:26:14
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-admin\src\store\modules\sso.js
 */
import * as api from "@/api/sso";

const state = {};

const mutations = {};

const actions = {
  // 员工系统映射列表分页查询
  staffmappingList({ commit }, data) {
    return api.staffmappingList(data)
  },
  // 映射删除
  staffmappingDelete({ commit }, data) {
    return api.staffmappingDelete(data)
  },
  // 映射修改
  staffmappingUpdate({ commit }, data) {
    return api.staffmappingUpdate(data)
  },
  // 员工操作列表分页查询
  staffList({ commit }, data) {
    return api.staffList(data)
  },
  // 用户不同系统账号映射
  staffMapping({ commit }, data) {
    return api.staffMapping(data)
  },
  // 业务系统下拉框
  staffPull({ commit }, data) {
    return api.staffPull(data)
  },
  // 员工绑定角色
  staffRole({ commit }, data) {
    return api.staffRole(data)
  },
   // 员工已绑定角色
  staffReled({ commit }, data) {
    return api.staffReled(data)
  },
  // 系统列表分页查询
  systemList({ commit }, data) {
    return api.systemList(data)
  },
  // 系统添加
  systemSave({ commit }, data) {
    return api.systemSave(data)
  },
  // 系统列表修改
  systemUpdate({ commit }, data) {
    return api.systemUpdate(data)
  },
   // 统一账号权限列表分页查询
  rolesList({ commit }, data) {
    return api.rolesList(data)
  },
  // 统一账号角色添加
  rolesSave({ commit }, data) {
    return api.rolesSave(data)
  },
  // 统一账号角色编辑
  rolesUpdate({ commit }, data) {
    return api.rolesUpdate(data)
  },
  // 统一账号角色详情
  rolesDetail({ commit }, data) {
    return api.rolesDetail(data)
  },
  // 统一账号下拉
  rolesPull({ commit }, data) {
    return api.rolesPull(data)
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};

/*
 * @Author: your name
 * @Date: 2020-07-07 18:21:17
 * @LastEditTime: 2020-11-05 15:25:00
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-admin\src\store\modules\report.js
 */

import * as api from "@/api/report";

const state = {};

const mutations = {};

const actions = {
  // 订单报表
  order({ commit }, data) {
    return api.order(data);
  },
  // 报表统计
  ordercount({ commit }, data) {
    return api.ordercount(data);
  },
  // 订单复购率报表
  orderreport({ commit }, data) {
    return api.orderreport(data);
  },
  // 订单未收账列表
  orderunreceive({ commit }, data) {
    return api.orderunreceive(data);
  },
  // 订单未收账报表导出
  orderunreceiveexal({ commit }, data) {
    return api.orderunreceiveexal(data);
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};

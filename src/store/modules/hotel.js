/*
 * @Author: your name
 * @Date: 2019-12-02 11:12:00
 * @LastEditTime: 2020-06-22 17:38:16
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \mini-admin\src\store\modules\user.js
 */
import * as api from "@/api/hotel";

const state = {};

const mutations = {};

const actions = {
  // 酒店列表
  hotelList({ commit }, data) {
    return api.hotelList(data);
  },
  // 酒店下拉
  hotelSelect({ commit }, data) {
    return api.hotelSelect(data);
  },
  // 酒店全部下拉
  hotelSelectAll({ commit }, data) {
    return api.hotelSelectAll(data);
  },
  //酒店品牌列表
  hotelBrand({ commit }, data) {
    return api.hotelBrand(data);
  },
  //酒店地址-省级行政单位
  hotelAddressProvince({ commit }, data) {
    return api.hotelAddressProvince(data);
  },
  //酒店地址-城市
  hotelAddressCity({ commit }, data) {
    return api.hotelAddressCity(data);
  },
  //酒店地址-区
  hotelAddressDistrict({ commit }, data) {
    return api.hotelAddressDistrict(data);
  },
  //酒店信息新增
  hotelSave({ commit }, data) {
    return api.hotelSave(data);
  },
  //酒店信息更新
  hotelEdit({ commit }, data) {
    return api.hotelEdit(data);
  },
  //酒店id获取酒店明细
  hotelDetail({ commit }, data) {
    return api.hotelDetail(data);
  },
  //酒店id获取酒店图片
  hotelImages({ commit }, data) {
    return api.hotelImages(data);
  },
  //上传酒店图片
  hotelImagesSave({ commit }, data) {
    return api.hotelImagesSave(data);
  },
  //删除酒店图片
  hotelImagesDelete({ commit }, data) {
    return api.hotelImagesDelete(data);
  },
  //删除酒店图片
  hotelFacility({ commit }, data) {
    return api.hotelFacility(data);
  },
  //获取设施列表
  hotelFacilityList({ commit }, data) {
    return api.hotelFacilityList(data);
  },
  //保存酒店介绍
  hotelSaveIntroduce({ commit }, data) {
    return api.hotelSaveIntroduce(data);
  },
  //酒店提示关联
  hotelSaveTips({ commit }, data) {
    return api.hotelSaveTips(data);
  },
  //查询酒店提示集合
  hotelGetTips({ commit }, data) {
    return api.hotelGetTips(data);
  },
  //酒店已选标签信息
  hotelCheckLabel({ commit }, data) {
    return api.hotelCheckLabel(data);
  },
  //酒店未选标签信息
  hotelUncheckLabel({ commit }, data) {
    return api.hotelUncheckLabel(data);
  },
  //保存酒店标签
  hotelSaveLabel({ commit }, data) {
    return api.hotelSaveLabel(data);
  },
  //房型&酒店设施更新
  hotelSaveFacility({ commit }, data) {
    return api.hotelSaveFacility(data);
  },

  /**
   * 房型管理API
   */
  //根据id获取房型明细
  hotelRoomDetail({ commit }, data) {
    return api.hotelRoomDetail(data);
  },
  //房型保存修改
  hotelRoomSave({ commit }, data) {
    return api.hotelRoomSave(data);
  },
  //房型保存修改
  hotelRoomEdit({ commit }, data) {
    return api.hotelRoomEdit(data);
  },
  roomGetImages({ commit }, data) {
    return api.roomGetImages(data);
  },
  roomGetFacility({ commit }, data) {
    return api.roomGetFacility(data);
  },

  /**
   * 设施管理API
   */

  // 新增设施
  facilityAdd({ commit }, data) {
    return api.facilityAdd(data);
  },
  // 修改设施
  facilityUpdate({ commit }, data) {
    return api.facilityUpdate(data);
  },

  facilityDetail({ commit }, data) {
    return api.facilityDetail(data);
  },

  /**
   * 品牌管理API
   */
  //分页查询列表信息
  brandList({ commit }, data) {
    return api.brandList(data);
  },
  //根据id查询明细
  brandDetail({ commit }, data) {
    return api.brandDetail(data);
  },
  //根据id查询明细
  brandImages({ commit }, data) {
    return api.brandImages(data);
  },
  //查询代表酒店
  brandHotel({ commit }, data) {
    return api.brandHotel(data);
  },
  //关联代表酒店道品牌
  brandRelation({ commit }, data) {
    return api.brandRelation(data);
  },
  //保存品牌基本信息
  brandUpdate({ commit }, data) {
    return api.brandUpdate(data);
  },
  //新增品牌基本信息
  brandSave({ commit }, data) {
    return api.brandSave(data);
  },

  /**
   * 房价码API
   */
  //查询房价码相关详情
  hotelPriceDetail({ commit }, data) {
    return api.hotelPriceDetail(data);
  },
  //新增房价码
  hotelPriceSave({ commit }, data) {
    return api.hotelPriceSave(data);
  },

  // 房型列表
  roomstyleList({ commit }, data) {
    return api.roomstyleList(data);
  },
  // 房型下拉
  roomstyleSelect({ commit }, data) {
    return api.roomstyleSelect(data);
  },
  // 查询酒店ID对应房型列表下拉
  roomstyleSelectById({ commit }, data) {
    return api.roomstyleSelectById(data);
  },
  // 房价码列表
  roompriceList({ commit }, data) {
    return api.roompriceList(data);
  },
  // 房价码下拉
  roompriceSelect({ commit }, data) {
    return api.roompriceSelect(data);
  },
  // 查询酒店ID 房型ID 对应房价码列表下拉
  roompriceSelectById({ commit }, data) {
    return api.roompriceSelectById(data);
  },
  /**
   * 标签API
   */
  // 标签下拉
  labelList({ commit }, data) {
    return api.labelList(data);
  },
  // 标签新增
  labelAction({ commit }, data) {
    return api.labelAction(data);
  },
  // 标签编辑
  labelEdit({ commit }, data) {
    return api.labelEdit(data);
  },
  // 标签详情
  labeDetail({ commit }, data) {
    return api.labeDetail(data);
  },
  /**
   * 规则API
   */
  //取消规则分页查询
  cancelRuleList({ commit }, data) {
    return api.cancelRuleList(data);
  },
  //取消规则新增
  cancelRuleSave({ commit }, data) {
    return api.cancelRuleSave(data);
  },
  //取消规则详情
  cancelRuleDetail({ commit }, data) {
    return api.cancelRuleDetail(data);
  },
  //取消规则详情
  cancelRuleUpdate({ commit }, data) {
    return api.cancelRuleUpdate(data);
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};

/*
 * @Author: your name
 * @Date: 2019-12-04 10:21:39
 * @LastEditTime: 2020-05-12 10:34:39
 * @LastEditors: Please set LastEditors
 * @Description: 渠道管理
 * @FilePath: \yuanzhou-admin\src\store\modules\crspirce.js
 */
import * as api from "@/api/coupon";

const state = {};

const mutations = {};

const actions = {
  //优惠券来源分页查询
  sourceList({ commit }, data) {
    return api.sourceList(data);
  },
  //优惠券来源详情查询
  sourceDetail({ commit }, data) {
    return api.sourceDetail(data);
  },
  //优惠券来源修改
  sourceUpdate({ commit }, data) {
    return api.sourceUpdate(data);
  },
  //优惠券来源新增
  sourceAdd({ commit }, data) {
    return api.sourceAdd(data);
  },
  //优惠券来源下拉
  sourceSelect({ commit }, data) {
    return api.sourceSelect(data);
  },
  //优惠券来源编码验证
  sourceExist({ commit }, data) {
    return api.sourceExist(data);
  },
  //优惠券分页查询
  couponList({ commit }, data) {
    return api.couponList(data);
  },
  //优惠券详情
  couponDetail({ commit }, data) {
    return api.couponDetail(data);
  },
  //新增优惠券
  couponAdd({ commit }, data) {
    return api.couponAdd(data);
  },
  //编辑优惠券
  couponUpdate({ commit }, data) {
    return api.couponUpdate(data);
  },
  //优惠券应用下拉框
  couponSelect({ commit }, data) {
    return api.couponSelect(data);
  },
  //优惠券发放分页查询
  couponSendList({ commit }, data) {
    return api.couponSendList(data);
  },
  //优惠券发放详情查询
  couponSendDetail({ commit }, data) {
    return api.couponSendDetail(data);
  },
  //根据手机号查询用户ID 用户姓名（判断手机号是否注册）
  couponSendVerify({ commit }, data) {
    return api.couponSendVerify(data);
  },
  //优惠券发放申请驳回
  couponReject({ commit }, data) {
    return api.couponReject(data);
  },
  //优惠券发放申请
  couponApply({ commit }, data) {
    return api.couponApply(data);
  },
  //优惠券发放申请审核同意（同时送券）msg 全员赠送 传 1 单个赠送 传 2
  couponSendAgree({ commit }, data) {
    return api.couponSendAgree(data);
  },
  //优惠券核销分页查询
  usrcouponList({ commit }, data) {
    return api.usrcouponList(data);
  },
  //优惠券核销详情查询
  usrcouponDetail({ commit }, data) {
    return api.usrcouponDetail(data);
  },
  //核销
  usrcouponCheck({ commit }, data) {
    return api.usrcouponCheck(data);
  },
  //作废
  usrcouponCancel({ commit }, data) {
    return api.usrcouponCancel(data);
  },
  // 升级有礼分页查询
  upgradeList({ commit }, data) {
    return api.upgradeList(data);
  },
  // 升级有礼删除
  upgradeDelete({ commit }, data) {
    return api.upgradeDelete(data);
  },
  // 升级有礼详情
  upgradeDetail({ commit }, data) {
    return api.upgradeDetail(data);
  },
  // 升级有礼新增
  upgradeSave({ commit }, data) {
    return api.upgradeSave(data);
  },
  // 升级有礼编辑
  upgradeUpdate({ commit }, data) {
    return api.upgradeUpdate(data);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};

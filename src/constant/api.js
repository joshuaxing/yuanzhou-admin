/*
 * @Author: your name
 * @Date: 2019-12-02 10:43:35
 * @LastEditTime: 2020-04-08 16:49:45
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \mini-admin\src\constant\api.js
 */

// base
const CDN = "http://192.168.199.200:8082";
const baseURL = process.env.NODE_ENV === "production" ? "" : "";
//console.log(baseURL)

/**
 * 酒店基础管路API
 */

// 酒店列表分页查询
export const HOTEL_LIST = `${baseURL}/api/hotel/list`;

// 获取酒店下拉框信息
export const HOTEL_SELECT = `${baseURL}/api/hotel/queryNameAndId`;

//获取酒店品牌列表
export const HOTEL_BRAND = `${baseURL}/api/hotel/brand`;

//获取酒店地址-获取省级行政单位
export const HOTEL_ADDRESS_PROVINCE = `${baseURL}/api/hotel/province`;

//获取酒店地址-获取城市
export const HOTEL_ADDRESS_CITY = `${baseURL}/api/hotel/city`;

//获取酒店地址-获取区
export const HOTEL_ADDRESS_DISTRICT = `${baseURL}/api/hotel/district`;

//酒店信息新增
export const HOTEL_SAVE = `${baseURL}/api/hotel/save`;

//酒店信息更新
export const HOTEL_EDIT = `${baseURL}/api/hotel/update`;

//根据酒店id获取酒店明细
export const HOTEL_DETAIL = `${baseURL}/api/hotel/detail`;

//获取酒店图片
export const HOTEL_IMAGES = `${baseURL}/api/htlResource/hotelImages`;

//上传酒店图片
export const HOTEL_IMAGES_SAVE = `${baseURL}/api/image/saveImg`;

//删除酒店图片
export const HOTEL_IMAGES_DELETE = `${baseURL}/api/image/deleteImages`;


//保存酒店介绍
export const HOTEL_SAVE_INTRODUCE = `${baseURL}/api/hotel/saveIntroduce`;

//酒店提示关联
export const HOTEL_SAVE_TIPS = `${baseURL}/api/hotel/saveTips`;

//查询酒店提示集合
export const HOTEL_GET_TIPS = `${baseURL}/api/hotel/tips`;

//酒店已选标签信息
export const HOTEL_CHECK_LABEL = `${baseURL}/api/hotel/checkLabel`;

//酒店未选标签信息
export const HOTEL_UNCHECK_LABEL = `${baseURL}/api/hotel/unCheckLabel`;

//保存酒店标签
export const HOTEL_SAVE_LABEL = `${baseURL}/api/hotel/saveLabel`;

//房型&酒店设施更新
export const HOTEL_SAVE_FACILITY = `${baseURL}/api/facilityRelate/save`;

//取消规则分页查询
export const CANCELRULE_LIST = `${baseURL}/api/cancelrule/list`;

//取消规则新增
export const CANCELRULE_SAVE = `${baseURL}/api/cancelrule/save`;

//取消规则详情
export const CANCELRULE_DETAIL = `${baseURL}/api/cancelrule/detail`;

//取消规则编辑
export const CANCELRULE_UPDATE = `${baseURL}/api/cancelrule/update`;

//取消规则下拉
export const CANCELRULE_SELECT = `${baseURL}/api/cancelrule/selected`;

/**
 * 房价码API
 */
//根据id获取房价码相关详情
export const HOTEL_PRICE_DETAIL = `${baseURL}/api/room/rate/detail`;

//保存房价码
export const HOTEL_PRICE_SAVE = `${baseURL}/api/room/rate/save`;

/**
 * 设施管理API
 */
// 新增设施
export const FACILITY_ADD = `${baseURL}/api/facility/add`;
// 修改设施
export const FACILITY_UPDATE = `${baseURL}/api/facility/update`;

//根据设施id获取设施详情
export const FACILITY_DETAIL = `${baseURL}/api/facility/detail`;

//获取设施列表
export const HOTEL_FACILITY_LIST = `${baseURL}/api/facility/list`;

//获取酒店关联设施
export const HOTEL_FACILITY = `${baseURL}/api/facility/listForHotel`;

/**
 * 品牌管理API
 */
//分页查询列表信息
export const BRAND_LIST = `${baseURL}/api/brand/list`;

//根据id查询明细
export const BRAND_DETAIL = `${baseURL}/api/brand/detail`;

//查询明细品牌关联图片
export const BRAND_IMAGES = `${baseURL}/api/htlResource/brandImages`;

//查询代表酒店
export const BRAND_HOTEL = `${baseURL}/api/brand/hotel`;

//关联代表酒店道品牌
export const BRAND_RELATION = `${baseURL}/api/brand/relation`;

//更新品牌基本信息
export const BRAND_UPDATE = `${baseURL}/api/brand/update`;

//新增品牌基本信息
export const BRAND_SAVE = `${baseURL}/api/brand/save`;

/**
 * 房型API
 */
// 房型列表分页查询
export const ROOM_LIST = `${baseURL}/api/room/list`;

//查询全部房型列表下拉
export const ROOM_SELECT = `${baseURL}/api/room/queryRoomSelect`;

//查询酒店ID对应房型列表下拉
export const ROOM_SELECTBYID = `${baseURL}/api/room/queryNameAndId`;

//根据id获取房型明细
export const HOTEL_ROOM_DETAIL = `${baseURL}/api/room/detail`;

//房型保存修改
export const HOTEL_ROOM_SAVE = `${baseURL}/api/room/save`;

//房型保存修改
export const HOTEL_ROOM_EDIT = `${baseURL}/api/room/update`;

//获取房型图片
export const ROOM_GET_IMAGES = `${baseURL}/api/htlResource/roomImages`;

//房型设施获取
export const ROOM_GET_FACILITY = `${baseURL}/api/facility/listForRoom`;

// 房价码分页查询
export const ROOMPRICE_LIST = `${baseURL}/api/room/rate/list`;

//查询全部房价码列表下拉
export const ROOMPRICE_SELECT = `${baseURL}/api/room/rate/queryNameAndId`;
//查询酒店ID 房型ID 对应房价码列表下拉
export const ROOMPRICE_SELECTBYID = `${baseURL}/api/room/rate/queryRoomrateId`;

// 标签分页查询
export const LABEL_LIST = `${baseURL}/api/label/list`;

//标签新增
export const LABEL_ACTION = `${baseURL}/api/label/save`;

//标签编辑
export const LABEL_EDIT = `${baseURL}/api/label/edit`;

//标签详情
export const LABEL_DETAIL = `${baseURL}/api/label/detail`;

/**
 * 订单管理API
 */

// 订单分页查询
export const ORDER_LIST = `${baseURL}/api/order/list`;

//订单详情
export const ORDER_DETAIL = `${baseURL}/api/order/detail`;

//退款审核通过
export const ORDER_CHECK = `${baseURL}/api/order/check`;

//退款审核拒绝
export const ORDER_REJECT = `${baseURL}/api/order/reject`;

//退款取消
export const ORDER_CANCEL = `${baseURL}/api/order/cancel`;

/**
 * 渠道基本信息API
 */
//渠道基础信息分页查询
export const CHANNEL_LIST = `${baseURL}/api/channel/list`;

//获取渠道详情
export const CHANNEL_DETAIL = `${baseURL}/api/channel/detail`;

//渠道更新
export const CHANNEL_UPDATE = `${baseURL}/api/channel/update`;

//渠道新增
export const CHANNEL_SAVE = `${baseURL}/api/channel/save`;

//渠道下拉
export const CHANNEL_LISTSELECT = `${baseURL}/api/channel/pull`;

/**
 * 房价基价管理API
 */

// 房价基本价格分页查询
export const CRSBASEPRICE_LIST = `${baseURL}/api/basicroomprice/list`;

//根据roomID获取库存数量
export const CRSBASEPRICE_ROOMCOUNT = `${baseURL}/api/basicroomprice/roomcount`;

//房价基价新增
export const CRSBASEPRICE_ADD = `${baseURL}/api/basicroomprice/save`;

//房价基价修改
export const CRSBASEPRICE_EDIT = `${baseURL}/api/basicroomprice/update`;

//房价基价详情
export const CRSBASEPRICE_DETAIL = `${baseURL}/api/basicroomprice/detail`;

/**
 * 房价管理API
 */

//房价批量修改
export const CRSROOMPRICE_BATCHUPDATE = `${baseURL}/api/roomprice/batchupdate`;
//房价列表带分页查询
export const CRSROOMRATE_SELECTBYID = `${baseURL}/api/roomprice/list`;
//酒店关联房价树节点
export const CRSROOMRATE_TREENODE = `${baseURL}/api/roomprice/treenode`;

/**
 * 房态管理API
 */

//房态管理批量修改
export const ROOMSTATUS_BATCHUPDATE = `${baseURL}/api/roomstatus/batchupdate`;
//房价管理列表查询
export const ROOMSTATUS_LIST = `${baseURL}/api/roomstatus/list`;

/**
 * 预定规则管理API
 */
//预定规则设置分页查询
export const BOOKINGRULE_LIST = `${baseURL}/api/bookingrule/list`;

//预定规则添加
export const BOOKINGRULE_SAVE = `${baseURL}/api/bookingrule/save`;

//预定规则详情
export const BOOKINGRULE_DETAIL = `${baseURL}/api/bookingrule/detail`;

//预定规则编辑
export const BOOKINGRULE_EDIT = `${baseURL}/api/bookingrule/edit`;

//预定规则下拉
export const BOOKINGRULE_SELECT = `${baseURL}/api/bookingrule/selected`;

/**
 * 订单评论API
 */



//评论模板分页查询
export const COMMENT_TEMPLATE_LIST = `${baseURL}/api/commentemplate/list`;

//评论模板详情
export const COMMENT_TEMPLATE_DETAIL = `${baseURL}/api/commentemplate/detail`;

//评论模板编辑
export const COMMENT_TEMPLATE_UPDATE = `${baseURL}/api/commentemplate/update`;

//评论模板新增
export const COMMENT_TEMPLATE_ADD = `${baseURL}/api/commentemplate/save`;

//根据酒店ID查询对应回复模板
export const COMMENT_TEMPLATE_BYHOTEL = `${baseURL}/api/commentemplate/template`;

//评论列表
export const COMMENT_LIST = `${baseURL}/api/comment/list`;

//评论详情
export const COMMENT_DETAIL = `${baseURL}/api/comment/detail`;
//酒店回复
export const COMMENT_REPLY = `${baseURL}/api/comment/reply`;

//评论展示
export const COMMENT_SHOW = `${baseURL}/api/comment/show`;

//评论屏蔽
export const COMMENT_HIDE = `${baseURL}/api/comment/hide`;

//评论删除
export const COMMENT_DELETE = `${baseURL}/api/comment/delete`;

/**
 * 系统管理/权限管理/用户管理API
 */

//用户列表
export const USER_LIST = `${baseURL}/api/system/user/list`;
//用户新增
export const USER_ADD = `${baseURL}/api/system/user/add`;
//用户编辑
export const USER_EDIT = `${baseURL}/api/system/user/update`;
//用户详情
export const USER_DETAIL = `${baseURL}/api/system/user/info`;
//角色下拉
export const ROLE_LISTSELECT = `${baseURL}/api/system/role/selectlist`;

/**
 * 系统管理/权限管理/角色API
 */

//角色列表
export const ROLE_LIST = `${baseURL}/api/system/role/list`;
//角色新增
export const ROLE_ADD = `${baseURL}/api/system/role/add`;
//角色编辑
export const ROLE_EDIT = `${baseURL}/api/system/role/update`;
//角色详情
export const ROLE_DETAIL = `${baseURL}/api/system/role/info`;
//应用树列表
export const ROLE_TREE = `${baseURL}/api/system/role/apptree`;

/**
 * 菜单栏API
 */

// 用户model列表
export const MODEL_LIST = `${baseURL}/api/system/user/modellist`;

// 用户action列表
export const MODEL_ACTION = `${baseURL}/api/system/user/actionlist`;

/**
 * 登录API
 */
// 登录
export const API_LOGIN = `${baseURL}/api/system/user/login`;
// 退出
export const API_LOGINOUT = `${baseURL}/api/system/user/loginout`;
//用户修改密码
export const USER_EDITPASSWORD = `${baseURL}/api/system/user/modifyPwd`;

/**
 * 营销中心/营销工具/会员价API
 */

//会员价分页查询
export const ACTIVITYMEMBER_LIST = `${baseURL}/api/activitymember/list`;

//会员价明细
export const ACTIVITYMEMBER_DETAIL = `${baseURL}/api/activitymember/detail`;

//查询所有会员卡
export const CARD_ALL = `${baseURL}/api/card/pull`;

//保存会员价编辑
export const ACTIVITYMEMBER_UPDATE = `${baseURL}/api/activitymember/update`;

//保存会员价新增
export const ACTIVITYMEMBER_SAVE = `${baseURL}/api/activitymember/save`;

//删除会员价
export const ACTIVITYMEMBER_DELETE = `${baseURL}/api/activitymember/delete`;

/**
 * CRM/会员管理/会员查询
 */

// 会员管理分页查询
export const MEMBER_LIST = `${baseURL}/api/user/list`;
// 会员详情基本信息
export const MEMBER_BASICINFO = `${baseURL}/api/user/basicinfo`;
// 会员详情权益信息
export const MEMBER_BENEFITS = `${baseURL}/api/user/benefits`;
// 会员详情明细
export const MEMBER_DETAIL = `${baseURL}/api/user/detail`;
// 会员新增
export const MEMBER_SAVE = `${baseURL}/api/user/save`;
// 会员详情积分信息分页查询
export const MEMBER_SCORE = `${baseURL}/api/user/score`;
// 会员详情消费信息分页查询
export const MEMBER_CONSUME = `${baseURL}/api/user/consume`;
// 会员详情消费明细统计
export const MEMBER_AMOUNT = `${baseURL}/api/user/amount`;
// 会员详情积分细统计
export const MEMBER_USEDSCORE = `${baseURL}/api/user/usedscore`;
// 会员等级下拉
export const MEMBER_CARDLEVEL = `${baseURL}/api/card/pull`;

/**
 * CRM/会员管理/会员等级配置
 */

// 会员等级详情
export const MEMBERLEVEL_DETAIL = `${baseURL}/api/card/detail`;

// 会员等级配置分页查询
export const MEMBERLEVEL_LIST = `${baseURL}/api/card/list`;

// 会员等级配置添加
export const MEMBERLEVEL_SAVE = `${baseURL}/api/card/save`;

// 会员等级配置编辑
export const MEMBERLEVEL_EDIT = `${baseURL}/api/card/edit`;

// 会员卡配置下拉
export const MEMBER_CARDSELEVCT = `${baseURL}/api/style/pull`;

// 等级特权下拉
export const MEMBER_LEVELSETSELEVCT = `${baseURL}/api/card/stylebenfits`;

/**
 * CRM/会员管理/会员权益配置
 */

// 权益分页查询
export const BENFITS_LIST = `${baseURL}/api/benfits/list`;

// 权益新加
export const BENFITS_SAVE = `${baseURL}/api/benfits/save`;

// 权益修改
export const BENFITS_UPDATE = `${baseURL}/api/benfits/update`;

// 权益详情
export const BENFITS_DEATAIL = `${baseURL}/api/benfits/detail`;

/**
 * CRM/会员管理/积分兑换配置
 */

// 积分兑换配置列表
export const RULE_LIST = `${baseURL}/api/rule/list`;

// 积分兑换配置详细
export const RULE_DETAIL = `${baseURL}/api/rule/detail`;

// 积分兑换配置修改
export const RULE_UPDATE = `${baseURL}/api/rule/update`;

/**
 * CRM/会员管理/会员卡配置
 */

//会员卡配置分页查询
export const STYLE_LIST = `${baseURL}/api/style/list`;

// 会员卡配置新增
export const STYLE_SAVE = `${baseURL}/api/style/save`;

//会员卡配置编辑
export const STYLE_UPDATE = `${baseURL}/api/style/edit`;

//会员卡配置详情
export const STYLE_DETAIL = `${baseURL}/api/style/detail`;

/**
 * 营销中心/营销活动/房价立减活动
 */

//房价立减活动分页查询
export const AROOMPRICE_LIST = `${baseURL}/api/activitydecrease/list`;
//房价立减活动详情
export const AROOMPRICE_DEATIL = `${baseURL}/api/activitydecrease/detail`;
//房价立减活动新加
export const AROOMPRICE_SAVE = `${baseURL}/api/activitydecrease/save`;
//房价立减活动编辑
export const AROOMPRICE_EDIT = `${baseURL}/api/activitydecrease/edit`;

//活动房型管理新增
export const ACTIVITYDECREASE_RELATION = `${baseURL}/api/activitydecrease/relation`;
//活动房型管理移除
export const ACTIVITYDECREASE_REMOVE = `${baseURL}/api/activitydecrease/remove`;
//活动关联已选房价数据查询
export const ACTIVITYDECREASE_CHOSE = `${baseURL}/api/basicroomprice/chose`;
//活动关联未选房价数据查询
export const ACTIVITYDECREASE_UNCHOSE = `${baseURL}/api/basicroomprice/unchose`;

/**
 * 营销中心/营销活动/连住早定
 */

//连住早定活动分页查询
export const ACONTINOUS_LIST = `${baseURL}/api/activityearly/list`;
//连住早定活动详情
export const ACONTINOUS_DETAIL = `${baseURL}/api/activityearly/detail`;
//连住早定活动添加
export const ACONTINOUS_SAVE = `${baseURL}/api/activityearly/save`;
//连住早定活动编辑
export const ACONTINOUS_EDIT = `${baseURL}/api/activityearly/edit`;
//连住早定活动删除
export const ACONTINOUS_DELETE = `${baseURL}/api/activityearly/delete`;

/**
 * 营销中心/营销活动/新人特惠活动
 */

//新人特惠活动分页查询
export const ANEWPEOPLE_LIST = `${baseURL}/api/activitynewcomer/list`;
//新人特惠活动详情
export const ANEWPEOPLE_DETAIL = `${baseURL}/api/activitynewcomer/detail`;
//新人特惠活动添加
export const ANEWPEOPLE_SAVE = `${baseURL}/api/activitynewcomer/save`;
//新人特惠活动编辑
export const ANEWPEOPLE_EDIT = `${baseURL}/api/activitynewcomer/edit`;
//新人特惠活动删除
export const ANEWPEOPLE_DELETE = `${baseURL}/api/activitynewcomer/delete`;

//房价立减房型管理详情
export const AROOMPRICE_ROOMDETAIL = `${baseURL}/api/activitydecrease/edit`;
//房价立减房型管理修改
export const AROOMPRICE_ROOMSAVE = `${baseURL}/api/activitydecrease/edit`;

/**
 * 优惠券
 */
//优惠券来源分页查询
export const SOURCE_LIST = `${baseURL}/api/couponsource/list`;

//优惠券来源详情查询
export const SOURCE_DETAIL = `${baseURL}/api/couponsource/detail`;

//优惠券来源修改
export const SOURCE_UPDATE = `${baseURL}/api/couponsource/update`;

//优惠券来源新增
export const SOURCE_ADD = `${baseURL}/api/couponsource/add`;

//优惠券来源下拉
export const SOURCE_SELECT = `${baseURL}/api/couponsource/select`;

//优惠券来源编码验证
export const SOURCE_EXIST = `${baseURL}/api/couponsource/exist`;

//优惠券分页查询
export const COUPON_LIST = `${baseURL}/api/coupon/list`;

//优惠券详情
export const COUPON_DETAIL = `${baseURL}/api/coupon/detail`;

//新增优惠券
export const COUPON_ADD = `${baseURL}/api/coupon/add`;

//编辑优惠券
export const COUPON_UPDATE = `${baseURL}/api/coupon/update`;

//优惠券应用下拉框
export const COUPON_SELECT = `${baseURL}/api/coupon/select`;

//优惠券发放分页查询
export const COUPONSEND_LIST = `${baseURL}/api/couponsend/list`;

//优惠券发放详情查询
export const COUPONSEND_DETAIL = `${baseURL}/api/couponsend/detail`;

// 升级有礼分页查询
export const UPGRADE_LIST = `${baseURL}/api/activityupgrade/list`;

// 升级有礼删除
export const UPGRADE_DELETE = `${baseURL}/api/activityupgrade/delete`;

// 升级有礼详情
export const UPGRADE_DETAIL = `${baseURL}/api/activityupgrade/detail`;

// 升级有礼新增
export const UPGRADE_SAVE = `${baseURL}/api/activityupgrade/save`;

// 升级有礼编辑
export const UPGRADE_UPDATE = `${baseURL}/api/activityupgrade/update`;

//根据手机号查询用户ID 用户姓名（判断手机号是否注册）
export const COUPONSEND_VERIFY = `${baseURL}/api/couponsend/verify`;

//优惠券发放申请驳回
export const COUPONSEND_REJECT = `${baseURL}/api/couponsend/reject`;

//优惠券发放申请
export const COUPONSEND_APPLY = `${baseURL}/api/couponsend/apply`;

//优惠券发放申请
export const COUPONSEND_AGREE = `${baseURL}/api/couponsend/agree`;

//优惠券核销分页查询
export const USRCOUPON_LIST = `${baseURL}/api/usrcoupon/list`;

//优惠券核销详情查询
export const USRCOUPON_DETAIL = `${baseURL}/api/usrcoupon/detail`;

//核销
export const USRCOUPON_CHECK = `${baseURL}/api/usrcoupon/check`;

//作废
export const USRCOUPON_CANCEL = `${baseURL}/api/usrcoupon/cancel`;

/**
 * 分销管理 - 分销报表
 */

// 关注公众号统计详细数据列表
export const GZHDATALIST = `${baseURL}/api/salerecord/publicdetail`;

// 关注公众号统计汇总数据列表
export const GZHDATATOTAL = `${baseURL}/api/salerecord/publictotal`;

// 新会员统计详细数据列表查询
export const MEMBERDATALIST = `${baseURL}/api/salerecord/memberdetail`;

// 新会员统计汇总数据列表查询
export const  MEMBERDATATOTAL = `${baseURL}/api/salerecord/membertotal`;

// 金卡会籍分销统计详细数据
export const GOLDDATALIST = `${baseURL}/api/salerecord/carddetail`;

// 金卡会籍分销统计汇总数据列表
export const GOLDDATATOTAL = `${baseURL}/api/salerecord/cardtotal`;

/**
 * 分销管理 - 推广人员设置
 */

// 人员审核分页查询
export const SALECHECKLIST = `${baseURL}/api/saleActivityUser/list`;

// 人员审核批量通过
export const SALECHECKPASS = `${baseURL}/api/saleActivityUser/pass`;

// 人员审核批量拒绝
export const SALECHECKREJECT = `${baseURL}/api/saleActivityUser/reject`;

// 人员列表分页查询
export const SALEPEOPLELIST = `${baseURL}/api/saleActivityUser/sales`;

// 人员列表批量冻结
export const SALEPEOPLEFROZEN = `${baseURL}/api/saleActivityUser/frozen`;

// 推广结算设置详情修改
export const SALEDETAILEDIT = `${baseURL}/api/saleActivity/memberupdate`;

// 推广结算设置详情显示
export const SALEDETAILDATA = `${baseURL}/api/saleActivity/detail`;

/**
 * 分销管理-公众号推广
 */

// 公众号推广是否考核显示
export const GZHCHECK = `${baseURL}/api/saleActivity/check`;

// 公众号考核审核修改
export const GZHCHECKEDIT = `${baseURL}/api/saleActivity/ischeck`;

/**
 * 分销管理-小程序会员拉新
 */

// 小程序会员拉新是否考核显示
export const MEMBERCHECK = `${baseURL}/api/saleActivity/xcxcheck`;

// 小程序会员拉新考核审核修改
export const MEMBERCHECKEDIT = `${baseURL}/api/saleActivity/xcxischeck`;


/**
 * 分销管理-金卡会籍销售
 */

// 金卡会籍是否考核显示
export const GOLDCHECK = `${baseURL}/api/saleActivity/cardcheck`;

// 金卡会籍考核审核修改
export const GOLDCHECKEDIT = `${baseURL}/api/saleActivity/cardischeck`;




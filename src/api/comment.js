/*
 * @Author: your name
 * @Date: 2019-12-19 11:12:46
 * @LastEditTime: 2020-05-11 12:39:58
 * @LastEditors: Please set LastEditors
 * @Description: 订单评论API
 * @FilePath: \mini-admin\src\api\hotel.js
 */

import request from "@/utils/request";
import { baseURL } from "./index.js";

// 评论分页查询
export function commentList(data) {
  return request.post(`${baseURL}/api/comment/list`, data);
}
// 评论详情查询
export function commentDetail(data) {
  return request.post(`${baseURL}/api/comment/detail`, data);
}
//评论模板分页查询
export function commentTemplateList(data) {
  return request.post(`${baseURL}/api/commentemplate/list`, data);
}
//评论模板详情
export function commentTemplateDetail(data) {
  return request.post(`${baseURL}/api/commentemplate/detail`, data);
}
//评论模板新增
export function commentTemplateAdd(data) {
  return request.post(`${baseURL}/api/commentemplate/save`, data);
}
//评论模板编辑
export function commentTemplateUpdate(data) {
  return request.post(`${baseURL}/api/commentemplate/update`, data);
}
//根据酒店ID查询对应回复模板
export function commentTemplateByHotel(data) {
  return request.post(`${baseURL}/api/commentemplate/template`, data);
}
//酒店回复
export function commentReply(data) {
  return request.post(`${baseURL}/api/comment/reply`, data);
}
//评论展示
export function commentShow(data) {
  return request.post(`${baseURL}/api/comment/show`, data);
}
//评论屏蔽
export function commentHide(data) {
  return request.post(`${baseURL}/api/comment/hide`, data);
}
//评论删除
export function commentDelete(data) {
  return request.post(`${baseURL}/api/comment/delete`, data);
}







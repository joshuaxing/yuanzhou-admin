/*
 * @Author: your name
 * @Date: 2020-01-03 14:00:24
 * @LastEditTime: 2020-06-03 14:18:44
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-admin\src\api\member.js
 */
import request from "@/utils/request.js";
import { baseURL } from "./index.js";

// 会员分页查询
export function memberList(data) {
  return request.post(`${baseURL}/api/user/list`, data);
}

// 会员详情基本信息
export function memberBasicinfo(data) {
  return request.post(`${baseURL}/api/user/basicinfo`, data);
}

// 会员详情权益信息
export function memberBenefits(data) {
  return request.post(`${baseURL}/api/user/benefits`, data);
}

// 会员详情明细
export function memberDetail(data) {
  return request.post(`${baseURL}/api/user/detail`, data);
}

// 会员新增
export function memberSave(data) {
  return request.post(`${baseURL}/api/user/save`, data);
}

// 会员详情积分信息分页查询
export function memberScore(data) {
  return request.post(`${baseURL}/api/user/score`, data);
}

// 会员详情消费信息分页查询
export function memberConsume(data) {
  return request.post(`${baseURL}/api/user/consume`, data);
}

// 会员详情消费明细统计
export function memberAmount(data) {
  return request.post(`${baseURL}/api/user/amount`, data);
}

// 会员详情积分细统计
export function memberUsedscore(data) {
  return request.post(`${baseURL}/api/user/usedscore`, data);
}

// 会员等级下拉
export function memberCardLevel(data) {
  return request.post( `${baseURL}/api/card/pull`, data);
}

/**
 * 会员等级配置
 */

 // 会员等级详情
export function memberLevelDetail(data) {
  return request.post(`${baseURL}/api/card/detail`, data);
}

// 会员等级配置分页查询
export function memberLevelList(data) {
  return request.post(`${baseURL}/api/card/list`, data);
}

// 会员等级配置添加
export function memberLevelSave(data) {
  return request.post(`${baseURL}/api/card/save`, data);
}

 // 会员等级配置编辑
 export function memberLevelEdit(data) {
  return request.post(`${baseURL}/api/card/edit`, data);
}

// 会员卡配置下拉
export function memberCardSelect(data) {
  return request.post(`${baseURL}/api/style/pull`, data);
}

// 等级特权下拉
export function memberLevelSetSelect(data) {
  return request.post(`${baseURL}/api/card/stylebenfits`, data);
}

/**
 * 会员权益配置
 */

 // 权益分页查询
 export function benfitsList(data) {
  return request.post(`${baseURL}/api/benfits/list`, data);
}

 // 权益新加
 export function benfitsSave(data) {
  return request.post(`${baseURL}/api/benfits/save`, data);
}

 // 权益修改
 export function benfitsUpdate(data) {
  return request.post(`${baseURL}/api/benfits/update`, data);
}

 // 权益详情
export function benfitsDetail(data) {
  return request.post(`${baseURL}/api/benfits/detail`, data);
}


/**
 * 积分兑换配置
 */

// 积分兑换配置列表
export function ruleList(data) {
  return request.post(`${baseURL}/api/rule/list`, data);
}

 // 积分兑换配置详细
export function ruleDetail(data) {
  return request.post(`${baseURL}/api/bookingrule/detail`, data);
}

// 积分兑换配置修改
export function ruleUpdate(data) {
  return request.post(`${baseURL}/api/cancelrule/update`, data);
}

/**
 * 会员卡配置
 */

// 会员卡配置分页查询
export function styleList(data) {
  return request.post(`${baseURL}/api/style/list`, data);
}

// 会员卡配置新增
export function styleSave(data) {
  return request.post(`${baseURL}/api/style/save`, data);
}

// 会员卡配置编辑
export function styleUpdate(data) {
  return request.post(`${baseURL}/api/style/edit`, data);
}

// 会员卡配置详情
export function styleDetail(data) {
  return request.post(`${baseURL}/api/style/detail`, data);
}

// 会员冻结/解冻
export function memberFrozen(data) {
  return request.post(`${baseURL}/api/user/frozen`, data);
}


// 金卡充值记录分页查询
export function goldcardList(data) {
  return request.post(`${baseURL}/api/goldcard/list`, data);
}

// 金卡充值记录导出
export function goldcardListExcel(data) {
  return request.downlaodfile(`${baseURL}/api/goldcard/goldcardexcel`, data);
}

// 储值卡绑定记录分页查询
export function cardbindingList(data) {
  return request.post(`${baseURL}/api/cardbinding/list`, data);
}

// 储值卡解绑
export function cardbindingCancel(data) {
  return request.post(`${baseURL}/api/cardbinding/cancelBinding`, data);
}

// 绑定储值卡
export function cardbindingBind(data) {
  return request.post(`${baseURL}/api/cardbinding/binding`, data);
}

// 绑卡短信验证码验证
export function cardbindingCheckCode(data) {
  return request.post(`${baseURL}/api/cardbinding/check`, data);
}

// 绑卡发送短信验证码
export function cardbindingSendCode(data) {
  return request.post(`${baseURL}/api/cardbinding/verify`, data);
}

// 会员储值记录分页查询
export function depositRecordList(data) {
  return request.post(`${baseURL}/api/depositRecord/list`, data);
}

// 冲销
export function depositRecordReverse(data) {
  return request.post(`${baseURL}/api/depositRecord/reverse`, data);
}

// 会员储值调整
export function depositRecordAdd(data) {
  return request.post(`${baseURL}/api/depositRecord/add`, data);
}

// 根据手机号查询客户姓名，余额信息
export function depositRecordInfo(data) {
  return request.post(`${baseURL}/api/depositRecord/info`, data);
}

// 根据用户主键ID查询手机号
export function userPhone(data) {
  return request.post(`${baseURL}/api/user/phone`, data);
}

// 用户手机号重新绑定
export function updatePhone(data) {
  return request.post(`${baseURL}/api/user/updatephone`, data);
}
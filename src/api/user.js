/*
 * @Author: your name
 * @Date: 2019-12-02 11:12:46
 * @LastEditTime: 2020-11-03 15:25:51
 * @LastEditors: Please set LastEditors
 * @Description: 用户API
 * @FilePath: \mini-admin\src\api\user.js
 */

import request from "@/utils/request";
import { baseURL } from "./index.js";

// 登录
export function login(data) {
  return request.post(`${baseURL}/api/system/user/login`, data);
}

// 自动登录
export function login2(data) {
  return request.post(`${baseURL}/api/system/user/nopwdlogin`, data);
}

// sessionid校验
export function logincheck(data) {
  return request.post(`${baseURL}/api/system/user/checksession`, data);
}

// 退出
export function loginout(data) {
  return request.post(`${baseURL}/api/system/user/loginout`, data);
}

// 用户列表
export function userList(data) {
  return request.post(`${baseURL}/api/system/user/list`, data);
}

// 用户新增
export function userAdd(data) {
  return request.post(`${baseURL}/api/system/user/add`, data);
}

// 用户编辑
export function userEdit(data) {
  return request.post(`${baseURL}/api/system/user/update`, data);
}

// 用户详情
export function userDetail(data) {
  return request.post(`${baseURL}/api/system/user/info`, data);
}

// 角色列表
export function roleList(data) {
  return request.post(`${baseURL}/api/system/role/list`, data);
}

// 角色新增
export function roleAdd(data) {
  return request.post(`${baseURL}/api/system/role/add`, data);
}

// 角色编辑
export function roleEdit(data) {
  return request.post(`${baseURL}/api/system/role/update`, data);
}
// 角色详情
export function roleDetail(data) {
  return request.post(`${baseURL}/api/system/role/info`, data);
}
// 应用树列表
export function roleTree(data) {
  return request.post(`${baseURL}/api/system/role/apptree`, data);
}

// 模块列表
export function modelList(data) {
  return request.post(`${baseURL}/api/system/user/modellist`, data);
}

// 用户模块列表
export function modelAction(data) {
  return request.post(`${baseURL}/api/system/user/actionlist`, data);
}

// 角色下拉
export function roleListSelect(data) {
  return request.post(`${baseURL}/api/system/role/selectlist`, data);
}

// 用户修改密码
export function userEditPassword(data) {
  return request.post(`${baseURL}/api/system/user/modifyPwd`, data);
}

// 用户重置密码
export function userResetPassword(data) {
  return request.post(`${baseURL}/api/system/user/reset`, data);
}
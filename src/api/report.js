/*
 * @Author: your name
 * @Date: 2020-07-07 18:20:00
 * @LastEditTime: 2020-11-05 17:13:47
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-admin\src\api\report.js
 */

import request from "@/utils/request";
import { baseURL } from "./index";

// 订单报表
export function order(data) {
  return request.post(`${baseURL}/api/report/order`, data);
}

// 报表统计
export function ordercount(data) {
  return request.post(`${baseURL}/api/report/ordercount`, data);
}


// 订单复购率报表
export function orderreport(data) {
  return request.post(`${baseURL}/api/order/report`, data);
}

// 订单未收账列表
export function orderunreceive(data) {
  return request.post(`${baseURL}/api/order/unreceive`, data);
}

// 订单未收账报表
export function orderunreceiveexal(data) {
  return request.downlaodfile(`${baseURL}/api/salerecord/unreceiveexcel`, data);
}


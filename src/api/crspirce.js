/*
 * @Author: your name
 * @Date: 2019-12-02 11:12:46
 * @LastEditTime: 2020-05-11 13:41:19
 * @LastEditors: Please set LastEditors
 * @Description: 渠道管理API
 * @FilePath: \mini-admin\src\api\crspirce.js
 */

import request from "@/utils/request";
import { baseURL } from "./index.js";

//渠道基础信息分页查询
export function channelList(data) {
  return request.post(`${baseURL}/api/channel/list`, data);
}

//获取渠道详情
export function channelDetail(data) {
  return request.post(`${baseURL}/api/channel/detail`, data);
}

//渠道更新
export function channelUpdate(data) {
  return request.post(`${baseURL}/api/channel/update`, data);
}

//渠道新增
export function channelSave(data) {
  return request.post(`${baseURL}/api/channel/save`, data);
}

// 房价基价列表
export function crsbasepirceList(data) {
  return request.post(`${baseURL}/api/basicroomprice/list`, data);
}
// 房价基价新增
export function crsbasepirceAdd(data) {
  return request.post(`${baseURL}/api/basicroomprice/save`, data);
}
// 房价基价修改
export function crsbasepirceEdit(data) {
  return request.post(`${baseURL}/api/basicroomprice/update`, data);
}
// 房价基价详情
export function crsbasepirceDetail(data) {
  return request.post(`${baseURL}/api/basicroomprice/detail`, data);
}
// 根据房型获取库存
export function crsbasepirceByroomId(data) {
  return request.post(`${baseURL}/api/basicroomprice/roomcount`, data);
}


// 房价批量修改
export function crsroompriceBatchupdate(data) {
  return request.post(`${baseURL}/api/roomprice/batchupdate`, data);
}
// 房价列表带分页查询
export function roompriceList(data) {
  return request.post(`${baseURL}/api/roomprice/list`, data);
}
// 酒店关联房价树节点
export function roompriceTreenode(data) {
  return request.post(`${baseURL}/api/roomprice/treenode`, data);
}

//房态管理批量修改
export function roomstatusBatchupdate(data) {
  return request.post(`${baseURL}/api/roomstatus/batchupdate`, data);
}
//房价管理列表查询
export function roomstatusList(data) {
  return request.post(`${baseURL}/api/roomstatus/list`, data);
}

//渠道下拉
export function channelListSelect(data) {
  return request.post(`${baseURL}/api/channel/pull`, data);
}

//预定规则设置分页查询
export function bookingruleList(data) {
  return request.post(`${baseURL}/api/bookingrule/list`, data);
}
//预定规则添加
export function bookingruleSave(data) {
  return request.post(`${baseURL}/api/bookingrule/save`, data);
}
//预定规则详情
export function bookingruleDetail(data) {
  return request.post(`${baseURL}/api/bookingrule/detail`, data);
}
//预定规则编辑
export function bookingruleEdit(data) {
  return request.post(`${baseURL}/api/bookingrule/edit`, data);
}
//预定规则下拉
export function bookingruleSelect(data) {
  return request.post(`${baseURL}/api/bookingrule/selected`, data);
}
//取消规则下拉
export function cancelruleSelect(data) {
  return request.post(`${baseURL}/api/cancelrule/selected`, data);
}




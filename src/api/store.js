/*
 * @Author: your name
 * @Date: 2020-06-24 09:35:32
 * @LastEditTime: 2020-08-13 15:10:39
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-admin\src\api\store.js
 */

import request from "@/utils/request";
import { baseURL } from "./index.js";

/**
 * 类目管理-自定义类目
 */

// 自定义类目分页查询
export function categoryList(data) {
  return request.post(`${baseURL}/api/category/list`, data);
}

// 自定义类目保存
export function categorySave(data) {
  return request.post(`${baseURL}/api/category/save`, data);
}

// 自定义类目编辑
export function categoryUpdate(data) {
  return request.post(`${baseURL}/api/category/update`, data);
}

// 上级分类下拉框
export function categoryPull(data) {
  return request.post(`${baseURL}/api/category/pull`, data);
}

// 类目删除
export function categoryDelete(data) {
  return request.post(`${baseURL}/api/category/delete`, data);
}

// 根据父类目ID查询子类目
export function categoryChild(data) {
  return request.post(`${baseURL}/api/category/child`, data);
}

/**
 * 商城基础设置-通用模块
 */

//首页轮播图显示
export function bannerConfigList(data) {
  return request.post(`${baseURL}/api/bannerConfig/list`, data);
}

//首页轮播图添加
export function bannerConfigSave(data) {
  return request.post(`${baseURL}/api/bannerConfig/save`, data);
}

//首页轮播图删除
export function bannerConfigDelete(data) {
  return request.post(`${baseURL}/api/bannerConfig/delete`, data);
}

//导航列表显示
export function navigationConfigList(data) {
  return request.post(`${baseURL}/api/navigationconfig/list`, data);
}

//导航列表添加
export function navigationConfigSave(data) {
  return request.post(`${baseURL}/api/navigationconfig/save`, data);
}

//导航列表修改
export function navigationConfigUpdate(data) {
  return request.post(`${baseURL}/api/navigationconfig/update`, data);
}

//导航列表详情查询
export function navigationConfigDetail(data) {
  return request.post(`${baseURL}/api/navigationconfig/detail`, data);
}

//导航列表删除
export function navigationConfigDelete(data) {
  return request.post(`${baseURL}/api/navigationconfig/delete`, data);
}

//物流模板分页查询
export function logisticsList(data) {
  return request.post(`${baseURL}/api/logistics/list`, data);
}

//物流模板发布
export function logisticsSave(data) {
  return request.post(`${baseURL}/api/logistics/save`, data);
}

//物流模板编辑
export function logisticsUpdate(data) {
  return request.post(`${baseURL}/api/logistics/update`, data);
}

//物流模板删除
export function logisticsDelete(data) {
  return request.post(`${baseURL}/api/logistics/delete`, data);
}

//物流模板详情
export function logisticsDetail(data) {
  return request.post(`${baseURL}/api/logistics/detail`, data);
}
//物流模板下拉
export function logisticsPull(data) {
  return request.post(`${baseURL}/api/logistics/pull`, data);
}

//预订须知保存
export function shoppingconfigBook(data) {
  return request.post(`${baseURL}/api/shoppingconfig/book`, data);
}

//通知列表
export function shoppingconfigNotice(data) {
  return request.post(`${baseURL}/api/shoppingconfig/notice`, data);
}

//通知栏保存
export function shoppingconfigSave(data) {
  return request.post(`${baseURL}/api/shoppingconfig/save`, data);
}

//显示通知栏
export function shoppingconfigShow(data) {
  return request.post(`${baseURL}/api/shoppingconfig/show`, data);
}

//隐藏通知栏
export function shoppingconfigUpdate(data) {
  return request.post(`${baseURL}/api/shoppingconfig/update`, data);
}

/**
 * 分组管理-分组基础信息
 */

// 分组分页查询
export function groupsList(data) {
  return request.post(`${baseURL}/api/groups/list`, data);
}
// 分组保存
export function groupsSave(data) {
  return request.post(`${baseURL}/api/groups/save`, data);
}
// 分组修改
export function groupsUpdate(data) {
  return request.post(`${baseURL}/api/groups/update`, data);
}
// 分组删除
export function groupsDelete(data) {
  return request.post(`${baseURL}/api/groups/delete`, data);
}
// 分组详情
export function groupsDetail(data) {
  return request.post(`${baseURL}/api/groups/detail`, data);
}

// 分组下拉
export function groupsPull(data) {
  return request.post(`${baseURL}/api/groups/pull`, data);
}

/**
 * 商品管理-发布商品
 */

// 真实商品发布
export function goodsSave(data) {
  return request.post(`${baseURL}/api/goods/save`, data);
}
// 真实商品发布上架
export function goodsSave2(data) {
  return request.post(`${baseURL}/api/goods/saveup`, data);
}
// 发布票券保存
export function goodsTicketsSave(data) {
  return request.post(`${baseURL}/api/goodsTickets/save`, data);
}
// 发布票券保存上架
export function goodsTicketsSave2(data) {
  return request.post(`${baseURL}/api/goodsTickets/saveup`, data);
}

// 商品详情查询
export function goodsDetail(data) {
  return request.post(`${baseURL}/api/goods/detail`, data);
}

// 票券详情查询
export function goodsTicketsDetail(data) {
  return request.post(`${baseURL}/api/goodsTickets/detail`, data);
}

// 商品编辑
export function goodsUpdate(data) {
  return request.post(`${baseURL}/api/goods/update`, data);
}

// 商品编辑上架
export function goodsUpdate2(data) {
  return request.post(`${baseURL}/api/goods/updateI`, data);
}

/**
 * 商品管理-商品商品
 */

// 商品列表
export function goodsList(data) {
  return request.post(`${baseURL}/api/goods/list`, data);
}

// 商品列表导出
export function goodsListExcel(data) {
  return request.downlaodfile(`${baseURL}/api/goods/goodsexcel`, data);
}


// 商品下架
export function editgoodsDown(data) {
  return request.post(`${baseURL}/api/goods/down`, data);
}


// 商品类别修改
export function editgoodsCategory(data) {
  return request.post(`${baseURL}/api/goods/category`, data);
}

// 商品删除
export function editgoodsDelete(data) {
  return request.post(`${baseURL}/api/goods/delete`, data);
}


// 商品分组
export function editgoodsGroup(data) {
  return request.post(`${baseURL}/api/goods/group`, data);
}

// 商品运费修改
export function editgoodsLogistics(data) {
  return request.post(`${baseURL}/api/goods/logistics`, data);
}


// 商品修改价格
export function editgoodsPrice(data) {
  return request.post(`${baseURL}/api/goods/price`, data);
}

// 商品修改库存
export function editgoodsStock(data) {
  return request.post(`${baseURL}/api/goods/stock`, data);
}

// 商品置顶
export function editgoodsTop(data) {
  return request.post(`${baseURL}/api/goods/top`, data);
}

// 商品上架
export function editgoodsUp(data) {
  return request.post(`${baseURL}/api/goods/up`, data);
}

// 添加商品图片获取主键ID
export function goodsID(data) {
  return request.post(`${baseURL}/api/goods/id`, data);
}

// 商品列表label数量
export function goodsCount(data) {
  return request.post(`${baseURL}/api/goods/count`, data);
}


/**
 * 订单管理-商城订单
 */

// 商城订单列表分页查询
export function shopOrderList(data) {
  return request.post(`${baseURL}/api/shoporder/list`, data);
}

// 商城订单详情查询
export function shopOrderDetail(data) {
  return request.post(`${baseURL}/api/shoporder/detail`, data);
}

// 商城订单发货接口
export function shopOrderSend(data) {
  return request.post(`${baseURL}/api/shoporder/send`, data);
}

// 商城订单退款接口
export function shopOrderBack(data) {
  return request.post(`${baseURL}/api/shoporder/back`, data);
}

// 商城订单添加备注接口
export function shopOrderRemark(data) {
  return request.post(`${baseURL}/api/shoporder/remark`, data);
}

// 商城订单操作日志
export function shopOrderLog(data) {
  return request.post(`${baseURL}/api/shoporder/log`, data);
}

// 商城订单修改
export function shopOrderUpdate(data) {
  return request.post(`${baseURL}/api/shoporder/update`, data);
}

// 商城订单取消
export function shopOrderCancle(data) {
  return request.post(`${baseURL}/api/shoporder/cancle`, data);
}

// 订单列表label数量
export function shopOrderCount(data) {
  return request.post(`${baseURL}/api/shoporder/count`, data);
}

// 订单导出
export function shopOrderExcel(data) {
  return request.downlaodfile(`${baseURL}/api/shoporder/excel`, data);
}

/**
 * 订单管理-券码核销
 */

// 券码核销分页查询
export function shopOrderTicket(data) {
  return request.post(`${baseURL}/api/orderticket/list`, data);
}

// 商城票券消费核销接口
export function shopOrderTicketAgree(data) {
  return request.post(`${baseURL}/api/orderticket/agree`, data);
}

// 商城票券退款核销接口
export function shopOrderTicketAgree2(data) {
  return request.post(`${baseURL}/api/orderticket/refund`, data);
}

// 商城票券过期票券核销接口
export function shopOrderTicketAgree3(data) {
  return request.post(`${baseURL}/api/orderticket/overdue`, data);
}

// 商城票券券核列表导出
export function shopOrderTicketExcel(data) {
  return request.downlaodfile(`${baseURL}/api/orderticket/ticketsexcel`, data);
}


// 物流省市区

//获取省级行政单位
export function shopProvince(data) {
  return request.post(`${baseURL}/api/province/all`, data);
}

//获取城市
export function shopCity(data) {
  return request.post(`${baseURL}/api/province/city`, data);
}

//获取区
export function shopDistrict(data) {
  return request.post(`${baseURL}/api/province/district`, data);
}

// 支持商户
export function goodsMerchant(data) {
  return request.post(`${baseURL}/api/goods/merchant`, data);
}

// 票券拆分
export function ticketSplit(data) {
  return request.post(`${baseURL}/api/shoporder/sendTicket`, data);
}

/*
 * @Author: your name
 * @Date: 2019-12-02 11:12:46
 * @LastEditTime: 2020-05-07 16:52:40
 * @LastEditors: Please set LastEditors
 * @Description: 订单管理API
 * @FilePath: \mini-admin\src\api\order.js
 */

import request from "@/utils/request";
import { baseURL } from "./index.js";

// 订单列表
export function list(data) {
  return request.post(`${baseURL}/api/order/list`, data);
}

// 订单详情
export function detail(data) {
  return request.post(`${baseURL}/api/order/detail`, data);
}

// 退款审核
export function check(data) {
  return request.post(`${baseURL}/api/order/check`, data);
}

// 退款审核拒绝
export function reject(data) {
  return request.post(`${baseURL}/api/order/reject`, data);
}

// 订单取消
export function cancle(data) {
  return request.post(`${baseURL}/api/order/cancel`, data);
}

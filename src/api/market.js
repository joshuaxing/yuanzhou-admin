/*
 * @Author: your name
 * @Date: 2019-12-02 11:12:46
 * @LastEditTime: 2020-07-02 10:44:43
 * @LastEditors: Please set LastEditors
 * @Description: 会员API
 * @FilePath: \mini-admin\src\api\market.js
 */

import request from "@/utils/request";
import { baseURL } from "./index.js";

//会员价分页查询
export function activityMemberList (data) {
  return request.post(`${baseURL}/api/activitymember/list`, data)
}

//会员价明细
export function activityMemberDetail (data) {
  return request.post(`${baseURL}/api/activitymember/detail`, data)
}

//所有会员卡
export function cardAll (data) {
  return request.post(`${baseURL}/api/card/pull`, data)
}

//保存会员价编辑
export function activityMemberUpdate (data) {
  return request.post(`${baseURL}/api/activitymember/update`, data)
}

//保存会员价新增
export function activityMemberSave (data) {
  return request.post(`${baseURL}/api/activitymember/save`, data)
}

//删除会员价
export function activityMemberDelete (data) {
  return request.post(`${baseURL}/api/activitymember/delete`, data)
}

/**
 * 营销中心/房价立减活动
 */

//房价立减活动分页查询
export function aRoompriceList (data) {
  return request.post(`${baseURL}/api/activitydecrease/list`, data)
}
//房价立减活动详情
export function aRoompriceDetail (data) {
  return request.post(`${baseURL}/api/activitydecrease/detail`, data)
}
//房价立减活动新加
export function aRoompriceSave (data) {
  return request.post(`${baseURL}/api/activitydecrease/save`, data)
}
//房价立减活动编辑
export function aRoompriceEdit (data) {
  return request.post(`${baseURL}/api/activitydecrease/edit`, data)
}
//房价立减活动删除
export function aRoompriceDelete (data) {
  return request.post(`${baseURL}/api/activitydecrease/delete`, data)
}

//活动房型管理新增
export function activitydecreaseRelation (data) {
  return request.post(`${baseURL}/api/activitydecrease/relation`, data)
}
//活动房型管理移除
export function activitydecreaseRemove (data) {
  return request.post(`${baseURL}/api/activitydecrease/remove`, data)
}

/**
 * 营销中心/连住早定
 */

//连住早定活动分页查询
export function aContinousList (data) {
  return request.post(`${baseURL}/api/activityearly/list`, data)
}
//连住早定活动详情
export function aContinousDetail (data) {
  return request.post(`${baseURL}/api/activityearly/detail`, data)
}
//连住早定活动新加
export function aContinousSave (data) {
  return request.post(`${baseURL}/api/activityearly/save`, data)
}
//连住早定活动编辑
export function aContinousEdit (data) {
  return request.post(`${baseURL}/api/activityearly/edit`, data)
}
//连住早定活动删除
export function aContinousDelete (data) {
  return request.post(`${baseURL}/api/activityearly/delete`, data)
}

/**
 * 营销中心/新人特惠活动
 */

//新人特惠活动分页查询
export function aNewPeopleList (data) {
  return request.post(`${baseURL}/api/activitynewcomer/list`, data)
}
//新人特惠活动详情
export function aNewPeopleDetail (data) {
  return request.post(`${baseURL}/api/activitynewcomer/detail`, data)
}
//新人特惠活动新加
export function aNewPeopleSave (data) {
  return request.post(`${baseURL}/api/activitynewcomer/save`, data)
}
//新人特惠活动编辑
export function aNewPeopleEdit (data) {
  return request.post(`${baseURL}/api/activitynewcomer/edit`, data)
}
//新人特惠活动删除
export function aNewPeopleDelete (data) {
  return request.post(`${baseURL}/api/activitynewcomer/delete`, data)
}

//活动关联已选房价数据查询
export function activitydecreaseChose(data) {
  return request.post(`${baseURL}/api/basicroomprice/chose`, data)
}
//活动关联未选房价数据查询
export function activitydecreaseUnChose(data) {
  return request.post(`${baseURL}/api/basicroomprice/unchose`, data)
}

/**
 * 营销中心/会员日活动
 */

// 会员日活动详情
export function activitymemberdayDetail(data) {
  return request.post(`${baseURL}/api/activitymemberday/detail`, data)
}

// 会员日活动分页查询
export function activitymemberdayList(data) {
  return request.post(`${baseURL}/api/activitymemberday/list`, data)
}

// 会员日活动添加
export function activitymemberdaySave(data) {
  return request.post(`${baseURL}/api/activitymemberday/save`, data)
}

// 会员日活动编辑
export function activitymemberdayUpdate(data) {
  return request.post(`${baseURL}/api/activitymemberday/update`, data)
}

// 会员日活动删除
export function activitymemberdayDelete(data) {
  return request.post(`${baseURL}/api/activitymemberday/delete`, data)
}
/*
 * @Author: your name
 * @Date: 2019-12-02 11:12:46
 * @LastEditTime: 2020-05-19 17:10:35
 * @LastEditors: Please set LastEditors
 * @Description: 渠道管理API
 * @FilePath: \mini-admin\src\api\crspirce.js
 */

import request from "@/utils/request";
import { baseURL } from "./index.js";

//优惠券来源分页查询
export function sourceList(data) {
  return request.post(`${baseURL}/api/couponsource/list`, data);
}
//优惠券来源详情查询
export function sourceDetail(data) {
  return request.post(`${baseURL}/api/couponsource/detail`, data);
}
//优惠券来源修改
export function sourceUpdate(data) {
  return request.post(`${baseURL}/api/couponsource/update`, data);
}
//优惠券来源新增
export function sourceAdd(data) {
  return request.post(`${baseURL}/api/couponsource/add`, data);
}
//优惠券来源下拉
export function sourceSelect(data) {
  return request.post(`${baseURL}/api/couponsource/select`, data);
}
//优惠券来源编码验证
export function sourceExist(data) {
  return request.post(`${baseURL}/api/couponsource/exist`, data);
}
//优惠券分页查询
export function couponList(data) {
  return request.post(`${baseURL}/api/coupon/list`, data);
}
//优惠券详情
export function couponDetail(data) {
  return request.post(`${baseURL}/api/coupon/detail`, data);
}
//新增优惠券
export function couponAdd(data) {
  return request.post(`${baseURL}/api/coupon/add`, data);
}
//编辑优惠券
export function couponUpdate(data) {
  return request.post(`${baseURL}/api/coupon/update`, data);
}
//优惠券应用下拉框
export function couponSelect(data) {
  return request.post(`${baseURL}/api/coupon/select`, data);
}
//优惠券发放分页查询
export function couponSendList(data) {
  return request.post(`${baseURL}/api/couponsend/list`, data);
}
//优惠券发放详情查询
export function couponSendDetail(data) {
  return request.post(`${baseURL}/api/couponsend/detail`, data);
}
//根据手机号查询用户ID 用户姓名（判断手机号是否注册）
export function couponSendVerify(data) {
  return request.post(`${baseURL}/api/couponsend/verify`, data);
}
//优惠券发放申请驳回
export function couponReject(data) {
  return request.post(`${baseURL}/api/couponsend/reject`, data);
}
//优惠券发放申请
export function couponApply(data) {
  return request.post(`${baseURL}/api/couponsend/apply`, data);
}
//优惠券发放申请
export function couponSendAgree(data) {
  return request.post(`${baseURL}/api/couponsend/agree`, data);
}
//优惠券核销分页查询
export function usrcouponList(data) {
  return request.post(`${baseURL}/api/usrcoupon/list`, data);
}
//优惠券核销详情查询
export function usrcouponDetail(data) {
  return request.post(`${baseURL}/api/usrcoupon/detail`, data);
}
//核销
export function usrcouponCheck(data) {
  return request.post(`${baseURL}/api/usrcoupon/check`, data);
}
//作废
export function usrcouponCancel(data) {
  return request.post(`${baseURL}/api/usrcoupon/cancel`, data);
}
// 升级有礼分页查询
export function upgradeList(data) {
  return request.post(`${baseURL}/api/activityupgrade/list`, data);
}
// 升级有礼删除
export function upgradeDelete(data) {
  return request.post(`${baseURL}/api/activityupgrade/delete`, data);
}
// 升级有礼详情
export function upgradeDetail(data) {
  return request.post(`${baseURL}/api/activityupgrade/detail`, data);
}
// 升级有礼新增
export function upgradeSave(data) {
  return request.post(`${baseURL}/api/activityupgrade/save`, data);
}
// 升级有礼编辑
export function upgradeUpdate(data) {
  return request.post(`${baseURL}/api/activityupgrade/update`, data);
}














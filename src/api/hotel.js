/*
 * @Author: your name
 * @Date: 2019-12-02 11:12:46
 * @LastEditTime: 2020-06-22 17:35:30
 * @LastEditors: Please set LastEditors
 * @Description: 酒店基础管理API
 * @FilePath: \mini-admin\src\api\hotel.js
 */

import request from "@/utils/request";
import { baseURL } from "./index.js";

// 酒店列表分页查询
export function hotelList(data) {
  return request.post(`${baseURL}/api/hotel/list`, data);
}

// 酒店列表下拉
export function hotelSelect(data) {
  return request.post(`${baseURL}/api/hotel/queryNameAndId`, data);
}

// 酒店列表下拉-添加总部节点
export function hotelSelectAll(data) {
  return request.post(`${baseURL}/api/hotel/queryNameAndIdII`, data);
}

//酒店品牌列表
export function hotelBrand(data) {
  return request.post( `${baseURL}/api/hotel/brand`, data);
}

//酒店地址获取-获取省级行政单位
export function hotelAddressProvince(data) {
  return request.post(`${baseURL}/api/hotel/province`, data);
}

//酒店地址获取-获取城市
export function hotelAddressCity(data) {
  return request.post(`${baseURL}/api/hotel/city`, data);
}

//酒店地址获取-获取区
export function hotelAddressDistrict(data) {
  return request.post(`${baseURL}/api/hotel/district`, data);
}

//酒店新增
export function hotelSave(data) {
  return request.post(`${baseURL}/api/hotel/save`, data);
}

//酒店更新
export function hotelEdit(data) {
  return request.post(`${baseURL}/api/hotel/update`, data);
}

//根据酒店id获取酒店明细
export function hotelDetail(data) {
  return request.post(`${baseURL}/api/hotel/detail`, data);
}

//根据酒店id获取酒店图片
export function hotelImages(data) {
  return request.post(`${baseURL}/api/htlResource/hotelImages`, data);
}
//上传酒店图片
export function hotelImagesSave(data) {
  return request.post(`${baseURL}/api/image/saveImg`, data);
}
//删除酒店图片
export function hotelImagesDelete(data) {
  return request.post(`${baseURL}/api/image/deleteImages`, data);
}
//获取酒店关联设施
export function hotelFacility(data) {
  return request.post(`${baseURL}/api/facility/listForHotel`, data);
}
//获取设施列表
export function hotelFacilityList(data) {
  return request.post(`${baseURL}/api/facility/list`, data);
}
//保存酒店介绍
export function hotelSaveIntroduce(data) {
  return request.post(`${baseURL}/api/hotel/saveIntroduce`, data);
}
//酒店提示关联
export function hotelSaveTips(data) {
  return request.post(`${baseURL}/api/hotel/saveTips`, data);
}
//查询酒店提示集合
export function hotelGetTips(data) {
  return request.post(`${baseURL}/api/hotel/tips`, data);
}
//酒店已选标签信息
export function hotelCheckLabel(data) {
  return request.post(`${baseURL}/api/hotel/checkLabel`, data);
}
//酒店未选标签信息
export function hotelUncheckLabel(data) {
  return request.post(`${baseURL}/api/hotel/unCheckLabel`, data);
}
//保存酒店标签
export function hotelSaveLabel(data) {
  return request.post(`${baseURL}/api/hotel/saveLabel`, data);
}
//房型&酒店设施更新
export function hotelSaveFacility(data) {
  return request.post(`${baseURL}/api/facilityRelate/save`, data);
}


/**
 * 房型管理API
 */
//根据id获取房型明细
export function hotelRoomDetail(data) {
  return request.post(`${baseURL}/api/room/detail`, data);
}
//房型保存修改
export function hotelRoomSave(data) {
  return request.post(`${baseURL}/api/room/save`, data);
}
//房型保存修改
export function hotelRoomEdit(data) {
  return request.post(`${baseURL}/api/room/update`, data);
}
//获取房型图片
export function roomGetImages(data) {
  return request.post(`${baseURL}/api/htlResource/roomImages`, data);
}
//获取房型设施
export function roomGetFacility(data) {
  return request.post(`${baseURL}/api/facility/listForRoom`, data);
}

/**
 * 设施管理API
 */

//新增设施
export function facilityAdd(data) {
  return request.post(`${baseURL}/api/facility/add`, data);
}
//修改设施
export function facilityUpdate(data) {
  return request.post(`${baseURL}/api/facility/update`, data);
}
//根据设施id获取设施详情
export function facilityDetail(data) {
  return request.post(`${baseURL}/api/facility/detail`, data);
}
/**
 * 品牌管理API
 */
//分页查询列表信息
export function brandList(data) {
  return request.post(`${baseURL}/api/brand/list`, data);
}
//根据id查询明细
export function brandDetail(data) {
  return request.post(`${baseURL}/api/brand/detail`, data);
}
//查询明细品牌关联图片
export function brandImages(data) {
  return request.post(`${baseURL}/api/htlResource/brandImages`, data);
}
//查询代表酒店
export function brandHotel(data) {
  return request.post(`${baseURL}/api/brand/hotel`, data);
}
//关联代表酒店道品牌
export function brandRelation(data) {
  return request.post(`${baseURL}/api/brand/relation`, data);
}
//保存品牌基本信息
export function brandUpdate(data) {
  return request.post(`${baseURL}/api/brand/update`, data);
}
//新增品牌基本信息
export function brandSave(data) {
  return request.post(`${baseURL}/api/brand/save`, data);
}


/**
 * 房价码API
 */
//根据id获取房价码相关详情
export function hotelPriceDetail(data) {
  return request.post(`${baseURL}/api/room/rate/detail`, data);
}

//新增房价码
export function hotelPriceSave(data) {
  return request.post(`${baseURL}/api/room/rate/save`, data);
}




// 房型列表分页查询
export function roomstyleList(data) {
  return request.post(`${baseURL}/api/room/list`, data);
}

// 房型列表下拉
export function roomstyleSelect(data) {
  return request.post(`${baseURL}/api/room/queryRoomSelect`, data);
}
// 查询酒店ID对应房型列表下拉
export function roomstyleSelectById(data) {
  return request.post(`${baseURL}/api/room/queryNameAndId`, data);
}

// 房价码列表分页查询
export function roompriceList(data) {
  return request.post(`${baseURL}/api/room/rate/list`, data);
}

// 房价码列表下拉
export function roompriceSelect(data) {
  return request.post(`${baseURL}/api/room/rate/queryNameAndId`, data);
}

//查询酒店ID 房型ID 对应房价码列表下拉
export function roompriceSelectById(data) {
  return request.post(`${baseURL}/api/room/rate/queryRoomrateId`, data);
}

// 标签列表分页查询
export function labelList(data) {
  return request.post(`${baseURL}/api/label/list`, data);
}

// 标签新增/编辑
export function labelAction(data) {
  return request.post(`${baseURL}/api/label/save`, data);
}

// 标签新增/编辑
export function labelEdit(data) {
  return request.post(`${baseURL}/api/label/edit`, data);
}

// 标签详情
export function labeDetail(data) {
  return request.post(`${baseURL}/api/label/detail`, data);
}

/**
 * 规则API
 */
// 取消规则分页查询
export function cancelRuleList(data) {
  return request.post(`${baseURL}/api/cancelrule/list`, data);
}
// 取消规则新增
export function cancelRuleSave(data) {
  return request.post(`${baseURL}/api/cancelrule/save`, data);
}
// 取消规则详情
export function cancelRuleDetail(data) {
  return request.post(`${baseURL}/api/cancelrule/detail`, data);
}
// 取消规则编辑
export function cancelRuleUpdate(data) {
  return request.post(`${baseURL}/api/cancelrule/update`, data);
}







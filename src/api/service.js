/*
 * @Author: your name
 * @Date: 2020-12-01 11:16:35
 * @LastEditTime: 2020-12-31 14:55:05
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-admin\src\api\service.js
 */
import request from "@/utils/request";
import { baseURL } from "./index.js";

/**
 *
 * 客房服务评论模板
 */

// 客房服务评论模板分页查询
export function replyList(data) {
  return request.post(`${baseURL}/api/servicereply/list`, data);
}

// 客房服务评论模板详情
export function replyDetail(data) {
  return request.post(`${baseURL}/api/servicereply/detail`, data);
}

// 客房服务评论模板添加
export function replySave(data) {
  return request.post(`${baseURL}/api/servicereply/save`, data);
}

// 客房服务评论模板修改
export function replyUpdate(data) {
  return request.post(`${baseURL}/api/servicereply/update`, data);
}

// 回复模板下拉
export function replypull(data) {
  return request.post(`${baseURL}/api/servicereply/pull`, data);
}

/**
 *
 * 客房服务基本分类
 */

// 客房服务类别分页查询
export function templateList(data) {
  return request.post(`${baseURL}/api/serviceTemplate/category`, data);
}

/**
 *
 * 客房服务模板
 */

// 客房服务项目列表
export function serviceList(data) {
  return request.post(`${baseURL}/api/serviceTemplate/list`, data);
}

// 客房服务项目是否显示
export function serviceShow(data) {
  return request.post(`${baseURL}/api/serviceTemplate/hide`, data);
}

// 酒店服务模板详情
export function hotelServiceDetail(data) {
  return request.post(`${baseURL}/api/serviceTemplate/hotelservice`, data);
}
// 酒店服务模板修改
export function hotelServiceUpdate(data) {
  return request.post(
    `${baseURL}/api/serviceTemplate/hotelserviceupdate`,
    data
  );
}

// 接送服务模板详情
export function carServiceDetail(data) {
  return request.post(`${baseURL}/api/serviceTemplate/pickupservice`, data);
}

// 接送服务模板修改
export function carServiceUpdate(data) {
  return request.post(
    `${baseURL}/api/serviceTemplate/pickupserviceupdate`,
    data
  );
}

// 清扫服务模板详情
export function cleanServiceDetail(data) {
  return request.post(`${baseURL}/api/serviceTemplate/clean`, data);
}

// 清扫服务模板修改
export function cleanServiceUpdate(data) {
  return request.post(`${baseURL}/api/serviceTemplate/cleanupdate`, data);
}

// 物品租赁模板详情
export function leaseServiceDetail(data) {
  return request.post(`${baseURL}/api/serviceTemplate/lease`, data);
}

// 物品租赁模板修改
export function leaseServiceUpdate(data) {
  return request.post(`${baseURL}/api/serviceTemplate/leaseupdate`, data);
}

// 商城模板详情
export function shopServiceDetail(data) {
  return request.post(`${baseURL}/api/serviceTemplate/shop`, data);
}
// 商城模板修改
export function shopServiceUpdate(data) {
  return request.post(`${baseURL}/api/serviceTemplate/shopupdate`, data);
}

// 一键WIFI模板详情
export function wifiServiceDetail(data) {
  return request.post(`${baseURL}/api/serviceTemplate/wifi`, data);
}
// 一键WIFI模板修改
export function wifiServiceUpdate(data) {
  return request.post(`${baseURL}/api/serviceTemplate/wifiupdate`, data);
}

/**
 * 客房服务酒店楼层
 */

// 根据酒店ID查询楼号
export function hotelbuildno(data) {
  return request.post(`${baseURL}/api/serviceroom/buildno`, data);
}
// 根据酒店ID 楼号 查询楼层
export function hotelfloor(data) {
  return request.post(`${baseURL}api/serviceroom/floor`, data);
}
// 根据酒店ID  楼号 楼层 查询房间号
export function hotelroomno(data) {
  return request.post(`${baseURL}/api/serviceroom/roomno`, data);
}
// 打印二维码
export function hotelqrcode(data) {
  return request.downlaodfile(`${baseURL}/api/serviceroom/qrcode`, data);
}

/**
 * 客房服务订单
 */

// 客房服务订单派单
export function orderSend(data) {
  return request.post(`${baseURL}/api/serviceorder/send`, data);
}
// 客房服务订单取消
export function orderCancle(data) {
  return request.post(`${baseURL}/api/serviceorder/cancel`, data);
}
// 客房服务订单列表
export function orderList(data) {
  return request.post(`${baseURL}/api/serviceorder/list`, data);
}
// 客房服务订单详情
export function orderDetail(data) {
  return request.post(`${baseURL}/api/serviceorder/detail`, data);
}
// 工作人员回复订单评论
export function orderReply(data) {
  return request.post(`${baseURL}/api/serviceorder/reply`, data);
}

// 工作人员回复订单评论
export function OrderReplyDetail(data) {
  return request.post(`${baseURL}/api/serviceorder/userreply`, data);
}

/**
 *
 * 排班班次
 */

// 班次列表新增
export function classAdd(data) {
  return request.post(`${baseURL}/api/serviceshift/add`, data);
}
// 班次列表删除
export function classDel(data) {
  return request.post(`${baseURL}/api/serviceshift/delete`, data);
}
// 班次列表分页查询
export function classList(data) {
  return request.post(`${baseURL}/api/serviceshift/list`, data);
}
// 班次列表修改
export function classEdit(data) {
  return request.post(`${baseURL}/api/serviceshift/update`, data);
}

// 班次列表下拉
export function classPull(data) {
  return request.post(`${baseURL}/api/serviceshift/pull`, data);
}

// 员工排班查询
export function employeeList(data) {
  return request.post(`${baseURL}/api/servicestaffshift/list`, data);
}

// 员工排班
export function employeeArrange(data) {
  return request.post(`${baseURL}/api/servicestaffshift/arrange`, data);
}

// 员工取消排班
export function employeeArrangeCancle(data) {
  return request.post(`${baseURL}/api/servicestaffshift/del`, data);
}

// 根据二级组织ID查询部门
export function employeeDept(data) {
  return request.post(`${baseURL}/api/servicestaffshift/dept`, data);
}

// 根据酒店ID查询二级组织
export function employeeSecond(data) {
  return request.post(`${baseURL}/api/servicestaffshift/second`, data);
}

// 根据二级组织ID查询岗位
export function employeePost(data) {
  return request.post(`${baseURL}/api/serviceprocess/gangwei`, data);
}

// 排班人员楼层查询
export function floorList(data) {
  return request.post(`${baseURL}/api/servicestaffshift/floor`, data);
}

// 排班人员楼层修改
export function floorUpdate(data) {
  return request.post(`${baseURL}/api/servicestaffshift/floorupdate`, data);
}

// 排班人员楼层添加
export function floorAddUpdate(data) {
  return request.post(`${baseURL}/api/servicestaffshift/addupdate`, data);
}


/**
 * 服务流程
 */
// 服务流程配置列表
export function processList(data) {
  return request.post(`${baseURL}/api/serviceprocess/list`, data);
}
// 服务流程配置列表下拉
export function processPull(data) {
  return request.post(`${baseURL}/api/serviceprocess/pull`, data);
}
// 服务流程配置详情
export function processDetail(data) {
  return request.post(`${baseURL}/api/serviceprocess/detail`, data);
}
// 流程节点编辑
export function processEdit(data) {
  return request.post(`${baseURL}/api/serviceprocess/nodeupdate`, data);
}
// 流程节点添加
export function processAdd(data) {
  return request.post(`${baseURL}/api/serviceprocess/nodeadd`, data);
}
// 服务流程配置删除
export function processDel(data) {
  return request.post(`${baseURL}/api/serviceprocess/delete`, data);
}
// 投诉与建议分页查询
export function suggestList(data) {
  return request.post(`${baseURL}/api/servicesuggest/list`, data);
}
// 投诉与建议详情
export function suggestDetail(data) {
  return request.post(`${baseURL}/api/servicesuggest/detail`, data);
}
// 建议反馈回复
export function suggestReply(data) {
  return request.post(`${baseURL}/api/servicesuggest/reply`, data);
}


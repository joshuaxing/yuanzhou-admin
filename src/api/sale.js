/*
 * @Author: your name
 * @Date: 2020-03-25 09:40:50
 * @LastEditTime: 2020-11-05 14:52:47
 * @LastEditors: Please set LastEditors
 * @Description: 分销管理API
 * @FilePath: \yuanzhou-admin\src\api\sale.js
 */

import request from "@/utils/request";
import { baseURL } from "./index.js";

/**
 * 分销管理 - 推广人员设置
 */

// 人员审核分页查询
export function saleCheckList(data) {
  return request.post(`${baseURL}/api/saleActivityUser/list`, data);
}

// 人员审核批量通过
export function saleCheckPass(data) {
  return request.post(`${baseURL}/api/saleActivityUser/pass`, data);
}

// 人员审核批量拒绝
export function saleCheckReject(data) {
  return request.post(`${baseURL}/api/saleActivityUser/reject`, data);
}

// 人员列表分页查询
export function salePeopleList(data) {
  return request.post(`${baseURL}/api/saleActivityUser/sales`, data);
}

// 人员列表批量冻结
export function salePeopleFrozen(data) {
  return request.post(`${baseURL}/api/saleActivityUser/frozen`, data);
}

// 推广结算设置详情修改
export function saleDetailEdit(data) {
  return request.post(`${baseURL}/api/saleActivity/memberupdate`, data);
}

// 推广结算设置详情显示
export function saleDetailData(data) {
  return request.post(`${baseURL}/api/saleActivity/detail`, data);
}

/**
 * 分销管理-公众号推广
 */

// 公众号推广是否考核显示
export function gzhCheck(data) {
  return request.post(`${baseURL}/api/saleActivity/check`, data);
}

// 公众号考核审核修改
export function gzhCheckEdit(data) {
  return request.post(`${baseURL}/api/saleActivity/ischeck`, data);
}

/**
 * 分销管理-小程序会员拉新
 */

// 小程序会员拉新是否考核显示
export function memberCheck(data) {
  return request.post(`${baseURL}/api/saleActivity/xcxcheck`, data);
}

// 小程序会员拉新考核审核修改
export function memberCheckEdit(data) {
  return request.post(`${baseURL}/api/saleActivity/xcxischeck`, data);
}


/**
 * 分销管理-金卡会籍销售
 */

// 金卡会籍是否考核显示
export function goldCheck(data) {
  return request.post(`${baseURL}/api/saleActivity/cardcheck`, data);
}

// 金卡会籍考核审核修改
export function goldCheckEdit(data) {
  return request.post(`${baseURL}/api/saleActivity/cardischeck`, data);
}


/**
 * 分销管理 - 分销报表
 */

// 关注公众号统计详细数据列表
export function gzhDataList(data) {
  return request.post(`${baseURL}/api/salerecord/publicdetail`, data);
}
// 关注公众号统计详细数据导出
export function gzhDataListExcel(data) {
  return request.downlaodfile(`${baseURL}/api/salerecord/publicexcel`, data);
}

// 关注公众号统计汇总数据列表
export function gzhDataTotal(data) {
  return request.post(`${baseURL}/api/salerecord/publictotal`, data);
}

// 关注公众号统计汇总数据导出
export function gzhDataTotalExcel(data) {
  return request.downlaodfile(`${baseURL}/api/salerecord/publictotalexcel`, data);
}

// 新会员统计详细数据列表查询
export function memberDataList(data) {
  return request.post(`${baseURL}/api/salerecord/memberdetail`, data);
}

// 新会员统计详细数据列表导出
export function memberDataListExcel(data) {
  return request.downlaodfile(`${baseURL}/api/salerecord/memberexcel`, data);
}

// 新会员统计汇总数据列表查询
export function memberDataTotal(data) {
  return request.post(`${baseURL}/api/salerecord/membertotal`, data);
}

// 新会员统计汇总数据列表导出
export function memberDataTotalExcel(data) {
  return request.downlaodfile(`${baseURL}/api/salerecord/membertotalexcel`, data);
}

// 金卡会籍分销统计详细数据
export function goldDataList(data) {
  return request.post(`${baseURL}/api/salerecord/carddetail`, data);
}

// 金卡会籍分销统计详细导出
export function goldDataListExcel(data) {
  return request.downlaodfile(`${baseURL}/api/salerecord/cardexcel`, data);
}

// 金卡会籍分销统计汇总数据列表
export function goldDataTotal(data) {
  return request.post(`${baseURL}/api/salerecord/cardtotal`, data);
}

// 金卡会籍分销统计汇总数据导出
export function goldDataTotalExcel(data) {
  return request.downlaodfile(`${baseURL}/api/salerecord/cardtotalexcel`, data);
}

// 推广人员全员显示(HR系统未接入过渡期显示)
export function saleActivityList(data) {
  return request.post(`${baseURL}/api/saleActivity/list`, data);
}



/**
 * 分销管理 - 日历房分销
 */

 // 日历房是否考核显示
export function roomCheck(data) {
  return request.post(`${baseURL}/api/saleActivity/bookcheck`, data);
}

// 日历房考核修改 1需要审核 0不需要审核
export function roomCheckEdit(data) {
  return request.post(`${baseURL}/api/saleActivity/bookischeck`, data);
}

// 日历房分销活动分页查询
export function bookHotelList(data) {
  return request.post(`${baseURL}/api/saleActivityHotelbooking/list`, data);
}

// 日历房分销活动添加
export function bookHotelSave(data) {
  return request.post(`${baseURL}/api/saleActivityHotelbooking/save`, data);
}

// 日历房分销活动修改
export function bookHotelUpdate(data) {
  return request.post(`${baseURL}/api/saleActivityHotelbooking/update`, data);
}

// 日历房分销活动修改
export function bookHotelDelete(data) {
  return request.post(`${baseURL}/api/saleActivityHotelbooking/delete`, data);
}


//  日历房统计详细数据列表查询
export function roomDataList(data) {
  return request.post(`${baseURL}/api/salerecord/salehotelbookdetail`, data);
}

//  日历房统计详细数据列表导出
export function roomDataListExcel(data) {
  return request.downlaodfile(`${baseURL}/api/salerecord/roomexcel`, data);
}

//  日历房统计汇总数据列表查询
export function roomDataTotal(data) {
  return request.post(`${baseURL}/api/salerecord/salehotelbooktotal`, data);
}

//  日历房统计汇总数据列表导出
export function roomDataTotalExcel(data) {
  return request.downlaodfile(`${baseURL}/api/salerecord/roomtotalexcel`, data);
}
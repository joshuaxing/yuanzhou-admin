/*
 * @Author: your name
 * @Date: 2020-09-03 13:59:26
 * @LastEditTime: 2020-09-08 14:55:02
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-admin\src\api\ticket.js
 */
import request from "@/utils/request";
import { baseURL } from "./index.js";

/**
 * 券列表分页查询
 */

// 券列表分页查询
export function TicketList(data) {
  return request.post(`${baseURL}/api/consumecoupon/list`, data);
}

// 券列表删除
export function TicketListDelete(data) {
  return request.post(`${baseURL}/api/consumecoupon/delete`, data);
}

// 券列表批量删除
export function TicketListDeleteAll(data) {
  return request.post(`${baseURL}/api/consumecoupon/batch`, data);
}

/**
 * 消费券订单
 */

// 消费券订单列表
export function TicketOrderList(data) {
  return request.post(`${baseURL}/api/consumeorder/list`, data);
}

// 消费券订单发货
export function TicketOrderSend(data) {
  return request.post(`${baseURL}/api/consumeorder/send`, data);
}

// 消费券订单导出
export function TicketOrderExport(data) {
  return request.downlaodfile(`${baseURL}/api/consumeorder/excel`, data);
}

/**
 * 消费券活动分页查询
 */

// 消费券活动分页查询
export function TicketActivityList(data) {
  return request.post(`${baseURL}/api/consumeactive/list`, data);
}

// 消费券活动接口添加
export function TicketActivityAdd(data) {
  return request.post(`${baseURL}/api/consumeactive/save`, data);
}

// 消费券活动修改
export function TicketActivityUpdate(data) {
  return request.post(`${baseURL}/api/consumeactive/update`, data);
}

// 消费券活动详情
export function TicketActivityDetail(data) {
  return request.post(`${baseURL}/api/consumeactive/detail`, data);
}

// 消费券活动导入消费券
export function TicketActivityLead(data) {
  return request.post(`${baseURL}/api/consumeactive/lead`, data);
}


// 消费券活动下拉
export function ticketActivitySelect(data) {
  return request.post(`${baseURL}/api/consumeactive/pull`, data);
}

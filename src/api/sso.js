/*
 * @Author: your name
 * @Date: 2020-09-16 17:13:55
 * @LastEditTime: 2020-09-18 16:26:05
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-admin\src\api\ssoapi.js
 */
import request from "@/utils/request";
import { baseURL } from "./index.js";

/**
 * 统一门户 - 系统账号
 */

// 员工系统映射列表分页查询
export function staffmappingList(data) {
  return request.post(`${baseURL}/api/staffmapping/list`, data);
}

// 映射删除
export function staffmappingDelete(data) {
  return request.post(`${baseURL}/api/staffmapping/delete`, data);
}

// 映射修改
export function staffmappingUpdate(data) {
  return request.post(`${baseURL}/api/staffmapping/update`, data);
}

/**
 * 统一门户 - 员工管理
 */

// 员工操作列表分页查询
export function staffList(data) {
  return request.post(`${baseURL}/api/staff/list`, data);
}

// 用户不同系统账号映射
export function staffMapping(data) {
  return request.post(`${baseURL}/api/staff/mapping`, data);
}

// 业务系统下拉框
export function staffPull(data) {
  return request.post(`${baseURL}/api/staff/pull`, data);
}

// 员工已绑定角色
export function staffReled(data) {
  return request.post(`${baseURL}/api/roles/reled`, data);
}

// 员工绑定角色
export function staffRole(data) {
  return request.post(`${baseURL}/api/staff/releate`, data);
}

/**
 * 统一门户 - 系统管理
 */

// 系统列表分页查询
export function systemList(data) {
  return request.post(`${baseURL}/api/systemlist/list`, data);
}

// 系统添加
export function systemSave(data) {
  return request.post(`${baseURL}/api/systemlist/save`, data);
}

// 系统列表修改
export function systemUpdate(data) {
  return request.post(`${baseURL}/api/systemlist/update`, data);
}


/**
 * 统一门户 - 角色管理
 */

// 统一账号权限列表分页查询
export function rolesList(data) {
  return request.post(`${baseURL}/api/roles/list`, data);
}

// 统一账号角色添加
export function rolesSave(data) {
  return request.post(`${baseURL}/api/roles/save`, data);
}

// 统一账号角色编辑
export function rolesUpdate(data) {
  return request.post(`${baseURL}/api/roles/update`, data);
}

// 统一账号角色详情
export function rolesDetail(data) {
  return request.post(`${baseURL}/api/roles/detail`, data);
}


// 统一账号角色下拉
export function rolesPull(data) {
  return request.post(`${baseURL}/api/roles/all`, data);
}

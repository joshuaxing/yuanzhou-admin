import axios from "axios";
import qs from "qs";
import { Message } from 'element-ui'
import store from "@/store/index";
import router from "@/router/index";
//定义全部http
let http = {};

//请求的公共参数
let arg = {};

// Content-Type: application/json
http.post = function(api, params) {
  arg.token = store.state.user.userinfo.token;
  arg.appcode = store.state.user.code.appcode;
  arg.modcode = store.state.user.code.modcode;
  arg.actcode = store.state.user.code.actcode;
  let data = Object.assign({}, arg, params);
  return new Promise((resolve, reject) => {
    axios
      .post(api, data)
      .then(response => {
        // console.log(response)
        if (response.status === 200) {
          if (response.data.code === 401) {
            //没有权限
            Message.error("您的账号登录已过期，请重新登录")
            localStorage.removeItem('yzzhongtai')
            localStorage.removeItem('codezhongtai')
            sessionStorage.removeItem('yzzhongtainav')
            router.push('/login')
          }
          resolve(response.data);
        } else {
          Message.error(response.status)
        }

      })
      .catch(error => {
        Message.error(error.message)
        reject(error.message);
      });
  });
};
// Content-Type: multipart/form-data
http.downlaodfile = function(api, params) {
  arg.token = store.state.user.userinfo.token;
  arg.appcode = store.state.user.code.appcode;
  arg.modcode = store.state.user.code.modcode;
  arg.actcode = store.state.user.code.actcode;
  let data = Object.assign({}, arg, params);
  return new Promise((resolve, reject) => {
    axios
      .post(api, data, {
        responseType: "blob"
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        Message.error(error.message)
        reject(error.message);
      });
  });
};

http.downlaodimage = function(api, params) {
  console.log(api)
  arg.token = store.state.user.userinfo.token;
  arg.appcode = store.state.user.code.appcode;
  arg.modcode = store.state.user.code.modcode;
  arg.actcode = store.state.user.code.actcode;
  let data = Object.assign({}, arg, params);
  return new Promise((resolve, reject) => {
    axios
      .post(api, data, {
        responseType: "arraybuffer"
      })
      .then(response => {
        // console.log(response)
        resolve(response.data);
      })
      .catch(error => {
        // Message.error(error.message)
        console.log(error)
        reject(error.message);
      });
  });
};

http.file = function(api, params) {
  let data = params;
  return new Promise((resolve, reject) => {
    axios
      .post(api, data, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        Message.error(error.message)
        reject(error.message);
      });
  });
};

// Content-Type: application/x-www-form-urlencoded
http.ajax = function(api, params) {
  let data = Object.assign({}, arg, params);
  return new Promise((resolve, reject) => {
    axios
      .post(
        api,
        {
          data: qs.stringify(data)
        },
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        }
      )
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        Message.error(error.message)
        reject(error.message);
      });
  });
};
// get请求
http.get = function(api, params) {
  let data = Object.assign({}, arg, params);
  return new Promise((resolve, reject) => {
    axios
      .get(api, {
        params: data
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        Message.error(error.message)
        reject(error.message);
      });
  });
};
// http request 拦截器
axios.interceptors.request.use(
  config => {
    return config;
  },
  err => {
    return Promise.reject(err);
  }
);

// http response 拦截器
axios.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if (error && error.response) {
      switch (error.response.status) {
        case 400:
          error.message = "请求错误(400)";
          break;
        case 401:
          error.message = "(401)";
          break;
        case 403:
          error.message = "拒绝访问(403)";
          break;
        case 404:
          error.message = "请求出错(404)";
          break;
        case 408:
          error.message = "请求超时(408)";
          break;
        case 500:
          error.message = "服务器错误(500)";
          break;
        case 501:
          error.message = "服务未实现(501)";
          break;
        case 502:
          error.message = "网络错误(502)";
          break;
        case 503:
          error.message = "服务不可用(503)";
          break;
        case 504:
          error.message = "网络超时(504)";
          break;
        case 505:
          error.message = "HTTP版本不受支持(505)";
          break;
        default:
          error.message = `连接出错(${error.response.status})!`;
      }
    } else {
      error.message = "网络不稳定,请稍后重试";
    }
    return Promise.reject(error);
  }
);

export default http;


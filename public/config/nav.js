var nav = [
  {
    id: 1,
    name: "酒店基础管理",
    code: "basicmanger",
    appid: 3,
    parentid: 0,
    icon: "jiudianguanli",
    link: null,
    children: [
      {
        id: 2,
        name: "酒店基础信息",
        code: "basicinfo",
        appid: 3,
        parentid: 1,
        icon: null,
        link: "/hotel",
        children: []
      },
      {
        id: 3,
        name: "房型管理",
        code: "room",
        appid: 3,
        parentid: 1,
        icon: null,
        link: "/roomstyle",
        children: []
      },
      {
        id: 4,
        name: "房价码管理",
        code: "roomrate",
        appid: 3,
        parentid: 1,
        icon: null,
        link: "/roomprice",
        children: []
      },
      {
        id: 5,
        name: "品牌管理",
        code: "brand",
        appid: 3,
        parentid: 1,
        icon: null,
        link: "/brand",
        children: []
      },
      {
        id: 6,
        name: "设施管理",
        code: "facility",
        appid: 3,
        parentid: 1,
        icon: null,
        link: "/facilities",
        children: []
      },
      {
        id: 7,
        name: "标签管理",
        code: "label",
        appid: 3,
        parentid: 1,
        icon: null,
        link: "/hotellabel",
        children: []
      },
      {
        id: 8,
        name: "取消规则管理",
        code: "rulecancel",
        appid: 3,
        parentid: 1,
        icon: "",
        link: "/rulecancel",
        children: []
      }
    ]
  },
  {
    id: 9,
    name: "渠道管理",
    code: "channel",
    appid: 1,
    parentid: 0,
    icon: "qudaoguanli",
    link: null,
    children: [
      {
        id: 10,
        name: "渠道基本信息",
        code: "channelbase",
        appid: 1,
        parentid: 9,
        icon: null,
        link: "/channelbase",
        children: []
      },
      {
        id: 11,
        name: "房价基价管理",
        code: "crsbaseprice",
        appid: 1,
        parentid: 9,
        icon: null,
        link: "/crsbaseprice",
        children: []
      },
      {
        id: 12,
        name: "房价管理",
        code: "crsprice",
        appid: 1,
        parentid: 9,
        icon: null,
        link: "/crsprice",
        children: []
      },
      {
        id: 13,
        name: "房态管理",
        code: "crsroom",
        appid: 1,
        parentid: 9,
        icon: null,
        link: "/crsroom",
        children: []
      },
      {
        id: 14,
        name: "订单查询",
        code: "order",
        appid: 1,
        parentid: 9,
        icon: null,
        link: "/order",
        children: []
      },
      {
        id: 43,
        name: "预定规则",
        code: "rulebuild",
        appid: 1,
        parentid: 9,
        icon: null,
        link: "/rulebuild",
        children: []
      }
    ]
  },
  {
    id: 15,
    name: "会员管理",
    code: "membermgr",
    appid: 2,
    parentid: 0,
    icon: "huiyuanleiguanli",
    link: null,
    children: [
      {
        id: 16,
        name: "会员查询",
        code: "member",
        appid: 2,
        parentid: 15,
        icon: null,
        link: "/member",
        children: []
      },
      {
        id: 17,
        name: "会员等级配置",
        code: "memberlevel",
        appid: 2,
        parentid: 15,
        icon: null,
        link: "/memberlevel",
        children: []
      },
      {
        id: 18,
        name: "积分兑换配置",
        code: "memberjifenexchange",
        appid: 2,
        parentid: 15,
        icon: null,
        link: "/memberjifenexchange",
        children: []
      },
      {
        id: 19,
        name: "自动升级配置",
        code: "memberlevelrule",
        appid: 2,
        parentid: 15,
        icon: null,
        link: "/memberlevelrule",
        children: []
      },
      {
        id: 20,
        name: "等级特权配置",
        code: "memberlevelset",
        appid: 2,
        parentid: 15,
        icon: null,
        link: "/memberlevelset",
        children: []
      },
      {
        id: 21,
        name: "会员卡配置",
        code: "membercard",
        appid: 2,
        parentid: 15,
        icon: null,
        link: "/membercard",
        children: []
      },
      {
        id: 22,
        name: "会员卡配置",
        code: "membercard",
        appid: 2,
        parentid: 15,
        icon: null,
        link: "/membercard",
        children: []
      }
    ]
  },
  {
    id: 23,
    name: "评论中心",
    code: "commentcenter",
    appid: 1,
    parentid: 0,
    icon: "pinglunzhongxin",
    link: null,
    children: [
      {
        id: 24,
        name: "待回复评论",
        code: "commentpend",
        appid: 1,
        parentid: 23,
        icon: null,
        link: "/commentpend",
        children: []
      },
      {
        id: 25,
        name: "已回复查看",
        code: "commentalready",
        appid: 1,
        parentid: 23,
        icon: null,
        link: "/commentalready",
        children: []
      },
      {
        id: 26,
        name: "回复模板设置",
        code: "commentmould",
        appid: 1,
        parentid: 23,
        icon: null,
        link: "/commentmould",
        children: []
      }
    ]
  },
  {
    id: 27,
    name: "营销活动",
    code: "marketactivity",
    appid: 4,
    parentid: 0,
    icon: "huiyuanleiguanli",
    link: null,
    children: [
      {
        id: 28,
        name: "房价立减活动",
        code: "marketroomprice",
        appid: 4,
        parentid: 27,
        icon: null,
        link: "/marketroomprice",
        children: []
      },
      {
        id: 29,
        name: "新人特惠活动",
        code: "marketnewpeople",
        appid: 4,
        parentid: 27,
        icon: null,
        link: "/marketnewpeople",
        children: []
      },
      {
        id: 30,
        name: "连住早订活动",
        code: "marketcontinous",
        appid: 4,
        parentid: 27,
        icon: null,
        link: "/marketcontinous",
        children: []
      },
      {
        id: 31,
        name: "积分预定活动",
        code: "marketjforder",
        appid: 4,
        parentid: 27,
        icon: null,
        link: "/marketjforder",
        children: []
      },
      {
        id: 44,
        name: "会员日",
        code: "marketbirthday",
        appid: 4,
        parentid: 27,
        icon: null,
        link: "/marketbirthday",
        children: []
      }
    ]
  },
  {
    id: 32,
    name: "营销工具",
    code: "markettool",
    appid: 4,
    parentid: 0,
    icon: "yingxiaogongju",
    link: null,
    children: [
      {
        id: 33,
        name: "会员价",
        code: "memeberprice",
        appid: 4,
        parentid: 32,
        icon: null,
        link: "/memeberprice",
        children: []
      },
      {
        id: 34,
        name: "升级有礼",
        code: "upgrade",
        appid: 4,
        parentid: 32,
        icon: null,
        link: "/upgrade",
        children: []
      }
    ]
  },
  {
    id: 35,
    name: "权限管理",
    code: "authmgr",
    appid: 6,
    parentid: 0,
    icon: "shezhi",
    link: null,
    children: [
      {
        id: 36,
        name: "用户管理",
        code: "employee",
        appid: 6,
        parentid: 35,
        icon: null,
        link: "/employee",
        children: []
      },
      {
        id: 37,
        name: "角色管理",
        code: "role",
        appid: 6,
        parentid: 35,
        icon: null,
        link: "/role",
        children: []
      }
    ]
  },
  {
    id: 38,
    name: "优惠券",
    code: "youhuiquan",
    appid: 7,
    parentid: 0,
    icon: "youhuiquan",
    link: null,
    children: [
      {
        id: 39,
        name: "优惠券应用",
        code: "couponuse",
        appid: 7,
        parentid: 38,
        icon: null,
        link: "/couponuse",
        children: []
      },
      {
        id: 40,
        name: "优惠券核销",
        code: "couponwriteoff",
        appid: 7,
        parentid: 38,
        icon: null,
        link: "/couponwriteoff",
        children: []
      },
      {
        id: 41,
        name: "优惠券发放",
        code: "couponsend",
        appid: 7,
        parentid: 38,
        icon: null,
        link: "/couponsend",
        children: []
      },
      {
        id: 42,
        name: "优惠券来源",
        code: "couponsource",
        appid: 7,
        parentid: 38,
        icon: null,
        link: "/couponsource",
        children: []
      }
    ]
  },
  {
    id: 43,
    name: "权限管理",
    code: "authmgr",
    appid: 6,
    parentid: 0,
    icon: "shezhi",
    link: null,
    children: [
      {
        id: 44,
        name: "用户管理",
        code: "employee",
        appid: 6,
        parentid: 0,
        icon: null,
        link: "/employee",
        children: []
      },
      {
        id: 45,
        name: "角色管理",
        code: "role",
        appid: 6,
        parentid: 0,
        icon: null,
        link: "/role",
        children: []
      }
    ]
  },
  {
    id: 46,
    name: "分销管理",
    code: "fenxiaoguanli",
    appid: 4,
    parentid: 0,
    icon: "yingxiaohuodong",
    link: null,
    children: [
      {
        id: 47,
        name: "分销报表",
        code: "salestatement",
        appid: 4,
        parentid: 27,
        icon: null,
        link: "/salestatement",
        children: []
      },
      {
        id: 48,
        name: "公众号推广",
        code: "ythgzh",
        appid: 4,
        parentid: 27,
        icon: null,
        link: "/ythgzh",
        children: []
      },
      {
        id: 49,
        name: "会员拉新",
        code: "gainmember",
        appid: 4,
        parentid: 27,
        icon: null,
        link: "/gainmember",
        children: []
      },
      {
        id: 40,
        name: "金卡会籍",
        code: "goldsale",
        appid: 4,
        parentid: 27,
        icon: null,
        link: "/goldsale",
        children: []
      },
      {
        id: '',
        name: "订单报表",
        code: "reportorder",
        appid: '',
        parentid: '',
        icon: null,
        link: "/reportorder",
        children: []
      },
      {
        id: '',
        name: "复购率报表",
        code: "reportrepurchase",
        appid: '',
        parentid: '',
        icon: null,
        link: "/reportrepurchase",
        children: []
      },
      {
        id: '',
        name: "日历房分销",
        code: "saleroom",
        appid: '',
        parentid: '',
        icon: null,
        link: "/saleroom",
        children: []
      },
      {
        id: '',
        name: "订单未收帐报表",
        code: "reportordernomoney",
        appid: '',
        parentid: '',
        icon: null,
        link: "/reportordernomoney",
        children: []
      }
    ]
  },
  {
    id: 51,
    name: "类目管理",
    code: "storetype",
    appid: 6,
    parentid: 0,
    icon: "wenjianjia",
    link: null,
    children: [
      {
        id: 52,
        name: "类目基础信息",
        code: "typebase",
        appid: 6,
        parentid: 35,
        icon: null,
        link: "/typebase",
        children: []
      }
    ]
  },
  {
    id: 53,
    name: "分组管理",
    code: "storegroup",
    appid: 6,
    parentid: 0,
    icon: "ziwenjian",
    link: null,
    children: [
      {
        id: 54,
        name: "分组基础信息",
        code: "storegroupbase",
        appid: 6,
        parentid: 35,
        icon: null,
        link: "/storegroupbase",
        children: []
      }
    ]
  },
  {
    id: 55,
    name: "商品管理",
    code: "storegoods",
    appid: 6,
    parentid: 0,
    icon: "yingxiaohuodong",
    link: null,
    children: [
      {
        id: 56,
        name: "发布商品",
        code: "storepushgoods",
        appid: 6,
        parentid: 35,
        icon: null,
        link: "/storepushgoods",
        children: []
      },
      {
        id: 57,
        name: "商品管理",
        code: "storegoodslist",
        appid: 6,
        parentid: 35,
        icon: null,
        link: "/storegoodslist",
        children: []
      }
    ]
  },
  {
    id: 58,
    name: "商城基础设置",
    code: "storebaseset",
    appid: 6,
    parentid: 0,
    icon: "shezhi",
    link: null,
    children: [
      {
        id: 59,
        name: "通用模块",
        code: "storecommon",
        appid: 6,
        parentid: 35,
        icon: null,
        link: "/storecommon",
        children: []
      }
    ]
  },
  {
    id: 60,
    name: "订单管理",
    code: "storeorder",
    appid: 6,
    parentid: 0,
    icon: "dingdanguanli",
    link: null,
    children: [
      {
        id: 61,
        name: "订单列表",
        code: "storeorderlist",
        appid: 6,
        parentid: 35,
        icon: null,
        link: "/storeorderlist",
        children: []
      },
      {
        id: 62,
        name: "售后服务",
        code: "storesaleservice",
        appid: 6,
        parentid: 35,
        icon: null,
        link: "/storesaleservice",
        children: []
      },
      {
        id: 63,
        name: "退换货设置",
        code: "storeexchangeset",
        appid: 6,
        parentid: 35,
        icon: null,
        link: "/storeexchangeset",
        children: []
      },
      {
        id: 65,
        name: "票券核销",
        code: "storeticket",
        link: "/storeticket",
        appid: 6,
        parentid: 35,
        icon: null,
        children: []
      },
    ]
  },
  {
    id: 66,
    appid: 6,
    parentid: 0,
    link: null,
    name: "消费券活动",
    code: "ticketcard",
    icon: "wenjianjia",
    children: [
      {
        id: 67,
        name: "活动列表",
        code: "ticketcardactivity",
        link: "/ticketcardactivity",
        appid: 6,
        parentid: 66,
        icon: null,
        children: []
      },
      {
        id: 67,
        name: "消费券列表",
        code: "ticketcardlist",
        link: "/ticketcardlist",
        appid: 6,
        parentid: 66,
        icon: null,
        children: []
      },
      {
        id: 68,
        name: "消费券订单",
        code: "ticketcardorder",
        link: "/ticketcardorder",
        appid: 6,
        parentid: 66,
        icon: null,
        children: []
      }
    ]
  },
  {
    id: 69,
    name: "统一门户",
    code: "sso",
    appid: 6,
    parentid: 0,
    icon: "qudaoguanli",
    link: null,
    children: [
      {
        id: 70,
        name: "系统账号管理",
        code: "ssoaccount",
        appid: 6,
        parentid: 35,
        icon: null,
        link: "/ssoaccount",
        children: []
      },
      {
        id: 71,
        name: "员工管理",
        code: "ssoemployee",
        appid: 6,
        parentid: 35,
        icon: null,
        link: "/ssoemployee",
        children: []
      },
      {
        id: 72,
        name: "系统管理",
        code: "ssosystem",
        appid: 6,
        parentid: 35,
        icon: null,
        link: "/ssosystem",
        children: []
      }
    ]
  },
];

// 最大id已更新到72

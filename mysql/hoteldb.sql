/*
 Navicat Premium Data Transfer

 Source Server         : yuanzhoutest
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 106.14.5.30:3306
 Source Schema         : hoteldb

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 26/02/2021 16:42:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for activity_card
-- ----------------------------
DROP TABLE IF EXISTS `activity_card`;
CREATE TABLE `activity_card`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activityid` int(11) NOT NULL COMMENT '活动id',
  `cardid` int(11) NOT NULL COMMENT '会员卡id',
  `type` tinyint(4) NOT NULL COMMENT '活动类型 1会员价',
  `category` tinyint(4) NULL DEFAULT NULL COMMENT '会员卡类别',
  `discount` decimal(10, 2) NOT NULL COMMENT '折扣',
  `score` int(11) NULL DEFAULT NULL COMMENT '积分',
  `balance` decimal(10, 2) NULL DEFAULT NULL COMMENT '余额',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 59 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '活动-会员卡关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for activity_card_coupon
-- ----------------------------
DROP TABLE IF EXISTS `activity_card_coupon`;
CREATE TABLE `activity_card_coupon`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_card_id` int(11) NOT NULL COMMENT '活动会员卡id',
  `couponid` int(11) NOT NULL COMMENT '优惠券id',
  `count` int(4) NOT NULL COMMENT '优惠券赠送数量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '活动会员等级-优惠券关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for activity_channel
-- ----------------------------
DROP TABLE IF EXISTS `activity_channel`;
CREATE TABLE `activity_channel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activityid` int(11) NOT NULL COMMENT '活动id',
  `channelid` int(11) NOT NULL COMMENT '渠道id',
  `activitytype` tinyint(4) NOT NULL COMMENT '活动类型 1积分预定 2房价立减 3新人特惠 4连住早订',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 729 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '活动-渠道关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for activity_decrease
-- ----------------------------
DROP TABLE IF EXISTS `activity_decrease`;
CREATE TABLE `activity_decrease`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '活动名称',
  `roomprice_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房价名称',
  `startdate` datetime(0) NOT NULL COMMENT '活动开始时间',
  `enddate` datetime(0) NOT NULL COMMENT '活动结束时间',
  `booking_startdate` datetime(0) NOT NULL COMMENT '可预订开始时间',
  `booking_enddate` datetime(0) NOT NULL COMMENT '可预订结束时间',
  `discounttype` tinyint(1) NOT NULL COMMENT '优惠类型 1折扣 2立减金额',
  `discount` decimal(10, 2) NULL DEFAULT NULL COMMENT '折扣',
  `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '减免金额',
  `content` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '优惠内容',
  `notice` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '使用须知',
  `status` tinyint(1) NOT NULL COMMENT '状态 1有效 2无效',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '房价立减活动' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for activity_memberday
-- ----------------------------
DROP TABLE IF EXISTS `activity_memberday`;
CREATE TABLE `activity_memberday`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '活动名称',
  `roomprice_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '房价名称',
  `startdate` datetime(0) NOT NULL COMMENT '活动开始日期',
  `enddate` datetime(0) NOT NULL COMMENT '活动结束日期',
  `booking_startdate` datetime(0) NOT NULL COMMENT '预定开始日期',
  `booking_enddate` datetime(0) NOT NULL COMMENT '预定结束日期',
  `discounttype` tinyint(1) NOT NULL COMMENT '优惠类型 1折扣 2立减金额',
  `discount` decimal(10, 2) NULL DEFAULT NULL COMMENT '折扣',
  `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '减免金额',
  `participants` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参与对象',
  `reward` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '活动奖励 1 入住后获得双倍积分 2 签到获得双倍积分',
  `notice` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '使用须知',
  `status` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '状态 1有效 2无效',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员日活动' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for activity_memberprice
-- ----------------------------
DROP TABLE IF EXISTS `activity_memberprice`;
CREATE TABLE `activity_memberprice`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '活动名称',
  `startdate` datetime(0) NOT NULL COMMENT '活动开始时间',
  `enddate` datetime(0) NOT NULL COMMENT '活动结束时间',
  `discount_way` tinyint(2) NULL DEFAULT NULL COMMENT '优惠方式 1折扣',
  `status` tinyint(1) NOT NULL COMMENT '状态 1有效 2无效',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员价' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for activity_newcomer
-- ----------------------------
DROP TABLE IF EXISTS `activity_newcomer`;
CREATE TABLE `activity_newcomer`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '活动名称',
  `roomprice_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房价名称',
  `startdate` datetime(0) NOT NULL COMMENT '活动开始时间',
  `enddate` datetime(0) NOT NULL COMMENT '活动结束时间',
  `booking_startdate` datetime(0) NOT NULL COMMENT '可预订开始时间',
  `booking_enddate` datetime(0) NOT NULL COMMENT '可预订结束时间',
  `nights` tinyint(1) NOT NULL COMMENT '优惠晚数量 1首晚 2不限晚数',
  `room` tinyint(1) NOT NULL COMMENT '优惠房间 1首间房 2不限间数',
  `discounttype` tinyint(1) NOT NULL COMMENT '优惠类型 1平台新人 2单店新人',
  `discount` decimal(10, 2) NULL DEFAULT NULL COMMENT '折扣',
  `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '减免金额',
  `content` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '优惠内容',
  `notice` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '使用须知',
  `status` tinyint(1) NOT NULL COMMENT '状态 1有效 2无效',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `saletype` tinyint(1) NULL DEFAULT NULL COMMENT '优惠类型 1折扣 2立减金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '新人优惠活动' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for activity_room
-- ----------------------------
DROP TABLE IF EXISTS `activity_room`;
CREATE TABLE `activity_room`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activityid` int(11) NOT NULL COMMENT '活动id',
  `roomid` int(11) NOT NULL COMMENT '房型id',
  `roomrate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '房价码',
  `activitytype` tinyint(4) NOT NULL COMMENT '活动类型 1积分预定 2房价立减 3新人特惠 4连住早订 5会员日',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `hotelid` int(11) NULL DEFAULT NULL COMMENT '酒店ID',
  `begin_date` datetime(0) NULL DEFAULT NULL COMMENT '房价基价开始时间',
  `end_date` datetime(0) NULL DEFAULT NULL COMMENT '房价基价结束时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 63 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '活动-房型关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for activity_stayearly
-- ----------------------------
DROP TABLE IF EXISTS `activity_stayearly`;
CREATE TABLE `activity_stayearly`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '活动名称',
  `roomprice_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房价名称',
  `startdate` datetime(0) NOT NULL COMMENT '活动开始时间',
  `enddate` datetime(0) NOT NULL COMMENT '活动结束时间',
  `booking_startdate` datetime(0) NOT NULL COMMENT '可预订开始时间',
  `booking_enddate` datetime(0) NOT NULL COMMENT '可预订结束时间',
  `type` tinyint(1) NOT NULL COMMENT '1连住优惠 2早订优惠',
  `days` int(4) NOT NULL COMMENT '连住/提前天数',
  `discount` decimal(10, 2) NULL DEFAULT NULL COMMENT '折扣',
  `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '减免金额',
  `content` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '优惠内容',
  `notice` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '使用须知',
  `status` tinyint(1) NOT NULL COMMENT '状态 1有效 2无效',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `saletype` tinyint(1) NULL DEFAULT NULL COMMENT '优惠类型 1折扣 2立减金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '连住早定活动' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for activity_upgrade
-- ----------------------------
DROP TABLE IF EXISTS `activity_upgrade`;
CREATE TABLE `activity_upgrade`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '活动名称',
  `startdate` datetime(0) NOT NULL COMMENT '活动开始时间',
  `enddate` datetime(0) NOT NULL COMMENT '活动结束时间',
  `gifttype` tinyint(1) NOT NULL COMMENT '赠送类型 1限一次 2不限次数',
  `status` tinyint(1) NOT NULL COMMENT '1有效 2无效',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `create_date` datetime(0) NOT NULL,
  `update_date` datetime(0) NOT NULL,
  `channelid` int(11) NOT NULL COMMENT '渠道表主键ID',
  `category` tinyint(4) NOT NULL COMMENT '会员卡类别   1 悦廷会',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员卡升级活动' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for activity_upgrade_prize_rel
-- ----------------------------
DROP TABLE IF EXISTS `activity_upgrade_prize_rel`;
CREATE TABLE `activity_upgrade_prize_rel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activityupgradeid` int(11) NOT NULL COMMENT '会员卡升级活动主键id',
  `cardid` int(11) NOT NULL COMMENT '卡 主键id',
  `conditionname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '升级条件',
  `sendcontent` tinyint(1) NOT NULL COMMENT '赠送内容 1优惠券 2积分 3余额',
  `couponid` int(11) NULL DEFAULT NULL COMMENT '优惠券主键id 赠送内容为1优惠券时，有值',
  `score` int(11) NULL DEFAULT NULL COMMENT '积分 赠送内容为2积分，有值',
  `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '金额 赠送内容为3余额，有值',
  `create_date` datetime(0) NOT NULL,
  `update_date` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 66 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '升级有礼关联奖品中间表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for basic_room_chanel_rel
-- ----------------------------
DROP TABLE IF EXISTS `basic_room_chanel_rel`;
CREATE TABLE `basic_room_chanel_rel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `basic_room_price_id` int(11) NOT NULL COMMENT '房价基价主键ID',
  `channelid` int(11) NOT NULL COMMENT '渠道表主键ID',
  `rulesid` int(11) NOT NULL COMMENT '取消规则表主键ID',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 234 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '房价基价渠道来源关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for basic_room_price
-- ----------------------------
DROP TABLE IF EXISTS `basic_room_price`;
CREATE TABLE `basic_room_price`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roomrule` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房价规则名称',
  `hotelid` int(11) NOT NULL COMMENT '酒店主键ID',
  `reserveid` int(11) NOT NULL COMMENT '预订规则主键ID',
  `begin_date` datetime(0) NOT NULL COMMENT '入住开始时间',
  `end_date` datetime(0) NOT NULL COMMENT '入住结束时间',
  `basic_date_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作用天数 平日：1234，平日+星期天：71234，周末：56，周末+星期天：567',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '是否启用 1：启用 0：禁用',
  `is_synchr` tinyint(4) NOT NULL DEFAULT 1 COMMENT '是否同步到room_price 1：未同步 0：同步',
  `synchr_mark` tinyint(1) NOT NULL DEFAULT 1 COMMENT '同步标记 用于判断与room_price表中数据是否一致',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 230 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '房价基价主表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for basic_room_rate_rel
-- ----------------------------
DROP TABLE IF EXISTS `basic_room_rate_rel`;
CREATE TABLE `basic_room_rate_rel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `basic_room_price_id` int(11) NOT NULL COMMENT '房价基价主键ID',
  `roomcode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房型码',
  `roomcodecount` int(11) NULL DEFAULT NULL COMMENT '房型库存',
  `roomrate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房价码',
  `roomrate_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '房价码价格',
  `roomrate_count` int(11) NULL DEFAULT NULL COMMENT '房价库存',
  `islimit` tinyint(4) NOT NULL DEFAULT 1 COMMENT '是否设置限量库存 1：是 0：否',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `roomid` int(11) NOT NULL COMMENT '房间 room表主键ID',
  `roomcount` int(11) NULL DEFAULT NULL,
  `breakfastcount` int(11) NULL DEFAULT NULL,
  `room_rate_id` int(11) NOT NULL COMMENT 'room-rate主键ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 627 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '房价基价关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for booking_rule
-- ----------------------------
DROP TABLE IF EXISTS `booking_rule`;
CREATE TABLE `booking_rule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '规则名称',
  `commonrule` tinyint(1) NULL DEFAULT 1 COMMENT '通用规则 1是 2否',
  `bookingdays` int(4) NULL DEFAULT NULL COMMENT '最多可预定天数',
  `bookingbegintime` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最晚预定开始时间',
  `bookingendtime` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最晚预定结束时间',
  `roomcount_type` int(4) NOT NULL COMMENT '最多可预定房间数类型  0 有限  1 不限',
  `roomcount` int(4) NULL DEFAULT NULL COMMENT '最多可定房间数量',
  `daterange_type` int(4) NOT NULL COMMENT '订房日期范围类型  0 有限   1 不限 ',
  `daterange` int(4) NULL DEFAULT NULL COMMENT '预定日期范围',
  `notes` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预定须知',
  `channel` tinyint(10) NOT NULL DEFAULT 0 COMMENT '渠道主键ID',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '预定规则' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for brand
-- ----------------------------
DROP TABLE IF EXISTS `brand`;
CREATE TABLE `brand`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brandname` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `logo` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `brandlocation` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `englishname` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `introduce` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `feature` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌特色',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cancel_rule
-- ----------------------------
DROP TABLE IF EXISTS `cancel_rule`;
CREATE TABLE `cancel_rule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '规则名称',
  `code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编码',
  `type` tinyint(4) NOT NULL COMMENT '类型 1限时取消 2免费取消 3不可取消 ',
  `descrip` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规则说明',
  `days` int(4) NULL DEFAULT NULL COMMENT '入住时间提前天数',
  `time` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '时间',
  `template_content` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '取消条件内容模板',
  `finetype` tinyint(1) NULL DEFAULT NULL COMMENT '罚金类型 1百分比 2固定金额 3间夜',
  `money` decimal(10, 2) NULL DEFAULT NULL COMMENT '金额/百分比',
  `nights` int(4) NULL DEFAULT NULL COMMENT '间夜数',
  `status` tinyint(1) NULL DEFAULT 0 COMMENT '状态 1启用 0禁用',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单取消规则' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for channel
-- ----------------------------
DROP TABLE IF EXISTS `channel`;
CREATE TABLE `channel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channelname` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '渠道名称',
  `type` int(11) NULL DEFAULT NULL COMMENT '渠道类型  1 直销  2 分销',
  `validtype` int(4) NULL DEFAULT NULL COMMENT '有效期类型 1长期有效 2固定日期',
  `startdate` datetime(0) NULL DEFAULT NULL COMMENT '有效期开始时间',
  `enddate` datetime(0) NULL DEFAULT NULL COMMENT '有效期结束时间',
  `descrip` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接口配置说明',
  `code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编码',
  `confirmway` int(4) NULL DEFAULT NULL COMMENT '预定确认方式 1立即确认 2酒店前台接单后确认',
  `hours` int(4) NULL DEFAULT NULL COMMENT '小时数',
  `create_date` datetime(0) NULL DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '状态 1启用 0禁用',
  `update_date` datetime(0) NULL DEFAULT NULL,
  `is_deleted` tinyint(4) UNSIGNED ZEROFILL NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '渠道' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city`  (
  `id` int(11) NOT NULL,
  `cityname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pinyin` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `provinceid` int(11) NOT NULL DEFAULT 0,
  `countryid` int(11) NOT NULL DEFAULT 0,
  `hotelcount` int(11) NOT NULL DEFAULT 0,
  `cname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for country
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country`  (
  `id` int(11) NOT NULL,
  `countryname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '	' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for district
-- ----------------------------
DROP TABLE IF EXISTS `district`;
CREATE TABLE `district`  (
  `id` int(11) NOT NULL,
  `districtname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pinyin` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cityid` int(11) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for facility_relate
-- ----------------------------
DROP TABLE IF EXISTS `facility_relate`;
CREATE TABLE `facility_relate`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facilityid` int(11) NULL DEFAULT NULL,
  `relateid` int(11) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 358 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for hotel
-- ----------------------------
DROP TABLE IF EXISTS `hotel`;
CREATE TABLE `hotel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotelname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `star` tinyint(10) NULL DEFAULT NULL,
  `score` float NULL DEFAULT NULL,
  `cityid` int(11) NULL DEFAULT NULL,
  `zoneid` int(11) NOT NULL DEFAULT 0,
  `districtid` int(11) NOT NULL DEFAULT 0,
  `brandid` int(11) NOT NULL DEFAULT 0,
  `advantage` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `introduce` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lat` float NULL DEFAULT NULL,
  `lon` float NULL DEFAULT NULL,
  `tel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `opendate` datetime(0) NULL DEFAULT NULL,
  `hotelimage` int(11) NULL DEFAULT NULL,
  `hotelcode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `enable` tinyint(4) NOT NULL DEFAULT 1,
  `bookingtel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fax` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `policy` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `roomcount` int(11) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `renovationTime` datetime(0) NULL DEFAULT NULL COMMENT '最近装修时间',
  `type` tinyint(4) UNSIGNED NULL DEFAULT NULL COMMENT '酒店类型 0 自营 1 非自营',
  `sort` int(11) NOT NULL COMMENT '排序字段',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 108 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for htl_facility
-- ----------------------------
DROP TABLE IF EXISTS `htl_facility`;
CREATE TABLE `htl_facility`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facilityname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `facilitycode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` int(2) NULL DEFAULT NULL,
  `description` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `imageurl` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 74 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for htl_resource
-- ----------------------------
DROP TABLE IF EXISTS `htl_resource`;
CREATE TABLE `htl_resource`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotelid` int(11) NULL DEFAULT NULL,
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `resourcetype` int(11) NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  `refid` int(11) NULL DEFAULT NULL,
  `title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `catalog` int(4) NULL DEFAULT NULL,
  `description` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 963 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for htl_sort
-- ----------------------------
DROP TABLE IF EXISTS `htl_sort`;
CREATE TABLE `htl_sort`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotelid` int(11) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for htl_tips
-- ----------------------------
DROP TABLE IF EXISTS `htl_tips`;
CREATE TABLE `htl_tips`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotelid` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT 1,
  `title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tips` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 92 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for label
-- ----------------------------
DROP TABLE IF EXISTS `label`;
CREATE TABLE `label`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `labelname` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 67 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for label_relate
-- ----------------------------
DROP TABLE IF EXISTS `label_relate`;
CREATE TABLE `label_relate`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `labelid` int(11) NULL DEFAULT NULL,
  `relateid` int(11) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 179 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for map_citycode
-- ----------------------------
DROP TABLE IF EXISTS `map_citycode`;
CREATE TABLE `map_citycode`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cityname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `adcode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `citycode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3528 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for province
-- ----------------------------
DROP TABLE IF EXISTS `province`;
CREATE TABLE `province`  (
  `id` int(11) NOT NULL,
  `provincename` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `countryid` int(11) NOT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for room
-- ----------------------------
DROP TABLE IF EXISTS `room`;
CREATE TABLE `room`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotelid` int(11) NOT NULL,
  `roomname` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `roomcode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `retailprice` decimal(10, 2) NULL DEFAULT NULL,
  `minprice` decimal(10, 2) NULL DEFAULT NULL,
  `roomcount` int(11) NULL DEFAULT NULL,
  `breakfast` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `notes` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `area` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bed` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bedcount` int(2) NULL DEFAULT NULL,
  `peoplecount` int(2) NULL DEFAULT NULL,
  `floor` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `windows` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `broadband` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `roomimage` int(11) NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `sort` int(11) NOT NULL DEFAULT 0,
  `overbooking` int(11) NULL DEFAULT 0,
  `peplecount` int(11) NULL DEFAULT NULL,
  `doorlock` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `takepower` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `feature` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 66 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for room_price
-- ----------------------------
DROP TABLE IF EXISTS `room_price`;
CREATE TABLE `room_price`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roomid` int(11) NOT NULL,
  `hotelid` int(11) NOT NULL,
  `roomdate` datetime(0) NOT NULL,
  `price` decimal(10, 2) NULL DEFAULT NULL,
  `roomcount` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `breakfastcount` int(11) NULL DEFAULT NULL,
  `roomrate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hotelcode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `roomcode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `salecount` int(11) NOT NULL DEFAULT 0,
  `channelid` int(11) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `basic_room_price_id` int(11) NOT NULL COMMENT '房价基价主键ID',
  `synchr_mark` tinyint(1) NULL DEFAULT 1 COMMENT '同步标记 用于判断与basic_room_price表中数据是否一致',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5203021 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for room_price_unused
-- ----------------------------
DROP TABLE IF EXISTS `room_price_unused`;
CREATE TABLE `room_price_unused`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roomid` int(11) NOT NULL,
  `hotelid` int(11) NOT NULL,
  `roomdate` datetime(0) NOT NULL,
  `price` decimal(10, 2) NULL DEFAULT NULL,
  `roomcount` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `breakfastcount` int(11) NULL DEFAULT NULL,
  `roomrate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hotelcode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `roomcode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `salecount` int(11) NOT NULL DEFAULT 0,
  `channelid` int(11) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `basic_room_price_id` int(11) NOT NULL COMMENT '房价基价主键ID',
  `synchr_mark` tinyint(1) NULL DEFAULT 1 COMMENT '同步标记 用于判断与basic_room_price表中数据是否一致',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5196528 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for room_rate
-- ----------------------------
DROP TABLE IF EXISTS `room_rate`;
CREATE TABLE `room_rate`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roomrate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ratename` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `sort` int(11) NULL DEFAULT 0,
  `type` int(11) NULL DEFAULT NULL,
  `pms_roomrate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pms_roomrate_val` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'pms房价码关联值',
  `bookingtype` int(11) NULL DEFAULT NULL,
  `breakfastcount` int(11) NULL DEFAULT 0,
  `valid` tinyint(4) NULL DEFAULT NULL,
  `begindate` datetime(0) NULL DEFAULT NULL,
  `enddate` datetime(0) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 71 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for roomprice_cancelrule
-- ----------------------------
DROP TABLE IF EXISTS `roomprice_cancelrule`;
CREATE TABLE `roomprice_cancelrule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roomid` int(11) NOT NULL COMMENT '房型id',
  `roomrate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '房价码',
  `roomdate` datetime(0) NOT NULL COMMENT '日期',
  `cancelrule_id` int(11) NOT NULL COMMENT '取消规则id',
  `channel_id` int(11) NOT NULL COMMENT '渠道id',
  `basic_room_price_id` int(11) NULL DEFAULT NULL COMMENT '房价基价主键ID',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30093 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '房态-取消规则关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for service_category
-- ----------------------------
DROP TABLE IF EXISTS `service_category`;
CREATE TABLE `service_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '项目名称',
  `type` int(3) NULL DEFAULT NULL COMMENT '类别1 一级（基础服务）2 二级',
  `code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编码',
  `url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '跳转url',
  `icon_url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'icon地址',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '服务类目' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_cleantime
-- ----------------------------
DROP TABLE IF EXISTS `service_cleantime`;
CREATE TABLE `service_cleantime`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateid` int(11) NOT NULL COMMENT '服务模板id',
  `time_range` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '时间范围',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 103 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '清扫服务时间' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_comment
-- ----------------------------
DROP TABLE IF EXISTS `service_comment`;
CREATE TABLE `service_comment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL COMMENT '用户id',
  `service_orderid` int(11) NOT NULL COMMENT '服务订单id',
  `hotelid` int(11) NULL DEFAULT NULL COMMENT '酒店id',
  `hotelcode` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '酒店code',
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评论内容',
  `star` double(2, 1) NULL DEFAULT 0.0 COMMENT '评论星级',
  `reply_status` int(1) NULL DEFAULT 0 COMMENT '回复状态 0待回复 1已回复',
  `anonymous` int(1) NULL DEFAULT 0 COMMENT '匿名 1匿名 0不匿名',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客服服务-评论' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_comment_reply
-- ----------------------------
DROP TABLE IF EXISTS `service_comment_reply`;
CREATE TABLE `service_comment_reply`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commentid` int(11) NOT NULL COMMENT '评论id',
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回复内容',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客服服务评论回复' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_lease
-- ----------------------------
DROP TABLE IF EXISTS `service_lease`;
CREATE TABLE `service_lease`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateid` int(11) NOT NULL COMMENT '服务模板id',
  `goodsname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物品名称',
  `imageurl` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物品图片路径',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 76 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客服服务-租赁物品' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_order
-- ----------------------------
DROP TABLE IF EXISTS `service_order`;
CREATE TABLE `service_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL COMMENT '会员id',
  `orderno` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务订单号',
  `urgency` int(2) NULL DEFAULT NULL COMMENT '服务紧急度',
  `templateid` int(11) NOT NULL COMMENT '服务模板id',
  `processid` int(11) NULL DEFAULT NULL COMMENT '服务流程id',
  `categorycode` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务类别编码',
  `categoryid` int(11) NULL DEFAULT NULL COMMENT '服务类别id',
  `categoryname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别名称（服务项目名称）',
  `temp_itemid` int(11) NULL DEFAULT NULL COMMENT '酒店服务服务项id',
  `temp_item_reply` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '酒店服务用户回复文本',
  `temp_item_replytype` tinyint(1) NULL DEFAULT NULL COMMENT '酒店服务用户回复文本类型 1.时间单选控件 2.日期控件 3时间控件4文本控件 5.单选框（是/否）',
  `temp_item_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '酒店服务服务项名称',
  `temp_item_desc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '酒店服务项描述',
  `orderid` int(11) NULL DEFAULT NULL COMMENT '酒店订单id',
  `crs_orderid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '酒店订单crs订单号',
  `status` int(2) NOT NULL COMMENT '状态 1待确认 2待服务 3服务完成 4服务结束(已取消) ',
  `comment_status` tinyint(1) NULL DEFAULT 1 COMMENT '评论状态 1待评价 2已评价',
  `hotelid` int(11) NULL DEFAULT NULL COMMENT '酒店id',
  `hotelcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '酒店编码',
  `roomno` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务房间号',
  `guest_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客人姓名',
  `guest_phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客人电话',
  `service_time` datetime(0) NULL DEFAULT NULL COMMENT '服务时间',
  `service_end_time` datetime(0) NULL DEFAULT NULL COMMENT '服务截止时间',
  `confirm_time` datetime(0) NULL DEFAULT NULL COMMENT '服务确认时间',
  `guest_remark` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客人备注（服务要求）',
  `confirm_user` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '确认人账号',
  `confirm_username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '确认人姓名',
  `service_staffid` int(11) NULL DEFAULT NULL COMMENT '服务人员id',
  `service_username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务人员姓名',
  `service_phone` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务人员电话',
  `service_finish_time` datetime(0) NULL DEFAULT NULL COMMENT '服务完成时间',
  `adressid` int(11) NULL DEFAULT NULL COMMENT '接送地址id',
  `pickup_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接送地址简称',
  `pickup_address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接送地址',
  `service_remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务备注',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 298 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '服务订单' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_order_lease
-- ----------------------------
DROP TABLE IF EXISTS `service_order_lease`;
CREATE TABLE `service_order_lease`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_orderid` int(11) NOT NULL COMMENT '服务单id',
  `leaseid` int(11) NULL DEFAULT NULL COMMENT '物品租赁id',
  `goodsname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物品名称',
  `imageurl` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物品图片地址',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 115 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '服务订单租赁的物品' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_order_servicetime_record
-- ----------------------------
DROP TABLE IF EXISTS `service_order_servicetime_record`;
CREATE TABLE `service_order_servicetime_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_shift_order_id` int(11) NOT NULL COMMENT '员工派发的服务单id',
  `service_time` datetime(0) NULL DEFAULT NULL COMMENT '服务时间',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '服务单服务时间修改记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_order_status_record
-- ----------------------------
DROP TABLE IF EXISTS `service_order_status_record`;
CREATE TABLE `service_order_status_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_order_id` int(11) NOT NULL COMMENT '服务单id',
  `status` tinyint(1) NOT NULL COMMENT '状态 1待确认 2待服务 3服务完成 4服务结束(已取消) 5已评价',
  `describ` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 382 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '服务单状态修改记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_pickupaddress
-- ----------------------------
DROP TABLE IF EXISTS `service_pickupaddress`;
CREATE TABLE `service_pickupaddress`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateid` int(11) NOT NULL COMMENT '服务模板id',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址名',
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '具体地址',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 58 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客服服务-接送地址' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_process
-- ----------------------------
DROP TABLE IF EXISTS `service_process`;
CREATE TABLE `service_process`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程名称',
  `hotelid` int(11) NOT NULL COMMENT '酒店id',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '服务流程配置' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_process_flow
-- ----------------------------
DROP TABLE IF EXISTS `service_process_flow`;
CREATE TABLE `service_process_flow`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_order_id` int(11) NOT NULL COMMENT '服务单id',
  `process_node_id` int(11) NOT NULL COMMENT '流程节点id',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 136 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '服务流程节点流转' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_process_node
-- ----------------------------
DROP TABLE IF EXISTS `service_process_node`;
CREATE TABLE `service_process_node`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_process_id` int(11) NOT NULL COMMENT '服务流程id',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点名称',
  `number` tinyint(2) NOT NULL COMMENT '节点序号（标识节点顺序 eg:节点1 序号为1）',
  `gangwei_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位id（hr数据岗位id）',
  `gangwei_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称（hr数据岗位名称）',
  `dept_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门id（hr部门id）',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 57 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '服务流程节点' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_process_record
-- ----------------------------
DROP TABLE IF EXISTS `service_process_record`;
CREATE TABLE `service_process_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_order_id` int(11) NOT NULL COMMENT '服务单id',
  `template_id` int(11) NOT NULL COMMENT '服务模板id',
  `process_node_id` int(11) NOT NULL COMMENT '流程节点id',
  `process_node_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程节点名称',
  `process_node_number` tinyint(2) NOT NULL COMMENT '流程节点序号',
  `gangwei_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位id',
  `gangwei_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 135 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '服务单流程流转记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_reply
-- ----------------------------
DROP TABLE IF EXISTS `service_reply`;
CREATE TABLE `service_reply`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板名称',
  `hotelid` int(11) NULL DEFAULT NULL COMMENT '酒店id',
  `content` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内容',
  `status` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态 1启用 2不启用',
  `type` tinyint(1) NOT NULL COMMENT '类型 1评论回复 2提交服务后显示话术 3投诉建议回复',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客房服务-评价回复模板' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_room
-- ----------------------------
DROP TABLE IF EXISTS `service_room`;
CREATE TABLE `service_room`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotelid` int(11) NULL DEFAULT NULL COMMENT '酒店id',
  `hotelcode` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '酒店code',
  `roomno` int(5) NULL DEFAULT NULL COMMENT '房间号',
  `roomcode` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房型code',
  `roomname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房型名称描述',
  `buildno` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '楼栋号',
  `floor` int(3) NULL DEFAULT NULL COMMENT '楼层',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 487 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客房服务房间' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_room_test
-- ----------------------------
DROP TABLE IF EXISTS `service_room_test`;
CREATE TABLE `service_room_test`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotelid` int(11) NULL DEFAULT NULL COMMENT '酒店id',
  `hotelcode` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '酒店code',
  `roomno` int(5) NULL DEFAULT NULL COMMENT '房间号',
  `roomcode` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房型code',
  `roomname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房型名称描述',
  `buildno` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '楼栋号',
  `floor` int(3) NULL DEFAULT NULL COMMENT '楼层',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 893 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客房服务房间' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_shift
-- ----------------------------
DROP TABLE IF EXISTS `service_shift`;
CREATE TABLE `service_shift`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotelid` int(11) NOT NULL COMMENT '酒店id',
  `color_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '颜色代码',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '班次名称',
  `name_abbr` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '班次简称',
  `star_time` time(0) NOT NULL COMMENT '班次开始时间',
  `end_time` time(0) NOT NULL COMMENT '班次结束时间',
  `phone` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务手机号',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客房服务-班次' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_shift_order
-- ----------------------------
DROP TABLE IF EXISTS `service_shift_order`;
CREATE TABLE `service_shift_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staffid` int(11) NOT NULL COMMENT '员工id',
  `staff_shift_id` int(11) NOT NULL COMMENT '员工排班id',
  `service_orderid` int(11) NOT NULL COMMENT '服务订单id',
  `status` int(2) NOT NULL COMMENT '服务订单状态 1待确认 2待服务 3服务完成 4服务结束(已取消) ',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 324 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '派发的服务单' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_staff_floor
-- ----------------------------
DROP TABLE IF EXISTS `service_staff_floor`;
CREATE TABLE `service_staff_floor`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staffid` int(11) NOT NULL COMMENT '员工id （staff表id）',
  `buildno` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '楼号',
  `floor` int(3) NOT NULL COMMENT '楼层',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 70 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '排班人员负责的楼层' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_staff_shift
-- ----------------------------
DROP TABLE IF EXISTS `service_staff_shift`;
CREATE TABLE `service_staff_shift`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staffid` int(11) NOT NULL COMMENT '员工id（staff表id）',
  `shiftid` int(11) NOT NULL COMMENT '班次id (service_shift表id)',
  `hotelid` int(11) NOT NULL COMMENT '酒店id',
  `shift_date` datetime(0) NOT NULL COMMENT '日期',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 269 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客房服务-员工排班关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_suggest
-- ----------------------------
DROP TABLE IF EXISTS `service_suggest`;
CREATE TABLE `service_suggest`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL COMMENT '用户id',
  `hotelcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '酒店编码',
  `roomno` int(11) NULL DEFAULT NULL COMMENT '房号',
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '投书建议内容',
  `phone` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `feedback` tinyint(1) NULL DEFAULT 0 COMMENT '是否需要反馈 0否 1是',
  `reply_status` tinyint(1) NULL DEFAULT 0 COMMENT '回复状态 0待回复 1已回复',
  `guest_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客人姓名',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '投诉与建议' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_suggest_reply
-- ----------------------------
DROP TABLE IF EXISTS `service_suggest_reply`;
CREATE TABLE `service_suggest_reply`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `suggestid` int(11) NOT NULL COMMENT '投诉与建议id',
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回复内容',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '投诉与建议回复' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_template
-- ----------------------------
DROP TABLE IF EXISTS `service_template`;
CREATE TABLE `service_template`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoryid` int(11) NULL DEFAULT NULL COMMENT '服务类别id',
  `title` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务标题',
  `describ` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务说明',
  `hotelid` int(11) NOT NULL COMMENT '酒店id',
  `hotelcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '酒店编码',
  `hotelname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '酒店名称',
  `processid` int(11) NULL DEFAULT NULL COMMENT '服务流程id',
  `hours` int(2) NULL DEFAULT NULL COMMENT '提前预定小时数',
  `replytemplateid` int(11) NULL DEFAULT NULL COMMENT '回复模板id',
  `status` int(1) NULL DEFAULT 2 COMMENT '状态 1启用 2不启用',
  `hide` int(1) NULL DEFAULT 1 COMMENT '隐藏 1显示 2隐藏',
  `wifi_loginurl` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'wifi登录地址',
  `wifi_count` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'wifi登录账号',
  `wifi_pwd` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'wifi登录密码',
  `shop_loginurl` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商城地址',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `clean_time` int(2) NULL DEFAULT NULL COMMENT '清扫时间 1每小时 2间隔2小时 3间隔3小时 4间隔4小时 5其他',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `time_slot` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '时间段',
  `is_provide` int(1) NULL DEFAULT NULL COMMENT '是否可提供服务 1 是  0 否',
  `is_must` int(1) NOT NULL DEFAULT 0 COMMENT '是否必须  1 是  0 否',
  `is_hide` int(1) NULL DEFAULT NULL COMMENT '是否隐藏服务  1 是  0 否',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '服务模板' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_template_item
-- ----------------------------
DROP TABLE IF EXISTS `service_template_item`;
CREATE TABLE `service_template_item`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateid` int(11) NOT NULL COMMENT '模板id',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务项名称',
  `desc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '服务项描述',
  `replytext_type` tinyint(1) NULL DEFAULT NULL COMMENT '1.时间单选控件 2.日期控件 3文本控件 4日期时间控件 5.单选框（是/否）6无',
  `processid` int(11) NOT NULL COMMENT '流程配置id',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 90 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '酒店服务模板服务项' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for service_voice_message
-- ----------------------------
DROP TABLE IF EXISTS `service_voice_message`;
CREATE TABLE `service_voice_message`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_orderid` int(11) NULL DEFAULT NULL COMMENT '服务订单id',
  `phone` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `request_data` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '短信请求内容',
  `response_data` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '短信相应内容',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 72 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '语音短信' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for zone
-- ----------------------------
DROP TABLE IF EXISTS `zone`;
CREATE TABLE `zone`  (
  `id` int(11) NOT NULL,
  `zonename` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pinyin` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cityid` int(11) NOT NULL,
  `description` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

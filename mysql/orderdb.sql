/*
 Navicat Premium Data Transfer

 Source Server         : yuanzhoutest
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 106.14.5.30:3306
 Source Schema         : orderdb

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 26/02/2021 16:41:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cardbinding_record
-- ----------------------------
DROP TABLE IF EXISTS `cardbinding_record`;
CREATE TABLE `cardbinding_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `type` tinyint(4) NULL DEFAULT NULL COMMENT '类型 1充值 2消费',
  `groupid` int(11) NULL DEFAULT NULL COMMENT '预留',
  `hotelid` int(11) NULL DEFAULT NULL COMMENT '预留',
  `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '金额',
  `ordertype` int(11) NULL DEFAULT NULL COMMENT '关联订单类型',
  `orderid` int(11) NULL DEFAULT NULL COMMENT '关联订单id',
  `itemid` int(11) NULL DEFAULT NULL COMMENT '关联产品id,房型',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态',
  `note` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `serialnum` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '储值卡记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for deposit_log
-- ----------------------------
DROP TABLE IF EXISTS `deposit_log`;
CREATE TABLE `deposit_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `username` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `orderid` int(11) NULL DEFAULT NULL COMMENT '订单id',
  `orderstatus` tinyint(4) NULL DEFAULT NULL COMMENT '订单状态',
  `description` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内容',
  `logdate` datetime(0) NULL DEFAULT NULL COMMENT '记录日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 88 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '余额充值日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for deposit_pay
-- ----------------------------
DROP TABLE IF EXISTS `deposit_pay`;
CREATE TABLE `deposit_pay`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `depositid` int(11) NULL DEFAULT NULL,
  `amount` decimal(10, 2) NULL DEFAULT NULL,
  `paymode` tinyint(1) NULL DEFAULT NULL,
  `payno` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `paystatus` tinyint(4) NULL DEFAULT NULL,
  `remark` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `paytime` datetime(0) NULL DEFAULT NULL,
  `refundtime` datetime(0) NULL DEFAULT NULL,
  `refundno` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `prepayid` varbinary(200) NULL DEFAULT NULL,
  `serialnum` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 65 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '余额充值支付记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for deposit_paycode
-- ----------------------------
DROP TABLE IF EXISTS `deposit_paycode`;
CREATE TABLE `deposit_paycode`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NULL DEFAULT NULL,
  `code` int(11) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 157 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '条形码支付' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for deposit_record
-- ----------------------------
DROP TABLE IF EXISTS `deposit_record`;
CREATE TABLE `deposit_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '充值类型1余额充值2商品购买3酒店预订,4扫码支付 5扫码退款',
  `groupid` int(11) NULL DEFAULT NULL COMMENT '集团编号',
  `hotelid` int(11) NULL DEFAULT NULL COMMENT '酒店编号',
  `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '金额',
  `itemid` int(11) NULL DEFAULT NULL COMMENT '关联ID,如房型id',
  `ordertype` int(11) NOT NULL COMMENT '订单类型1房型 2积分 3 余额',
  `orderid` int(11) NULL DEFAULT NULL COMMENT '订单id',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态1有限0无效',
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `reverse` tinyint(4) NULL DEFAULT NULL COMMENT '0正常1冲销2撤回冲销',
  `paytype` int(11) NULL DEFAULT NULL COMMENT '支付方式',
  `couponid` int(11) NULL DEFAULT 0 COMMENT '赠送优惠券id',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 199 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '余额记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for goldcard_record
-- ----------------------------
DROP TABLE IF EXISTS `goldcard_record`;
CREATE TABLE `goldcard_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `saleid` int(11) NULL DEFAULT NULL COMMENT '销售员id',
  `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '金额',
  `userid` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态',
  `paytype` int(11) NULL DEFAULT NULL COMMENT '支付类型',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '金卡购买' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ord_cancelrule
-- ----------------------------
DROP TABLE IF EXISTS `ord_cancelrule`;
CREATE TABLE `ord_cancelrule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL COMMENT '订单id',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '取消规则名称',
  `code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编码',
  `type` tinyint(4) NOT NULL COMMENT '类型 1限时取消 2免费取消 3不可取消 ',
  `descrip` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '取消规则说明',
  `days` int(4) NULL DEFAULT NULL COMMENT '入住时间提前天数',
  `time` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '允许取消时间点',
  `template_content` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '取消条件内容模板',
  `finetype` tinyint(1) NULL DEFAULT NULL COMMENT '罚金类型 1百分比 2固定金额 3间夜',
  `money` decimal(10, 2) NULL DEFAULT NULL COMMENT '金额/百分比',
  `nights` int(4) NULL DEFAULT NULL COMMENT '间夜数',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 116 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单取消规则' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ord_coupon
-- ----------------------------
DROP TABLE IF EXISTS `ord_coupon`;
CREATE TABLE `ord_coupon`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL COMMENT '订单id',
  `usrcouponid` int(11) NOT NULL COMMENT '会员优惠券(usr_coupon)id',
  `coupontype` int(11) NOT NULL COMMENT '类型1满减 2折扣 3免房 4升房 5餐饮',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '金额',
  `couponname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '优惠券名称',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 78 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ord_customer
-- ----------------------------
DROP TABLE IF EXISTS `ord_customer`;
CREATE TABLE `ord_customer`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL,
  `customername` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `idtype` tinyint(4) NULL DEFAULT NULL,
  `idno` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 444 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ord_hotelinfo
-- ----------------------------
DROP TABLE IF EXISTS `ord_hotelinfo`;
CREATE TABLE `ord_hotelinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL,
  `hotelname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `roomname` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `notes` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 411 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ord_log
-- ----------------------------
DROP TABLE IF EXISTS `ord_log`;
CREATE TABLE `ord_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NULL DEFAULT NULL,
  `username` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `orderid` int(11) NULL DEFAULT NULL,
  `orderstatus` int(11) NULL DEFAULT NULL,
  `description` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `logdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 445976 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ord_notify
-- ----------------------------
DROP TABLE IF EXISTS `ord_notify`;
CREATE TABLE `ord_notify`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL,
  `notifytype` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 123 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ord_pay
-- ----------------------------
DROP TABLE IF EXISTS `ord_pay`;
CREATE TABLE `ord_pay`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL,
  `amount` decimal(10, 2) NOT NULL DEFAULT 0.00,
  `payno` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `paymode` int(11) NULL DEFAULT NULL,
  `paystatus` int(11) NULL DEFAULT NULL,
  `remark` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `prepayid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `paytime` datetime(0) NULL DEFAULT NULL,
  `refundno` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `refundtime` datetime(0) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 289 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ord_roomprice
-- ----------------------------
DROP TABLE IF EXISTS `ord_roomprice`;
CREATE TABLE `ord_roomprice`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL,
  `roompriceid` int(11) NULL DEFAULT NULL,
  `roomdate` datetime(0) NULL DEFAULT NULL,
  `price` decimal(10, 2) NULL DEFAULT NULL,
  `roomcount` int(11) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 508 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ord_score
-- ----------------------------
DROP TABLE IF EXISTS `ord_score`;
CREATE TABLE `ord_score`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL COMMENT '订单id',
  `items` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '项目',
  `used_score` int(11) NOT NULL COMMENT '使用的积分',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单积分' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderno` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userid` int(11) NOT NULL,
  `cardno` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `membertype` int(11) NULL DEFAULT NULL,
  `channel` int(11) NULL DEFAULT NULL,
  `hotelcode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `roomcode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hotelid` int(11) NOT NULL,
  `roomid` int(11) NOT NULL,
  `roomrate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pms_roomrate_val` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'pms房价码（pms_roomrate）',
  `orderamount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '订单金额',
  `offeramount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `discountamount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '折扣金额',
  `amount` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '支付金额',
  `checkindate` datetime(0) NULL DEFAULT NULL,
  `checkoutdate` datetime(0) NULL DEFAULT NULL,
  `roomcount` int(11) NULL DEFAULT NULL,
  `child` int(11) NULL DEFAULT NULL,
  `addbed` int(11) NULL DEFAULT NULL,
  `contactname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `paymode` int(11) NULL DEFAULT NULL,
  `note` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `outorder` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `arrivaltime` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bookingtype` int(11) NULL DEFAULT NULL,
  `activityid` int(11) NULL DEFAULT NULL COMMENT '营销活动id',
  `activitytype` tinyint(1) NULL DEFAULT NULL COMMENT '活动类型 1 房价立减活动 2 新人特惠活动  3 连住早订活动',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `remark` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10320 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for payment
-- ----------------------------
DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `refid` int(11) NULL DEFAULT NULL COMMENT '支付关联id',
  `reftype` int(11) NULL DEFAULT NULL COMMENT '支付产品类型1金卡销售',
  `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '金额',
  `paymode` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `payno` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `paystatus` tinyint(4) NULL DEFAULT NULL,
  `remark` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `paytime` datetime(0) NULL DEFAULT NULL,
  `refundtime` datetime(0) NULL DEFAULT NULL,
  `refundno` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `prepayid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '支付记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for payment_log
-- ----------------------------
DROP TABLE IF EXISTS `payment_log`;
CREATE TABLE `payment_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `username` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `refid` int(11) NULL DEFAULT NULL,
  `reftype` int(11) NULL DEFAULT NULL,
  `refstatus` tinyint(4) NULL DEFAULT NULL,
  `description` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `logdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 77 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '支付日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for payment_order
-- ----------------------------
DROP TABLE IF EXISTS `payment_order`;
CREATE TABLE `payment_order`  (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '订单号',
  `serial_num` bigint(18) NULL DEFAULT NULL COMMENT '流水号',
  `guest_id` int(11) NULL DEFAULT NULL COMMENT '用户ID',
  `booking_name` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户昵称',
  `mobile` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机',
  `book_date` date NULL DEFAULT NULL COMMENT '下单日期',
  `hotel_id` int(11) NULL DEFAULT NULL COMMENT '酒店ID',
  `pay_back_status` tinyint(4) NULL DEFAULT 1 COMMENT '退款状态 0 代表正常 1 发起退款 2 退款中 3 退款失败 4 退款成功',
  `status` tinyint(4) NULL DEFAULT 0 COMMENT '支付状态，0未支付，1已支付，2已关闭',
  `create_date` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `amount` decimal(10, 2) UNSIGNED NULL DEFAULT 0.00 COMMENT '实际支付金额',
  `origin_data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'json格式的原始数据',
  `payment_id` tinyint(4) NULL DEFAULT 1 COMMENT '1:到店支付 2：微信 3：支付宝',
  `trading_status` tinyint(4) NULL DEFAULT 0 COMMENT '交易状态0:未支付 1：已支付 2:已发货3：已完成',
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `group_id` int(11) NOT NULL DEFAULT 1 COMMENT '集团编号',
  `pay_time` datetime(0) NULL DEFAULT NULL COMMENT '支付时间',
  `code` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 107 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for report_order
-- ----------------------------
DROP TABLE IF EXISTS `report_order`;
CREATE TABLE `report_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reportday` datetime(0) NOT NULL COMMENT '日期',
  `salecount` int(11) NOT NULL DEFAULT 0 COMMENT '间夜数',
  `amount` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '销售金额',
  `ordercount` int(11) NOT NULL DEFAULT 0 COMMENT '订单数量',
  `hotelid` int(11) NOT NULL COMMENT '酒店id',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单报表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for report_order_create
-- ----------------------------
DROP TABLE IF EXISTS `report_order_create`;
CREATE TABLE `report_order_create`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reportday` datetime(0) NOT NULL COMMENT '日期',
  `salecount` int(11) NOT NULL DEFAULT 0 COMMENT '间夜数',
  `amount` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '销售金额',
  `ordercount` int(11) NOT NULL DEFAULT 0 COMMENT '订单数量',
  `hotelid` int(11) NOT NULL COMMENT '酒店id',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 53 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单报表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for score_record
-- ----------------------------
DROP TABLE IF EXISTS `score_record`;
CREATE TABLE `score_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NOT NULL COMMENT '类型 1 酒店预定  2  商品兑换 3扫码送积分 9 签到送积分',
  `refid` int(11) NULL DEFAULT NULL COMMENT '兑换的产品id',
  `userid` int(11) NULL DEFAULT NULL,
  `groupid` int(11) NULL DEFAULT NULL,
  `hotelid` int(11) NULL DEFAULT NULL,
  `score` double NULL DEFAULT NULL COMMENT '积分',
  `orderid` int(11) NULL DEFAULT NULL COMMENT '关联订单id',
  `status` int(11) NULL DEFAULT NULL COMMENT '记录状态1有限0无效',
  `serialnum` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 154 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '积分兑换记录' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

/*
 Navicat Premium Data Transfer

 Source Server         : yuanzhoutest
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 106.14.5.30:3306
 Source Schema         : shoppingdb

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 26/02/2021 16:44:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for banner_config
-- ----------------------------
DROP TABLE IF EXISTS `banner_config`;
CREATE TABLE `banner_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `linkurl` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '链接地址',
  `imageurl` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片地址',
  `sort` int(11) NOT NULL DEFAULT 1 COMMENT '排序',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0 COMMENT '0正常1删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '轮播配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoryname` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类目名称',
  `parentid` int(11) NOT NULL DEFAULT 0 COMMENT '父类目id',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '是否启用0禁用 1启用',
  `level` int(11) NOT NULL DEFAULT 1 COMMENT '深度',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '品类' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city`  (
  `id` int(11) NOT NULL,
  `cityname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pinyin` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `provinceid` int(11) NOT NULL DEFAULT 0,
  `countryid` int(11) NOT NULL DEFAULT 0,
  `hotelcount` int(11) NOT NULL DEFAULT 0,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for city_copy1
-- ----------------------------
DROP TABLE IF EXISTS `city_copy1`;
CREATE TABLE `city_copy1`  (
  `id` int(11) NOT NULL,
  `cityname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pinyin` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `provinceid` int(11) NOT NULL DEFAULT 0,
  `countryid` int(11) NOT NULL DEFAULT 0,
  `hotelcount` int(11) NOT NULL DEFAULT 0,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for consume_active
-- ----------------------------
DROP TABLE IF EXISTS `consume_active`;
CREATE TABLE `consume_active`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activename` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '活动名称',
  `startdate` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `enddate` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `activedesc` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '活动介绍',
  `picurl` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '活动图片',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态 1启用 0禁用',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '消费券活动' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for consume_coupon
-- ----------------------------
DROP TABLE IF EXISTS `consume_coupon`;
CREATE TABLE `consume_coupon`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activeid` int(11) NULL DEFAULT NULL COMMENT '活动id',
  `codeno` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '券码',
  `hotelid` int(11) NULL DEFAULT NULL COMMENT '酒店id',
  `salename` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '销售渠道名称',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态 1可用 2已使用',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7336 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '消费券' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for consume_order
-- ----------------------------
DROP TABLE IF EXISTS `consume_order`;
CREATE TABLE `consume_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NULL DEFAULT NULL,
  `activeid` int(11) NULL DEFAULT NULL,
  `couponid` int(11) NULL DEFAULT NULL,
  `codeno` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '券号',
  `truename` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `provinceid` int(11) NULL DEFAULT NULL COMMENT '行政区',
  `cityid` int(11) NULL DEFAULT NULL COMMENT '城市',
  `districtid` int(11) NULL DEFAULT NULL COMMENT '行政区',
  `address` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮寄地址',
  `phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `status` int(11) NULL DEFAULT NULL COMMENT '1新订单 2已邮寄',
  `expressno` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '快递号',
  `express` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '快递公司',
  `create_date` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '消费订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for district
-- ----------------------------
DROP TABLE IF EXISTS `district`;
CREATE TABLE `district`  (
  `id` int(11) NOT NULL,
  `districtname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pinyin` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cityid` int(11) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for district_copy1
-- ----------------------------
DROP TABLE IF EXISTS `district_copy1`;
CREATE TABLE `district_copy1`  (
  `id` int(11) NOT NULL,
  `districtname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pinyin` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cityid` int(11) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods`  (
  `id` int(11) NOT NULL,
  `goods_type` int(1) NOT NULL DEFAULT 1 COMMENT '商品分类 1 实物商品  2 票卷   3 红包',
  `specstype` tinyint(1) NOT NULL COMMENT '规格类型 1默认 2其他',
  `categoryid` int(11) NOT NULL COMMENT '目录',
  `goodsname` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品名称',
  `sellingpoint` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品卖点',
  `goodsno` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品编码',
  `costprice` decimal(10, 2) NULL DEFAULT NULL COMMENT '成本价',
  `goodsdesc` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '商品说明,富文本',
  `rules` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品规则',
  `tips` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '温馨提示',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `pointtype` int(1) NULL DEFAULT 1 COMMENT '积分抵扣类型 1不抵扣 2部分 3全额',
  `pointvalue` double(11, 2) NULL DEFAULT 0.00 COMMENT '抵扣百分比',
  `showsalescount` int(11) NOT NULL DEFAULT 1 COMMENT '是否显示销量 1显示 0不显示',
  `showpoint` int(11) NULL DEFAULT NULL COMMENT '是否显示积分售价',
  `showstock` int(11) NOT NULL DEFAULT 1 COMMENT '是否显示库存 1显示 0不显示',
  `savestatus` int(11) NULL DEFAULT NULL COMMENT '上架状态 0临时 1上架 2下架',
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '上架方式 1立即 2定时',
  `status_time` datetime(0) NULL DEFAULT NULL COMMENT '定时上线时间',
  `usecoupon` int(11) NULL DEFAULT NULL COMMENT '1可以使用优惠券 0不可以',
  `sales_aways` int(11) NULL DEFAULT NULL COMMENT '是否长期销售 1是 0否',
  `sales_starttime` datetime(0) NULL DEFAULT NULL COMMENT '开始销售时间',
  `sales_endtime` datetime(0) NULL DEFAULT NULL COMMENT '结束销售时间',
  `startcount` int(11) NOT NULL COMMENT '起售份数',
  `limitsaletype` int(11) NULL DEFAULT NULL COMMENT '每人1 每人每天2',
  `limitsalevalue` int(11) NULL DEFAULT NULL COMMENT '限购数量',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for goods_group
-- ----------------------------
DROP TABLE IF EXISTS `goods_group`;
CREATE TABLE `goods_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goodsid` int(11) NOT NULL,
  `groupid` int(11) NOT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51187 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品分组' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for goods_id
-- ----------------------------
DROP TABLE IF EXISTS `goods_id`;
CREATE TABLE `goods_id`  (
  `goodsid` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`goodsid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2083 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '产品自增号' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for goods_label
-- ----------------------------
DROP TABLE IF EXISTS `goods_label`;
CREATE TABLE `goods_label`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goodsid` int(11) NOT NULL,
  `labelname` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标签名称',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52294 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品标签' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for goods_logistics
-- ----------------------------
DROP TABLE IF EXISTS `goods_logistics`;
CREATE TABLE `goods_logistics`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goodsid` int(11) NOT NULL COMMENT '产品id',
  `logistics_type` int(11) NULL DEFAULT NULL COMMENT '物流类型1快递 2自提',
  `postage_type` int(11) NULL DEFAULT 1 COMMENT '是否包邮 1包邮 2统一邮费 3运费',
  `postage_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '统一邮费金额',
  `logisticsid` int(11) NULL DEFAULT NULL COMMENT '邮费模板',
  `hotelid` int(11) NULL DEFAULT NULL COMMENT '自提商户id',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 470 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '产品物流' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for goods_merchant
-- ----------------------------
DROP TABLE IF EXISTS `goods_merchant`;
CREATE TABLE `goods_merchant`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goodsid` int(11) NOT NULL COMMENT '产品ID',
  `merchantid` int(11) NOT NULL COMMENT '商户id',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 551 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '适合商户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for goods_resource
-- ----------------------------
DROP TABLE IF EXISTS `goods_resource`;
CREATE TABLE `goods_resource`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goodsid` int(11) NOT NULL COMMENT '商品id',
  `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '资源地址',
  `resourcetype` int(11) NOT NULL COMMENT '资源类型 1图片  2 商品',
  `type` int(11) NOT NULL COMMENT '关联类型 1首图 2 商品 3 商品分享',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 655 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商城资源表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for goods_share
-- ----------------------------
DROP TABLE IF EXISTS `goods_share`;
CREATE TABLE `goods_share`  (
  `goodsid` int(11) NOT NULL,
  `title` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分享标题',
  `sharedesc` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分享描述',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`goodsid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '产品分享设置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for goods_specs
-- ----------------------------
DROP TABLE IF EXISTS `goods_specs`;
CREATE TABLE `goods_specs`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goodsid` int(11) NOT NULL,
  `specsname` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规格名称',
  `codeno` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品编码',
  `originalprice` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '原价',
  `price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '现价',
  `stock` int(11) NOT NULL DEFAULT 0 COMMENT '库存',
  `salecount` int(11) NULL DEFAULT 0 COMMENT '已售份数',
  `weight` float NULL DEFAULT NULL COMMENT '重量',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 166 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品规格表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for goods_ticket
-- ----------------------------
DROP TABLE IF EXISTS `goods_ticket`;
CREATE TABLE `goods_ticket`  (
  `goodsid` int(11) NOT NULL COMMENT '商品id',
  `use_range` tinyint(1) NULL DEFAULT NULL COMMENT '适用范围 1单商户 2多商户',
  `split` tinyint(1) NOT NULL COMMENT '是否拆分 1拆分 2不拆分',
  `split_count` int(2) NULL DEFAULT NULL COMMENT '拆分份数',
  `valid_type` tinyint(1) NOT NULL COMMENT '有效期类型 1 长期有效 2 固定日期 3 天数',
  `valid_startdate` datetime(0) NULL DEFAULT NULL COMMENT '有效期开始日期',
  `valid_enddate` datetime(0) NULL DEFAULT NULL COMMENT '有效期结束日期',
  `valid_days` int(3) NULL DEFAULT NULL COMMENT '有效期天数',
  `available_date` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '可用日期描述',
  `use_time` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '使用时段描述',
  `reservation` tinyint(1) NOT NULL COMMENT '1 不预约 2 需预约',
  `reservation_desc` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预约说明',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`goodsid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '票券商品' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL COMMENT '是否显示 1显示 0不显示',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '组别' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for logistics
-- ----------------------------
DROP TABLE IF EXISTS `logistics`;
CREATE TABLE `logistics`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logisticsname` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模板名称',
  `express` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '快递公司',
  `costtype` int(11) NOT NULL DEFAULT 1 COMMENT '计价方式1按件数 2重量 ',
  `arealimit` int(11) NOT NULL DEFAULT 0 COMMENT '区域限制 0不支持 1支持',
  `num_count` int(11) NULL DEFAULT NULL COMMENT '首件件数',
  `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '首件金额',
  `num_count2` int(11) NULL DEFAULT NULL COMMENT '次件件数',
  `num_amount2` decimal(10, 2) NULL DEFAULT NULL COMMENT '次件金额',
  `weight_count` float NULL DEFAULT NULL COMMENT '首重重量',
  `weight_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '首重金额',
  `weight_count2` float NULL DEFAULT NULL COMMENT '续重重量',
  `weight_amount2` decimal(10, 2) NULL DEFAULT NULL COMMENT '续重金额',
  `logisticestatus` int(11) NULL DEFAULT NULL COMMENT '状态1 启用 0禁用',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '物流模板' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for logistics_province
-- ----------------------------
DROP TABLE IF EXISTS `logistics_province`;
CREATE TABLE `logistics_province`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logisticsid` int(11) NULL DEFAULT NULL,
  `provinceid` int(11) NULL DEFAULT NULL COMMENT '省份id',
  `num_count` int(11) NULL DEFAULT NULL COMMENT '首件件数',
  `num_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '首件金额',
  `num_count2` int(11) NULL DEFAULT NULL COMMENT '次件件数',
  `num_amount2` decimal(10, 2) NULL DEFAULT NULL COMMENT '次件金额',
  `weight_count` float NULL DEFAULT NULL COMMENT '首重重量',
  `weight_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '首重金额',
  `weight_count2` float NULL DEFAULT NULL COMMENT '续重重量',
  `weight_amount2` decimal(10, 2) NULL DEFAULT NULL COMMENT '续重金额',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '快递省份关联' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for merchant
-- ----------------------------
DROP TABLE IF EXISTS `merchant`;
CREATE TABLE `merchant`  (
  `id` int(11) NOT NULL,
  `merchantname` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for navigation_config
-- ----------------------------
DROP TABLE IF EXISTS `navigation_config`;
CREATE TABLE `navigation_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `position` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '位置INDEX',
  `logourl` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
  `linkurl` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '链接',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '导航配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for province
-- ----------------------------
DROP TABLE IF EXISTS `province`;
CREATE TABLE `province`  (
  `id` int(11) NOT NULL,
  `provincename` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `countryid` int(11) NOT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for province_copy1
-- ----------------------------
DROP TABLE IF EXISTS `province_copy1`;
CREATE TABLE `province_copy1`  (
  `id` int(11) NOT NULL,
  `provincename` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `countryid` int(11) NOT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for region
-- ----------------------------
DROP TABLE IF EXISTS `region`;
CREATE TABLE `region`  (
  `id` int(11) NOT NULL COMMENT '区域主键',
  `name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区域名称',
  `pid` int(11) NULL DEFAULT NULL COMMENT '区域上级标识',
  `sname` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地名简称',
  `level` int(11) NULL DEFAULT NULL COMMENT '区域等级',
  `citycode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区域编码',
  `yzcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮政编码',
  `mername` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组合名称',
  `Lng` float NULL DEFAULT NULL,
  `Lat` float NULL DEFAULT NULL,
  `pinyin` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for shop_order
-- ----------------------------
DROP TABLE IF EXISTS `shop_order`;
CREATE TABLE `shop_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL COMMENT '用户id',
  `goodsid` int(11) NOT NULL COMMENT '产品id',
  `specid` int(11) NULL DEFAULT NULL COMMENT '产品型号id',
  `orderno` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单号',
  `orderstatus` int(11) NOT NULL COMMENT '订单状态  1 待支付  4 支付完成  12 退款中   20 订单关闭',
  `orderamount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '订单金额',
  `count` int(11) NULL DEFAULT NULL COMMENT '数量',
  `point_amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '积分抵扣金额',
  `pointstatus` int(11) NULL DEFAULT NULL COMMENT '积分状态 0未扣 1扣 2退',
  `amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '支付金额',
  `express_amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '邮费金额',
  `coupon_amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '优惠券金额',
  `discount_amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `remark` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `expressstatus` int(11) NULL DEFAULT NULL COMMENT '发货状态 0未发货 1发货 4已经收货',
  `express_date` datetime(0) NULL DEFAULT NULL COMMENT '发货时间',
  `express_type` int(11) NULL DEFAULT NULL COMMENT '快递类型  1快递 2自提',
  `express_company` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '快递公司',
  `express_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '快递号',
  `truename` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `provinceid` int(11) NULL DEFAULT NULL COMMENT '省份',
  `cityid` int(11) NULL DEFAULT NULL COMMENT '城市',
  `districtid` int(11) NULL DEFAULT NULL COMMENT '行政区',
  `address` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `refunddate` datetime(0) NULL DEFAULT NULL COMMENT '取消时间',
  `refundtype` int(11) NULL DEFAULT NULL COMMENT '取消类型1系统 2人工',
  `refundremark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '取消备注',
  `refunduserid` int(11) NULL DEFAULT NULL COMMENT '操作取消人id',
  `refundname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作取消人姓名',
  `merchantid` int(11) NULL DEFAULT NULL COMMENT '商户id',
  `merchantname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商户名称',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `is_send` int(11) NULL DEFAULT NULL COMMENT '是否推送提示到公众号  1推送  不为1 未推送',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25055 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for shop_order_coupon
-- ----------------------------
DROP TABLE IF EXISTS `shop_order_coupon`;
CREATE TABLE `shop_order_coupon`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL,
  `couponid` int(11) NULL DEFAULT NULL,
  `usrcouponid` int(11) NULL DEFAULT NULL COMMENT '会员优惠券(usr_coupon)id',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '优惠金额',
  `couponno` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '券号',
  `coupontype` tinyint(1) NULL DEFAULT NULL COMMENT '优惠券类型1满减 2折扣 3免房 4升房 5餐饮',
  `couponname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '优惠券名称',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '订单优惠券' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for shop_order_log
-- ----------------------------
DROP TABLE IF EXISTS `shop_order_log`;
CREATE TABLE `shop_order_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `username` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名称',
  `orderid` int(11) NULL DEFAULT NULL COMMENT '订单id',
  `orderstatus` int(11) NULL DEFAULT NULL COMMENT '订单状态',
  `description` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '内容',
  `logdate` datetime(0) NULL DEFAULT NULL COMMENT '日志日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 518 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '订单日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for shop_order_payment
-- ----------------------------
DROP TABLE IF EXISTS `shop_order_payment`;
CREATE TABLE `shop_order_payment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL COMMENT '订单id',
  `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '支付金额',
  `paymode` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '支付方式  1 微信支付  2 积分支付 3   余额支付 4   储值卡支付',
  `payno` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '流水号',
  `paystatus` int(11) NULL DEFAULT NULL COMMENT '支付状态  1 待支付    4   支付成功   10 退款中   11 退款失败   12   退款成功 ',
  `remark` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `paytime` datetime(0) NULL DEFAULT NULL COMMENT '支付时间',
  `refundtime` datetime(0) NULL DEFAULT NULL COMMENT '退款时间',
  `refundno` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '退款流水',
  `prepayid` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微信订单号',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 92 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '订单支付' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for shop_order_rep
-- ----------------------------
DROP TABLE IF EXISTS `shop_order_rep`;
CREATE TABLE `shop_order_rep`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL,
  `goodsid` int(11) NOT NULL,
  `specid` int(11) NULL DEFAULT NULL,
  `goodsname` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '产品名称',
  `specname` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规格名称',
  `resourceid` int(11) NULL DEFAULT NULL COMMENT '资源图片id',
  `weight` float NULL DEFAULT NULL COMMENT '重量',
  `goodsno` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品编码',
  `codeno` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品规格编码',
  `rules` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品规则',
  `others` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '其余信息',
  `goods_type` int(11) NULL DEFAULT NULL COMMENT '商品分类 1 实物商品  2 票卷   3 红包',
  `pointtype` int(1) NULL DEFAULT NULL COMMENT '积分抵扣类型 1不抵扣 2部分 3全额',
  `pointvalue` double(11, 2) NULL DEFAULT NULL COMMENT '抵扣百分比',
  `originalprice` decimal(10, 2) NULL DEFAULT NULL COMMENT '原价',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '现价',
  `split` tinyint(4) NULL DEFAULT NULL COMMENT '1拆分 2不拆分',
  `split_count` int(11) NULL DEFAULT NULL COMMENT '拆分份数',
  `valid_type` tinyint(4) NULL DEFAULT NULL COMMENT '有效期类型1长期 2固定 3天数',
  `valid_startdate` datetime(0) NULL DEFAULT NULL COMMENT '有效期开始时间',
  `valid_enddate` datetime(0) NULL DEFAULT NULL COMMENT '有效期结束时间',
  `valid_days` int(11) NULL DEFAULT NULL COMMENT '有效期天数',
  `available_date` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '可用日期描述',
  `use_time` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '使用时段描述',
  `reservation` tinyint(4) NULL DEFAULT NULL COMMENT '1无需预约 2需预约',
  `reservation_desc` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预约说明',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 156 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '订单快照' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for shop_order_ticket
-- ----------------------------
DROP TABLE IF EXISTS `shop_order_ticket`;
CREATE TABLE `shop_order_ticket`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL,
  `ticketno` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL COMMENT '状态 1 未使用 2已使用 3作废',
  `startdate` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `enddate` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `merchantid` int(11) NULL DEFAULT NULL COMMENT '商户ID',
  `usedate` datetime(0) NULL DEFAULT NULL COMMENT '使用时间',
  `usemerchantid` int(11) NULL DEFAULT NULL COMMENT '使用商户id',
  `useuserid` int(11) NULL DEFAULT NULL COMMENT '使用人id',
  `usename` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '使用人姓名',
  `usetype` int(255) NULL DEFAULT NULL COMMENT '使用类型1 正常使用 2过期使用',
  `useremark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '使用备注',
  `refunddate` datetime(0) NULL DEFAULT NULL COMMENT '退款时间',
  `refundremark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '退款备注',
  `refundtype` int(11) NULL DEFAULT NULL COMMENT '作废类型 1正常 2已使用作废',
  `refunduserid` int(11) NULL DEFAULT NULL COMMENT '作废人id',
  `refundname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '作废人姓名',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 58 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '订单券号' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for shopping_config
-- ----------------------------
DROP TABLE IF EXISTS `shopping_config`;
CREATE TABLE `shopping_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shownotice` int(11) NOT NULL DEFAULT 0 COMMENT '是否显示通知 1显示 0不显示',
  `notice` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '通知内容',
  `bookingdesc` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预订须知',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商城配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user_address
-- ----------------------------
DROP TABLE IF EXISTS `user_address`;
CREATE TABLE `user_address`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `provinceid` int(11) NULL DEFAULT NULL COMMENT '省份id',
  `provincename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '省份名称',
  `cityid` int(11) NULL DEFAULT NULL COMMENT '城市id',
  `cityname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '城市名称',
  `districtid` int(11) NULL DEFAULT NULL COMMENT '区id',
  `districtname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '区名称',
  `address` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机',
  `truename` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `setdefault` int(11) NOT NULL COMMENT '是否默认1默认 0非默认',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '收货地址' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

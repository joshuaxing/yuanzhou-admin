/*
 Navicat Premium Data Transfer

 Source Server         : yuanzhoutest
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 106.14.5.30:3306
 Source Schema         : memberdb

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 26/02/2021 16:39:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for benefits
-- ----------------------------
DROP TABLE IF EXISTS `benefits`;
CREATE TABLE `benefits`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catagory` tinyint(4) NOT NULL COMMENT '特权类别 1基本权益 2生日权益',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '特权名称',
  `code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权益码',
  `url` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标url',
  `descrip` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员权益表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for card
-- ----------------------------
DROP TABLE IF EXISTS `card`;
CREATE TABLE `card`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `level` int(4) NOT NULL COMMENT '会员等级',
  `cardtype_id` int(11) NOT NULL COMMENT '会员卡样式id',
  `vaildtype` int(2) NOT NULL COMMENT ' 0 永久有效 1 1年有效  2  2年有效 以此类推  ',
  `valid_days` int(4) NULL DEFAULT NULL COMMENT '有效期天数',
  `descrip` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '说明',
  `proportion` decimal(10, 2) NOT NULL COMMENT '消费获取积分比例',
  `status` tinyint(1) UNSIGNED ZEROFILL NOT NULL DEFAULT 0 COMMENT '1 启用 0 禁用',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员卡等级' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for card_channel
-- ----------------------------
DROP TABLE IF EXISTS `card_channel`;
CREATE TABLE `card_channel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cardid` int(11) NOT NULL COMMENT '会员卡id',
  `channelid` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '渠道id',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员卡-渠道关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for card_style
-- ----------------------------
DROP TABLE IF EXISTS `card_style`;
CREATE TABLE `card_style`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` tinyint(4) NOT NULL COMMENT '类别 1悦廷会',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `descrip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `style` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '会员卡样式',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片url',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员卡样式表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL COMMENT '用户id',
  `hotelid` int(11) NOT NULL COMMENT '酒店id',
  `roomid` int(11) NULL DEFAULT NULL COMMENT '房型id',
  `orderid` int(11) NOT NULL COMMENT '订单id',
  `roomname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房型名称',
  `content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评论内容',
  `star` double(2, 1) NULL DEFAULT NULL COMMENT '综合星级',
  `score` double(2, 1) NULL DEFAULT NULL COMMENT '综合评分',
  `facility_star` double(2, 1) NULL DEFAULT NULL COMMENT '设施星级',
  `facility_score` double(2, 1) NULL DEFAULT NULL COMMENT '设施评分',
  `health_star` double(2, 1) NULL DEFAULT NULL COMMENT '卫生星级',
  `health_score` double(2, 1) NULL DEFAULT NULL COMMENT '卫生评分',
  `service_star` double(2, 1) NULL DEFAULT NULL COMMENT '服务星级',
  `service_score` double(2, 1) NULL DEFAULT NULL COMMENT '服务评分',
  `env_star` double(2, 1) NULL DEFAULT NULL COMMENT '环境星级',
  `env_score` double(2, 1) NULL DEFAULT NULL COMMENT '环境评分',
  `staus` tinyint(1) NOT NULL DEFAULT 1 COMMENT '与评论审核表状态 一致 1屏蔽  2评论展示  3删除 默认 1',
  `reply_status` tinyint(1) NULL DEFAULT 1 COMMENT '回复状态 1待回复 2已回复',
  `anonymous` tinyint(1) NULL DEFAULT 0 COMMENT '匿名 1匿名 0不匿名',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 275 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '评论表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for comment_check
-- ----------------------------
DROP TABLE IF EXISTS `comment_check`;
CREATE TABLE `comment_check`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL COMMENT '用户id',
  `commentid` int(11) NOT NULL COMMENT '评论id',
  `check_status` tinyint(1) NOT NULL COMMENT ' 状态 ：1屏蔽  2评论展示  3删除 ',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '评论审核表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for comment_picture
-- ----------------------------
DROP TABLE IF EXISTS `comment_picture`;
CREATE TABLE `comment_picture`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commentid` int(11) NOT NULL COMMENT '评论id',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片地址',
  `title` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `descrip` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '评论图片' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for comment_reply
-- ----------------------------
DROP TABLE IF EXISTS `comment_reply`;
CREATE TABLE `comment_reply`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL COMMENT '用户id',
  `commentid` int(11) NOT NULL COMMENT '评论id',
  `content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回复内容',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '评论回复' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for comment_template
-- ----------------------------
DROP TABLE IF EXISTS `comment_template`;
CREATE TABLE `comment_template`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模板编码',
  `templatename` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模板名称',
  `brandid` int(11) NOT NULL COMMENT '品牌id',
  `content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模板内容',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '评论回复模板表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `couponname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '优惠券名称',
  `subtitle` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '副标题',
  `coupontype` int(11) NULL DEFAULT NULL COMMENT '类型1满减 2折扣 3免房 4升房 5餐饮',
  `functype` int(2) NULL DEFAULT NULL COMMENT '功能类型 1金额券 2功能券',
  `quantity` int(11) NULL DEFAULT NULL COMMENT '数量',
  `conditionprice` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '金额条件',
  `peoplelimit` int(11) NULL DEFAULT NULL COMMENT '每人限制数量 每次发放数量',
  `couponprice` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '减免金额',
  `vaildtype` int(11) NOT NULL COMMENT '是否长期有效 0 否 1 是',
  `valid_startdate` datetime(0) NULL DEFAULT NULL COMMENT '有效期开始时间',
  `valid_enddate` datetime(0) NULL DEFAULT NULL COMMENT '有效期结束时间',
  `vaild_startcount` int(11) NULL DEFAULT NULL COMMENT '领取后几天内生效',
  `vaild_length` int(11) NULL DEFAULT NULL COMMENT '有效天数',
  `usedesc` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '使用须知',
  `discount` int(11) NULL DEFAULT NULL COMMENT '折扣率,类型为折扣的时候才用到',
  `discountprice` decimal(10, 2) NULL DEFAULT NULL COMMENT '封底金额',
  `source_type` int(11) NULL DEFAULT NULL COMMENT '券来源',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态  状态  0 启用  1 禁用',
  `use_type` int(11) NULL DEFAULT NULL COMMENT '使用方式   1 通用  2 会员商城  3 餐饮商城  4 客房预订',
  `groupid` int(11) NULL DEFAULT NULL,
  `hotelid` int(11) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `couponvalue` int(11) NULL DEFAULT NULL COMMENT '面值',
  `receivequantity` int(11) NULL DEFAULT NULL COMMENT '领取数量',
  `useChannel` int(11) NULL DEFAULT NULL COMMENT '使用渠道 关联渠道表主键ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 217 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for coupon_send
-- ----------------------------
DROP TABLE IF EXISTS `coupon_send`;
CREATE TABLE `coupon_send`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NULL DEFAULT NULL,
  `guest_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应userid用户的名称',
  `mobile` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `couponid` int(11) NULL DEFAULT NULL COMMENT '券id',
  `sendtype` int(11) NULL DEFAULT NULL COMMENT '0无效 1全员赠送 2单个赠送',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态0无效1申请中2通过3驳回',
  `send_status` int(11) NULL DEFAULT NULL COMMENT '0未赠送1正常2异常',
  `note` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `proposer_id` int(11) NULL DEFAULT NULL COMMENT '申请人ID',
  `approver_id` int(11) NULL DEFAULT NULL COMMENT '审批人ID',
  `create_date` datetime(0) NOT NULL,
  `update_date` datetime(0) NOT NULL,
  `is_deleted` int(11) NULL DEFAULT 0,
  `synchro` int(11) NULL DEFAULT 0 COMMENT '全员发券是否同步  0 否 1 是 （全员发券标记）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 97 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '优惠券申请表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for coupon_source
-- ----------------------------
DROP TABLE IF EXISTS `coupon_source`;
CREATE TABLE `coupon_source`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '启用 0 不启用 1 ',
  `source` int(11) NULL DEFAULT NULL COMMENT '1 CRM 2  h5  3 历史 ',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `sourcecode` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '优惠券来源编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '优惠券来源' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for couponsendhistory_birthday
-- ----------------------------
DROP TABLE IF EXISTS `couponsendhistory_birthday`;
CREATE TABLE `couponsendhistory_birthday`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `year` int(11) NOT NULL COMMENT '年份，如2019',
  `sendstatus` tinyint(4) NOT NULL COMMENT '0未发送，1已发送',
  `remark` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23425 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '优惠券会员生日发放历史' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for couponsendhistory_birthday_job
-- ----------------------------
DROP TABLE IF EXISTS `couponsendhistory_birthday_job`;
CREATE TABLE `couponsendhistory_birthday_job`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jobdate` date NULL DEFAULT NULL,
  `starttime` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `endtime` datetime(0) NULL DEFAULT NULL COMMENT '完成时间',
  `jobstatus` tinyint(4) NULL DEFAULT NULL COMMENT '0执行中 1执行完成',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14511 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '生日定时器记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for couponsendhistory_firsthotel
-- ----------------------------
DROP TABLE IF EXISTS `couponsendhistory_firsthotel`;
CREATE TABLE `couponsendhistory_firsthotel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `sendstatus` tinyint(4) NULL DEFAULT NULL COMMENT '0未发送1已发送',
  `remark` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '优惠券会员首住离店发放历史' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for hotel_dept
-- ----------------------------
DROP TABLE IF EXISTS `hotel_dept`;
CREATE TABLE `hotel_dept`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `group_id` int(11) NULL DEFAULT NULL COMMENT '所属集团',
  `creator_id` int(11) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` int(11) NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `status` tinyint(2) NULL DEFAULT 1 COMMENT '状态0禁用1启用',
  `refid` int(11) NULL DEFAULT NULL COMMENT '关联ID，生成二维码用这个字段',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for information_template
-- ----------------------------
DROP TABLE IF EXISTS `information_template`;
CREATE TABLE `information_template`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息名称',
  `type` tinyint(4) NULL DEFAULT NULL COMMENT '消息类型 1 微信消息 2 短信消息',
  `trig` int(11) NULL DEFAULT NULL COMMENT '触发场景 1 预定成功  2 用户登陆 3 订单取消  4 入住提醒',
  `triggertype` int(11) NULL DEFAULT NULL COMMENT '触发时间类型 1 立即触发  2 时间类型',
  `trigger_minute` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '触发类型为 2 时间类型 此处有值',
  `channels` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道表主键集合',
  `content` varchar(800) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板内容',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消息模板表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for manage_user
-- ----------------------------
DROP TABLE IF EXISTS `manage_user`;
CREATE TABLE `manage_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for range
-- ----------------------------
DROP TABLE IF EXISTS `range`;
CREATE TABLE `range`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rangename` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '范围名称',
  `create_date` datetime(0) NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '应用范围' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sale_activity
-- ----------------------------
DROP TABLE IF EXISTS `sale_activity`;
CREATE TABLE `sale_activity`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activitytype` tinyint(4) NULL DEFAULT NULL COMMENT '类型，每个类型只有一种1公众号推广2会员拉新 3金卡分销 4日历房分销',
  `activityname` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `costdepartment` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '成本部门',
  `commissiontype` int(11) NULL DEFAULT NULL COMMENT '佣金规则1比例 2固定金额',
  `commissionrule` decimal(10, 2) NULL DEFAULT NULL COMMENT '佣金规则',
  `onlinecash` tinyint(4) NULL DEFAULT NULL COMMENT '线上提现',
  `offlinecash` tinyint(4) NULL DEFAULT NULL COMMENT '线下提现',
  `cashdesc` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '结算说明',
  `checkset` tinyint(4) NULL DEFAULT NULL COMMENT '审核设置1需要审核 0不需要审核',
  `applyset` tinyint(4) NULL DEFAULT NULL COMMENT '1报名参加 2全员 3指定人群',
  `rewardcount` int(11) NULL DEFAULT NULL COMMENT '奖励人数',
  `selfreward` tinyint(4) NULL DEFAULT NULL COMMENT '自购奖励1获得，2获取',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分销活动' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sale_activity_hotelbooking
-- ----------------------------
DROP TABLE IF EXISTS `sale_activity_hotelbooking`;
CREATE TABLE `sale_activity_hotelbooking`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activityid` int(11) NULL DEFAULT NULL COMMENT '活动id',
  `hotelid` int(11) NULL DEFAULT NULL COMMENT '酒店id',
  `hotelname` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '酒店名称',
  `hotelcode` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '酒店编码',
  `roomid` int(11) NULL DEFAULT NULL COMMENT '房型id',
  `roomname` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '房型名称',
  `title` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '海报标题',
  `roomprice` decimal(10, 2) NULL DEFAULT NULL COMMENT '房价',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '分销活动酒店设置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sale_activity_user
-- ----------------------------
DROP TABLE IF EXISTS `sale_activity_user`;
CREATE TABLE `sale_activity_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `saleid` int(11) NULL DEFAULT NULL COMMENT '销售id',
  `department` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门',
  `activityusertype` tinyint(4) NULL DEFAULT NULL COMMENT '人员设置类型1人员2部门',
  `activityid` int(11) NULL DEFAULT NULL COMMENT '活动id',
  `status` int(11) NULL DEFAULT NULL COMMENT '审核状态1申请中 2正常 3 拒绝',
  `lockuser` tinyint(4) NULL DEFAULT 0 COMMENT '0正常 1冻结',
  `applytype` tinyint(255) NOT NULL DEFAULT 1 COMMENT '1报名参加 2指定人员',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '推广人员设置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sale_goldcard
-- ----------------------------
DROP TABLE IF EXISTS `sale_goldcard`;
CREATE TABLE `sale_goldcard`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activityid` int(11) NULL DEFAULT NULL COMMENT '活动id',
  `recordid` int(11) NULL DEFAULT NULL COMMENT '记录id',
  `goldcardid` int(11) NULL DEFAULT NULL COMMENT '金卡订单id',
  `saleid` int(11) NULL DEFAULT NULL COMMENT '销售id',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '金卡销售' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sale_hotelbooking
-- ----------------------------
DROP TABLE IF EXISTS `sale_hotelbooking`;
CREATE TABLE `sale_hotelbooking`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activityid` int(11) NULL DEFAULT NULL COMMENT '分销活动id',
  `recordid` int(11) NULL DEFAULT NULL COMMENT '收益记录id',
  `saleid` int(11) NULL DEFAULT NULL COMMENT '分销员id',
  `hotelbookingid` int(11) NULL DEFAULT NULL COMMENT '分销日历房id',
  `orderid` int(11) NULL DEFAULT NULL COMMENT '酒店订单id',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '日历房分销详细表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sale_newuser
-- ----------------------------
DROP TABLE IF EXISTS `sale_newuser`;
CREATE TABLE `sale_newuser`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activityid` int(11) NULL DEFAULT NULL COMMENT '活动id',
  `recordid` int(11) NULL DEFAULT NULL COMMENT '记录id',
  `saleid` int(11) NULL DEFAULT NULL COMMENT '销售id',
  `userid` int(11) NULL DEFAULT NULL COMMENT '注册用户id',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '新用户注册' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sale_picture
-- ----------------------------
DROP TABLE IF EXISTS `sale_picture`;
CREATE TABLE `sale_picture`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `saleid` int(11) NOT NULL COMMENT '人员id',
  `saletype` tinyint(4) NOT NULL COMMENT '分销类型 1公众号推广 3会员拉新 4金卡分销 5日历房分销',
  `imagename` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片名称',
  `path` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片路径',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 56 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分销金额记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sale_record
-- ----------------------------
DROP TABLE IF EXISTS `sale_record`;
CREATE TABLE `sale_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `saleid` int(11) NULL DEFAULT NULL COMMENT '分销id',
  `saletype` int(11) NULL DEFAULT NULL COMMENT '销售类型1公众号推广2会员拉新3金卡分销4提现 5日历房',
  `amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '金额',
  `activityid` int(11) NULL DEFAULT NULL COMMENT '活动id',
  `status` tinyint(255) NULL DEFAULT NULL COMMENT '状态1新建 2待处理 4成功 5失败',
  `accounttype` tinyint(255) NOT NULL DEFAULT 1 COMMENT '1线下 2线上',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 46 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分销金额记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sale_weixinfollow
-- ----------------------------
DROP TABLE IF EXISTS `sale_weixinfollow`;
CREATE TABLE `sale_weixinfollow`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activityid` int(11) NULL DEFAULT NULL,
  `recordid` int(11) NULL DEFAULT NULL,
  `saleid` int(11) NULL DEFAULT NULL,
  `phone` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '被关注人手机号',
  `openid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '被关注人微信号',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '关注公众号' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sales
-- ----------------------------
DROP TABLE IF EXISTS `sales`;
CREATE TABLE `sales`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `truename` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `department` int(11) NULL DEFAULT NULL COMMENT '部门id',
  `position` int(11) NULL DEFAULT NULL COMMENT '职位id',
  `phone` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '可提现佣金',
  `cumulative_amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '累计佣金',
  `withdraw_amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '已提现金额',
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '状态',
  `openid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微信昵称',
  `logourl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信头像',
  `session_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hotelid` int(11) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_deleted` int(11) NOT NULL DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 506 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '分销员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for score
-- ----------------------------
DROP TABLE IF EXISTS `score`;
CREATE TABLE `score`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL COMMENT '会员id',
  `category` tinyint(1) NOT NULL COMMENT '类别 1 积分 2消费',
  `items` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '项目',
  `get_score` int(11) NOT NULL COMMENT '获取的积分',
  `used_score` int(11) NOT NULL COMMENT '消费的积分',
  `validtype` tinyint(1) NULL DEFAULT NULL COMMENT '有效期 1固定日期 2永久',
  `startdate` datetime(0) NULL DEFAULT NULL COMMENT '有效期开始时间',
  `enddate` datetime(0) NULL DEFAULT NULL COMMENT '有效期结束时间',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员积分' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for score_rule
-- ----------------------------
DROP TABLE IF EXISTS `score_rule`;
CREATE TABLE `score_rule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_consumption` decimal(10, 2) NOT NULL COMMENT '客房消费',
  `food_consumption` decimal(10, 2) NOT NULL COMMENT '餐饮消费',
  `cash` decimal(10, 2) NOT NULL COMMENT '现金价值',
  `room_score` int(11) NOT NULL COMMENT '客房消费兑换积分数',
  `food_score` int(11) NOT NULL COMMENT '餐饮消费兑换积分数',
  `cash_socre` int(11) NOT NULL COMMENT '现金价值折合积分数',
  `descrip` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '积分获取及使用说明',
  `valid_type` tinyint(1) NOT NULL COMMENT '有效期类型 1日期 2天数',
  `valid_enddate` datetime(0) NULL DEFAULT NULL COMMENT '积分有效期结束时间',
  `valid_days` int(4) NULL DEFAULT NULL COMMENT '有效期天数',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '积分兑换规则' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for scorerule_channel
-- ----------------------------
DROP TABLE IF EXISTS `scorerule_channel`;
CREATE TABLE `scorerule_channel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scoreruleid` int(11) NOT NULL COMMENT '积分规则id',
  `channelid` int(11) NOT NULL COMMENT '渠道id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '积分规则-渠道关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for scorerule_range
-- ----------------------------
DROP TABLE IF EXISTS `scorerule_range`;
CREATE TABLE `scorerule_range`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scoreruleid` int(11) NOT NULL COMMENT '积分规则id',
  `rangeid` int(11) NOT NULL COMMENT '范围id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '积分规则-范围关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for style_benefits
-- ----------------------------
DROP TABLE IF EXISTS `style_benefits`;
CREATE TABLE `style_benefits`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `styleid` int(11) NOT NULL COMMENT '会员样式id',
  `benefitsid` int(11) NOT NULL COMMENT '权益id',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 46 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员卡样式-权益关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for upgrade
-- ----------------------------
DROP TABLE IF EXISTS `upgrade`;
CREATE TABLE `upgrade`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL COMMENT '类型  1升级 2降级 3保级',
  `primitive_level_id` int(11) NOT NULL COMMENT '原始等级id',
  `upgrade_level_id` int(11) NOT NULL COMMENT '升级等级id',
  `basic_rule` int(11) NOT NULL COMMENT '级别规则',
  `other_rule_desc` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其他规则描述',
  `trigger_time` tinyint(1) NOT NULL COMMENT '触发时间点 1立即 2有效期届满',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '自动升级配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `wx_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `membertype` int(11) NULL DEFAULT NULL COMMENT '会员等级 1银卡  2 金卡 3 白金卡 4 黑卡     ((不用该字段)会员类别 1 注册会员  2 特权  3  业主   4  员工)',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `birthday` datetime(0) NULL DEFAULT NULL,
  `sex` tinyint(4) NULL DEFAULT NULL COMMENT '性别  1  男  2  女 ',
  `idtype` int(11) NULL DEFAULT NULL COMMENT '证件类型  1 身份证  2 护照  3 驾驶证',
  `idno` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cardno` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `countyrid` int(11) NULL DEFAULT NULL,
  `provinceid` int(11) NULL DEFAULT NULL,
  `cityid` int(11) NULL DEFAULT NULL,
  `districtid` int(11) NULL DEFAULT NULL,
  `address` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '1.正常 2.锁定',
  `setpass` tinyint(4) NULL DEFAULT NULL,
  `truename` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nickname` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `logourl` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `memberid` int(11) NOT NULL DEFAULT 0,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `usercode` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '会员编码',
  `channelid` int(11) NULL DEFAULT NULL COMMENT '来源渠道  ',
  `nativeplace` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '籍贯',
  `famous` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '民族',
  `company` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户所在单位',
  `job` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职务',
  `hobby` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '爱好',
  `inviter_userid` int(11) NULL DEFAULT NULL COMMENT '推荐人id',
  `hotelcode` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '酒店编码',
  `channelcode` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 784223 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user_card
-- ----------------------------
DROP TABLE IF EXISTS `user_card`;
CREATE TABLE `user_card`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL COMMENT '会员id',
  `cardid` int(11) NOT NULL COMMENT '会员卡id',
  `vaildtype` tinyint(1) NOT NULL COMMENT '有效期类别 1永久有效  2固定天数',
  `startdate` datetime(0) NULL DEFAULT NULL COMMENT '有效期开始时间',
  `enddate` datetime(0) NULL DEFAULT NULL COMMENT '有效期结束时间',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `applied` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否应用 0 否 1 是',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 升级  1 降级',
  `item` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '项目名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员-等级关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user_inviter
-- ----------------------------
DROP TABLE IF EXISTS `user_inviter`;
CREATE TABLE `user_inviter`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `userid` int(11) NOT NULL,
  `invitations` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邀请语',
  `img_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `img_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `times` tinyint(4) NOT NULL DEFAULT 0,
  `group_id` int(11) NULL DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sex` tinyint(4) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '分享有礼' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user_signed
-- ----------------------------
DROP TABLE IF EXISTS `user_signed`;
CREATE TABLE `user_signed`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NULL DEFAULT NULL COMMENT '签到日期',
  `times` int(11) NULL DEFAULT NULL COMMENT '连续签到天数',
  `userid` int(11) NOT NULL,
  `score` double NULL DEFAULT NULL COMMENT '积分',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态，PMS返回1成功0失败',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `group_id` int(11) NULL DEFAULT NULL,
  `hotel_id` int(11) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户签到' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user_wx
-- ----------------------------
DROP TABLE IF EXISTS `user_wx`;
CREATE TABLE `user_wx`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NULL DEFAULT NULL,
  `openid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `session_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `shop_openid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `shop_session_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for usr_assets
-- ----------------------------
DROP TABLE IF EXISTS `usr_assets`;
CREATE TABLE `usr_assets`  (
  `userid` int(11) NOT NULL,
  `score` double NOT NULL DEFAULT 0 COMMENT '积分',
  `deposit` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '余额',
  `pay_password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付密码',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`userid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '余额表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for usr_cardbinding
-- ----------------------------
DROP TABLE IF EXISTS `usr_cardbinding`;
CREATE TABLE `usr_cardbinding`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NULL DEFAULT NULL,
  `value_card_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '储蓄卡号',
  `binding` int(11) NULL DEFAULT NULL COMMENT '0绑定 1解绑',
  `creater` int(11) NULL DEFAULT NULL,
  `updater` int(11) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '储值卡记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for usr_contacts
-- ----------------------------
DROP TABLE IF EXISTS `usr_contacts`;
CREATE TABLE `usr_contacts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NULL DEFAULT NULL,
  `contactname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `idtype` int(11) NULL DEFAULT NULL,
  `idno` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` tinyint(2) NULL DEFAULT 0 COMMENT '0普通联系人 1默认联系人',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 59 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '常用联系人' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for usr_coupon
-- ----------------------------
DROP TABLE IF EXISTS `usr_coupon`;
CREATE TABLE `usr_coupon`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL COMMENT '会员id',
  `couponid` int(11) NOT NULL COMMENT '优惠券id',
  `couponno` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '优惠券编码',
  `status` int(11) NOT NULL COMMENT '优惠券使用状态 ： 1 未使用   2 已使用  3 已核销  99 作废',
  `startdate` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `enddate` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `couponname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '优惠券名称',
  `coupontitle` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '优惠券 标题 ',
  `usedesc` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '使用须知',
  `coupontype` int(11) NULL DEFAULT NULL COMMENT '优惠券类型',
  `conditionprice` decimal(10, 2) NULL DEFAULT NULL COMMENT '金额条件',
  `couponprice` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '减免金额  类型为满减时使用',
  `discount` int(11) NULL DEFAULT NULL COMMENT '折扣率  类型为折扣 使用',
  `discountprice` decimal(10, 2) NULL DEFAULT NULL COMMENT '封底金额',
  `finish_time` datetime(0) NULL DEFAULT NULL COMMENT '使用时间',
  `account_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `order_id` int(11) NULL DEFAULT NULL,
  `offlineuserid` int(11) NULL DEFAULT NULL,
  `cancel_time` datetime(0) NULL DEFAULT NULL,
  `order_type` tinyint(4) NULL DEFAULT NULL,
  `hotel_id` int(11) NULL DEFAULT NULL,
  `group_id` int(11) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `vaildtype` int(11) NULL DEFAULT NULL COMMENT '是否长期有效 0 否 1 是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1474364 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for usr_favorites
-- ----------------------------
DROP TABLE IF EXISTS `usr_favorites`;
CREATE TABLE `usr_favorites`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL COMMENT '类型 1酒店',
  `relateid` int(11) NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 58 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for usr_verifycode
-- ----------------------------
DROP TABLE IF EXISTS `usr_verifycode`;
CREATE TABLE `usr_verifycode`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `verifycode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `expiredate` datetime(0) NULL DEFAULT NULL,
  `content` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT 1,
  `response` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `verifytype` int(255) NOT NULL DEFAULT 1 COMMENT '类型 1登录 2忘记密码 3分销申请 4 储值卡绑定',
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 105 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
